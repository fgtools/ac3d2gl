@setlocal
@set TMPEXE=Release\ac3dview.exe
@if NOT EXIST %TMPEXE% goto ERR1
@set TMPDLL=freeglut.dll
@set TMP3RD=C:\FG\17\3rdParty\bin
@if NOT EXIST %TMP3RD%\%TMPDLL% goto ERR2

cd ..
@set TMPEXE=build\Release\ac3dview.exe
@if NOT EXIST %TMPEXE% goto ERR1
@echo RUnning in %CD%

@set PATH=%PATH%;%TMP3RD%
%TMPEXE% test.ac

@goto END

:ERR1
@echo Can NOT locate %TMPEXE%! Check name, location and FIX ME!
@goto END

:ERR2
@echo Can NOT locate %TMP3RD%\%TMPDLL%! Check name, location and FIX ME!
@goto END

:ERR3
@echo Directory structure not as expected. Can not locate aerostar folder!
@goto END


:END

