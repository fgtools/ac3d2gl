/* ======================================================================
    from : README.txt
    AC3DVIEW version 1.14 

    All the files here are free of copyright so you can use them as you 
    please.  If you do use them them please give credit to AC3D. www.ac3d.org

    1.11 added cdecl Prototype for WIN32 
    1.12 added ac_object_free
    1.13 reduced the default rotation speed; fixed fatal bug with argc when file 
         name was passed to program on commandline
    1.14 2013/05/06 - geoff
        Add keyboard interface to control position, orientation and rotation of
        view, and added a WritePPM dump capability
        Added 'crease %f' to ac3d.cxx parsing.

   ====================================================================== */
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#ifdef WIN32
#include <Windows.h>
#include <direct.h>
#else
#include <sys/time.h>
#include <string.h> // strlen()
#endif
#include <string>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "ac3d.hxx"

#define WINDOW_WIDTH    768
#define WINDOW_HEIGHT   576

static const char *mod_name = "main.cxx";

static char *acFileName = (char *)"test.ac";
#define MX_TITLE_BUF 264
static char title_buff[MX_TITLE_BUF+2];

int display_list = 0;
static int do_dump = 0;
static char *dump_file = 0;
static int got_config_item = 0; // set to 1 IFF we have had a CONIFG for this file
static int set_mod_siz_pos = 1;
struct timeval tv;
int ts, tu;
int ets, etu;

// ===========================================
// read and write this set to a config file
// ===========================================
static float x_trans =   0.0f;
static float y_trans =  -4.0f;  // was 0.0;
static float z_trans = -10.0f;  // was -3.0
static float angle_rate = 25.0f; // change at rate of ?? degrees per second
static int frozen = 0;
static float x_axis = 0.0f;
static float y_axis = 1.0f;
static float z_axis = 0.0f;
static float rot = 0.0;
static int window_width = WINDOW_WIDTH;
static int window_height = WINDOW_HEIGHT;
// -------------------------
static float eyex = 0.0f;
static float eyey = 1.8f;
static float eyez = 4.0f;
static float centrex = 0.0f;
static float centrey = 0.0f;
static float centrez = 0.0f;
static float upx = 0.0f;
static float upy = 1.0f;
static float upz = 0.0f;
// ===========================================

static float frametime;
static float tottime = 0.0;
static int framec = 0;
static float total_time = 0.0;

static time_t last_secs;
static int frames = 0;
static int frames_per_sec = 0;
static int elap_secs = 0;
static double begin_secs, prev_secs;

void set_projection(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (float)w/(float)h, 0.1, 10000.0);
}

void init_gfx(void)
{
    ac_prepare_render();
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    set_projection(WINDOW_WIDTH,WINDOW_HEIGHT);
}

#ifdef WIN32
void gettimeofday( struct timeval *tv, void *dummy )
{
	LARGE_INTEGER perfCount;
	LARGE_INTEGER perfFreq;

	double freq, count, time;
	tv->tv_sec = 0;
	tv->tv_usec= 0;

	// Get the frequency
	if( !QueryPerformanceFrequency( &perfFreq ) )
		return;

	// Get the current count.
	if( !QueryPerformanceCounter( &perfCount ) )
		return;

	freq  = (double) perfFreq.LowPart;
	count = (double) perfCount.LowPart;
	freq += (double) perfFreq.HighPart  *  4294967296.0;
	count+= (double) perfCount.HighPart *  4294967296.0;

	time = count / freq;

	tv->tv_sec = (int) time;
	tv->tv_usec= (int) ((time - (double)tv->tv_sec) * 1000000.0);

}
#endif // WIN32

void timer_start()
{
    gettimeofday(&tv, NULL);
    ts = tv.tv_sec;
    tu = tv.tv_usec;
}

float timer_stop()
{
    int s, u;
    gettimeofday(&tv, NULL);
    ets = tv.tv_sec;
    etu = tv.tv_usec;
    s = ets-ts;
    u = etu-tu;
    return (float)(s+u/1.0E+6);
}

///////////////////////////////////////////////////////////////////////////////
#define USE_PERF_COUNTER
///////////////////////////////////////////////////////////////////////////////
#if (defined(WIN32) && defined(USE_PERF_COUNTER))
// QueryPerformanceFrequency( &frequency ) ;
// QueryPerformanceCounter(&timer->start) ;
double get_seconds()
{
    static double dfreq;
    static int done_freq = 0;
    static int got_perf_cnt = 0;
    double d;
    if (!done_freq) {
        LARGE_INTEGER frequency;
        if (QueryPerformanceFrequency( &frequency )) {
            got_perf_cnt = 1;
            dfreq = (double)frequency.QuadPart;
        }
        done_freq = 1;
    }
    if (got_perf_cnt) {
        LARGE_INTEGER counter;
        QueryPerformanceCounter (&counter);
        d = (double)counter.QuadPart / dfreq;
    }  else {
        DWORD dwd = GetTickCount(); // milliseconds that have elapsed since the system was started
        d = (double)dwd / 1000.0;
    }
    return d;
}

#else // !WIN32
double get_seconds()
{
    struct timeval tv;
    gettimeofday(&tv,0);
    double t1 = (double)(tv.tv_sec+((double)tv.tv_usec/1000000.0));
    return t1;
}
#endif // WIN32 y/n
///////////////////////////////////////////////////////////////////////////////


void drawscene(void)
{
    double curr = get_seconds();
    double elap = curr - prev_secs;
    time_t currs = time(0);
    GLfloat light_position[] = { -1000.0, 1000.0, 1000.0, 0.0 };

    timer_start();

    set_projection(window_width, window_height);

    // gluLookAt(0, 1.8, 4, 0, 0, 0, 0, 1, 0);
    gluLookAt(eyex, eyey, eyez, centrex, centrey, centrez, upx, upy, upz);

    glClearColor(0,0,0,0);
    glClearColor(0.5,0.5,0.5,0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glTranslatef ( x_trans, y_trans, z_trans ) ;

    // this can be TOO FAST!
    //rot += 0.01f; // was 1
    if (!frozen) {
        // bump the rotation angle 
        rot += (float) ( elap * angle_rate );
    }

    if (rot >= 360.0) rot -= 360.0f;

    // glRotatef(rot, 0, 1, 0);
    glRotatef(rot, x_axis, y_axis , z_axis );

    glCallList(display_list);

    glFlush();
    glFinish();
    glutSwapBuffers();

    frametime = timer_stop();
    tottime += frametime;
    framec++;
    if (tottime > 5.0) {
        total_time += tottime;
        if (ac_verb5()) {
    	    printf("%.1f: Approx frames per sec: %.1f (%d)\n", 
                total_time, 1.0/(tottime/framec), frames_per_sec);
        }
	    tottime = 0;
	    framec = 0;
    }
    frames++;
    if (currs != last_secs) {
        frames_per_sec = frames;
        frames = 0;
        last_secs = currs;
        elap_secs++;
    }
    prev_secs = curr;
}

void reshape_gfx(int w, int h)
{
    // potentially set new width/height
    window_width  = w;
    window_height = h;
    glViewport(0, 0, w, h);
    set_projection(w,h);
}

/* ****************************************************************************************
   WritePPM
   **************************************************************************************** */
#ifdef _MSC_VER
#define M_IS_DIR _S_IFDIR
#else // !_MSC_VER
#define M_IS_DIR S_IFDIR
#endif

#define DT_NONE     0
#define DT_FILE     1
#define DT_DIR      2

int is_file_or_directory ( char * path )
{
	struct stat buf;
    if (!path || (*path == 0))
        return DT_NONE;
	if (stat(path,&buf) == 0) {
		if (buf.st_mode & M_IS_DIR)
			return DT_DIR;
		else
			return DT_FILE;
	}
	return DT_NONE;
}

/*
   Write the current view to a file
   The multiple fputc()s can be replaced with
      fwrite(image,width*height*3,1,fptr);
   If the memory pixel order is the same as the destination file format.
   from : http://paulbourke.net/miscellaneous/windowdump/

*/
static int counter = 0; /* This supports animation sequences */

static int WritePPM(int width, int height, int stereo)
{
   int i,j;
   FILE *fptr;
   char fname[32];
   char fname2[32];
   unsigned char *image;
   int OpenFile;

   /* Allocate our buffer for the image */
   if ((image = (unsigned char *)malloc(3*width*height*sizeof(char))) == NULL) {
      fprintf(stderr,"Failed to allocate memory for image\n");
      return(FALSE);
   }

   glPixelStorei(GL_PACK_ALIGNMENT,1);

   /* Open the file */
   OpenFile = 1;
   while (OpenFile) {
       if (stereo)
          sprintf(fname,"L_%04d.ppm",counter);
       else
          sprintf(fname,"C_%04d.ppm",counter);
       if (is_file_or_directory(fname))
           counter++;   /* don't overwrite any existing file */
       else
           OpenFile = 0;
   }

   if ((fptr = fopen(fname,"w")) == NULL) {
      fprintf(stderr,"Failed to open file for window dump\n");
      free(image);
      return(FALSE);
   }

   /* Copy the image into our buffer */
   glReadBuffer(GL_BACK_LEFT);
   glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE,image);

   /* Write the PPM file */
   fprintf(fptr,"P6\n%d %d\n255\n",width,height); /* for ppm */
   for ( j = height-1; j >= 0 ;j-- ) {
      for ( i = 0; i < width; i++ ) {
         fputc( image[3*j*width+3*i+0], fptr);
         fputc( image[3*j*width+3*i+1], fptr);
         fputc( image[3*j*width+3*i+2], fptr);
      }
   }
   fclose(fptr);

   if (stereo) {
      /* Open the file */
      sprintf(fname2,"R_%04d.ppm",counter);
      if ((fptr = fopen(fname2,"w")) == NULL) {
         fprintf(stderr,"Failed to open file for window dump\n");
         free(image);
         return(FALSE);
      }

      /* Copy the image into our buffer */
      glReadBuffer(GL_BACK_RIGHT);
      glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE,image);

      /* Write the PPM file */
      fprintf(fptr,"P6\n%d %d\n255\n",width,height); /* for ppm */
      for ( j = height-1; j >= 0; j-- ) {
         for ( i = 0; i < width ; i++ ) {
            fputc( image[3*j*width+3*i+0], fptr);
            fputc( image[3*j*width+3*i+1], fptr);
            fputc( image[3*j*width+3*i+2], fptr);
         }
      }
      fclose(fptr);
   }

   /* Clean up */
   counter++;
   free(image);
   printf("PPM image %dx%d, written to %s\n", width, height, fname);
   if (stereo)
       printf("2nd image %dx%d, written to %s\n", width, height, fname2);

   return(TRUE);
}
/* ******************************************************************************* */

/* ---------------------------------------------------
    save and restore configuration file
    An INI type file, with section [name_of.ac]
    conf_file = ".ac3d2glrc";
    parameters kept/restored
    float x_trans =   0.0f;
    float y_trans =  -4.0f;  // was 0.0;
    float z_trans = -10.0f;  // was -3.0
    double angle_rate = 25.0; // change at rate of ?? degrees per second
    int frozen = 0;
    float x_axis = 0.0f;
    float y_axis = 1.0f;
    float z_axis = 0.0f;
    float rot = 0.0;
  ----------------------------------------------------- */
static const char*conf_file = ".ac3d2glrc";
static const char *app_name = "ac3dview";

static const char *szx_trans = "x_trans";
static const char *szy_trans = "y_trans";
static const char *szz_trans = "z_trans";
static const char *szangle_rate = "angle_rate";
static const char *szfrozen = "frozen";
static const char *szx_axis = "x_axis";
static const char *szy_axis = "y_axis";
static const char *szz_axis = "z_axis";
static const char *szrot = "rot";
static const char *szwwid = "window_width";
static const char *szwhei = "window_height";

#ifdef WIN32
#define PATH_SEP "\\"
#else
#define PATH_SEP "/"
#endif

#define t_int 1
#define t_float 2

typedef struct tagMYINI {
    const char *sz;
    int typ;
    char *val;
}MYINI, *PMYINI;

// *** INI LIST ***
static MYINI myini[] = {
    { szx_trans, t_float, (char *)&x_trans },
    { szy_trans, t_float, (char *)&y_trans },
    { szz_trans, t_float, (char *)&z_trans },
    { szx_axis,  t_float, (char *)&x_axis  },
    { szy_axis,  t_float, (char *)&y_axis  },
    { szz_axis,  t_float, (char *)&z_axis  },
    { szrot,     t_float, (char *)&rot     },
    { szangle_rate,t_float,(char *)&angle_rate },
    { szfrozen,  t_int,   (char *)&frozen  },
    { szwwid,    t_int,   (char *)&window_width },
    { szwhei,    t_int,   (char *)&window_height },
    // always last
    { 0,        0,        0               }
};

#define MyINICount  (sizeof(myini) / sizeof(MYINI)) - 1 // count

char *get_base_name( char *name )
{ 
    int i, c, len;
    char *bn = name;
    len = (int)strlen(name);
    for (i = 0; i < len; i++) {
        c = name[i];
        if ((c == '/')||(c == '\\')) {
            bn = &name[i+1];
        }
    }
    return bn;
}

char *get_config_dir()
{
    char *data = 0;
#ifdef _MSC_VER
    data = getenv("APPDATA");
    if (!data) {
        data = getenv("LOCALAPPDATA");
    }
#else
    data = getenv("HOME");
#endif
    return data;
}

int get_config_file( std::string &s )
{
    s += PATH_SEP;
    s += app_name;
    if ( is_file_or_directory ( (char *) s.c_str() ) != DT_DIR ) return 0;
    s += PATH_SEP;
    s += conf_file;
    if ( is_file_or_directory ( (char *) s.c_str() ) != DT_FILE ) return 0;
    return 1;
}

int my_strncmp(const char *string1, const char *string2, size_t count )
{
    while (*string1 && *string2 && count) {
        if (*string1 != *string2)
            return *string1 - *string2;
        string1++;
        string2++;
        count--;
    }
    return 0;
}

void clean_buff_tail(char *rb)
{
    size_t len = strlen(rb);
    while (len) {
        len--;
        if (rb[len] > ' ')
            break;
        rb[len] = 0;
    }
}

const char *get_config_file_name()
{
    static std::string cfg;
    char *data = get_config_dir();
    if (!data) return "";
    cfg = data;
    if (!get_config_file(cfg)) return "";
    return cfg.c_str();
}

int read_config()
{
    int ifound = 0; // number of items FOUND by config
    int ichg = 0;   // number of items CHANGED by config
    int iclean = 0;
    char *data = get_config_dir();
    if (!data) return -1;   // FAILED
    std::string s(data);
    if (!get_config_file(s)) return -2;
    FILE *fp = fopen(s.c_str(),"r");
    if (!fp) return -3; // no file FAILED
    std::string sect(get_base_name(acFileName));
    int len = (int)sect.size();
    char *rb = title_buff;
    int in_section = 0;
    int had_section = 0;
    int vi;
    float fi;
    while (!feof(fp)) {
        char *gp = fgets(rb,MX_TITLE_BUF,fp);
        if (!gp) break;
        size_t red = strlen(rb);
        clean_buff_tail(rb);
        red = strlen(rb);
        if (rb[0] == '[') {
            int cmp = my_strncmp(&rb[1],sect.c_str(),len);
            if ((cmp == 0) && (rb[len+1] == ']')) {
                in_section = 1; // found 'our' section
                had_section = 1;
            } else {
                in_section = 0;
                if (had_section) break;
            }
        } else if (in_section) {
            PMYINI pini = &myini[0];    // INI list
            size_t len2;
            char *tp;
            int fnd = 0;
            while (pini->sz) {
                len2 = strlen(pini->sz);
                if (red > len2) {
                    if ((strncmp(rb,pini->sz,len2) == 0) && (rb[len2] == '=')) {
                        fnd = 1;
                        tp = &rb[len2+1];
                        switch (pini->typ) {
                        case t_int:
                            ifound++;
                            vi = atoi(tp);
                            if (*(int *)pini->val != vi) {
                                *(int *)pini->val = vi;
                                ichg++;
                            }
                            break;
                        case t_float:
                            ifound++;
                            fi = (float)atof(tp);
                            if (*(float *)pini->val != fi) {
                                *(float *)pini->val = fi;
                                ichg++;
                            }
                            break;
                        }
                    }
                }
                if (fnd) break; // done this parameter
                pini++;
            }
            if (!fnd) {
                if (iclean == 0)
                    printf("In the [%s] section\n", sect.c_str());
                printf("Found [%s] in config file, not used!\n", rb);
                iclean++;
            }
        }

    }
    fclose(fp);
    if (iclean) {
        printf("Note the above %d item(s) should be removed from the\n%s config file\n",
            iclean, s.c_str());
    }
    if ((ifound + 1) >= MyINICount) {
        got_config_item = 1;    // we have had a CONFIG for this file
        printf("%s: Loaded config for %s, from %s\n", mod_name, sect.c_str(), s.c_str());
    }
    return ifound;
}

// just an estimate of the maximum size of a section
size_t maximum_section(int slen)
{
    size_t total = slen + 4;
    PMYINI pini = &myini[0];
    size_t len2;
    while (pini->sz) {
        len2 = strlen(pini->sz);
        len2 += 32; // + '=' and a float
        total += len2;
        pini++;
    }
    return total;
}

#ifdef _WIN32
#  define MkDir(d,m)       _mkdir(d)
#else
#  define MkDir(d,m)       mkdir(d,m)
#endif

#define EndBuf(a)   ( a + strlen(a) )

int write_config()
{
    char *data = get_config_dir();
    if (!data) return -1;
    std::string s(data);
    s += PATH_SEP;
    s += app_name;
    if ( is_file_or_directory ( (char *) s.c_str() ) != DT_DIR ) {
        // need to create directory
        if ( MkDir( s.c_str(), 0777 )) {
            printf("error: Unable to create directory [%s]!\nThus unable to write config!\n", s.c_str());   
            return -2;
        }
    }
    s += PATH_SEP;
    s += conf_file;
    FILE *fp = 0;
    size_t size = 0;
    if ( is_file_or_directory ( (char *) s.c_str() ) == DT_FILE ) {
        fp = fopen(s.c_str(),"r");
        if (!fp) {
            printf("error: Unable to read file [%s]!\nThus unable to write config!\n", s.c_str());   
            return -3;
        }
        fseek(fp,0,SEEK_END);   // = 2
        size = ftell(fp);
        fseek(fp,0,SEEK_SET);   // = 0
    }
    std::string sect(get_base_name(acFileName));
    int len = (int)sect.size();
    size_t sbuf = size + maximum_section(len);
    char *rb = (char *)malloc(sbuf);
    if (!rb) {
        // complain about memory???
        if (fp) fclose(fp);
        return -4;
    }

    // write this section into allocated memory
    PMYINI pini = &myini[0];
    *rb = 0;
    size_t cfglen = sprintf(EndBuf(rb),"[%s]\n", sect.c_str());
    while (pini->sz) {
        cfglen += sprintf(EndBuf(rb),"%s=", pini->sz);
        switch (pini->typ) {
        case t_int:
            cfglen += sprintf(EndBuf(rb),"%d\n", *(int *)pini->val);
            break;
        case t_float:
            cfglen += sprintf(EndBuf(rb),"%f\n", *(float *)pini->val);
            break;
        }
        pini++;
    }

    // reallocate if we need more buffer
    if ((cfglen + size + 2) >= sbuf) {
        sbuf = cfglen + size + 2;
        rb = (char *)realloc(rb,sbuf);
        if (!rb) {
            // complain about memory???
            fclose(fp);
            return -5;
        }
    }

    if (fp) {
        // ok, must read and filter the existing file
        int in_section = 0;
        char *gp = EndBuf(rb);
        while (!feof(fp)) {
            gp = fgets(gp,MX_TITLE_BUF,fp);
            if (!gp) break;
            size_t red = strlen(gp);
            if (gp[0] == '[') {
                if ((strncmp(&rb[1],sect.c_str(),len) == 0) && (rb[len+1] == ']')) {
                    in_section = 1; // found 'our' section
                } else {
                    in_section = 0;
                    cfglen += red;
                    gp = EndBuf(gp);
                }
            } else if (!in_section) {
                cfglen += red;
                gp = EndBuf(gp);
            }
        }
        fclose(fp);
    }

    // =================================================================
    // overwrite any existing file with buffered config data
    fp = fopen(s.c_str(),"w");
    if (!fp) {
        printf("error: Unable to write file [%s]!\nThus unable to write config!\n", s.c_str());
        free(rb);
        return -6;
    }
    size_t wtn = fwrite(rb,1,cfglen,fp);
    fclose(fp);
    free(rb);
    return ((wtn == cfglen) ? 0 : -7);   // success in writting config file
}


static void key_help()
{
    int tframes = frames_per_sec;
    printf("Keyboard Help - to size, position and rotate the model.\n");
    printf(" ESC or q = Quit\n");
    printf(" f/F      = Toggle rotation freeze. (def=%s)\n", (frozen ? "On" : "Off"));
    printf(" s/S      = Slow/Speed up rotation speed. angle_rate=%f\n", angle_rate);
    printf(" x/X      = Left/Right - x translation factor (%f)\n", x_trans);
    printf(" y/Y      = Up/Down    - y translation factor (%f)\n", y_trans);
    printf(" z/Z      = Near/Far   - z translation factor (%f)\n", z_trans);
    printf(" ?/h      = This help, frame rate and current angle...\n");
    printf(" 1-7      = Set glRotate(rot,x,y,z) - %.1f,%.1f,%.1f\n", x_axis, y_axis, z_axis);
    printf(" d/D      = Dump scene to numbered PPM file in work directory. (D in stereo)\n");
    printf(" v/V      = Reduce/Increase verbosity. (def=%d)\n", ac_get_verbosity());
    printf(" w/W      = Write/Read config file.\n");
    printf(" fps=%d, rotation angle=%f, secs %d\n", tframes, rot, elap_secs);
}

// in the range -10 to 10 subtract 0.5
// if lesser or greater   subtract 1/20 of the value
// if zero,               subtract 0.5
float sub_5_percent(float curr)
{
    float next = curr;
    if (next > 0.0f) {
        if (next > 10.0f) {
            next -= (next / 20.0f);
        } else {
            if (next < 0.5f)
                next = 0.0f;
            else
                next -= 0.5f;
        }
    } else if (next < 0.0f) {
        if (next < -10.0f)
            next -= (-next / 20.0f);
        else 
            next -= 0.5f;
    } else {
        next -= 0.5f;
    }
    return next;
}
// in the range -10 to 10 add 0.5
// if lesser or greater   add 1/20 of the value
// if zero,               add 0.5
float add_5_percent(float curr)
{
    float next = curr;
    if (next > 0.0f) {
        if (next > 10.0f) {
            next += (next / 20.0f);
        } else {
            next += 0.5f;
        }
    } else if (next < 0.0f) {
        if (next < -10.0f)
            next += (-next / 20.0f);
        else {
            if (next < -0.5f)
                next += 0.5f;
            else
                next = 0.0f;
        }
    } else {
        next += 0.5f;
    }
    return next;
}

static void keyfn ( unsigned char c, int i1, int i2 )
{
    int v, sxyz = 0;
    if ((c == 'q')||(c == 0x1b)) {
        printf("Got exit key...\n");
        ac_free_objects();
        exit ( 0 ) ;
    }

    if ((c == '?')||(c == 'h')) {
        key_help();
    } else if (c == 'w') {
        v = write_config();
        //if (ac_verb2()) 
        printf("w = Written config %s (%d)\n", get_config_file_name(), v);
    } else if (c == 'W') {
        v = read_config();
        if (ac_verb2()) printf("W = Read config %s (%d)\n", get_config_file_name(), v);
    } else if ((c == 'f')||(c == 'F')) {
        if (frozen)
            frozen = 0;
        else
            frozen = 1;
        if (ac_verb2()) printf("f = Toggle frozen %d\n", frozen);
    } else if (c == 's') {
        angle_rate -= 1.0;
        if (ac_verb2()) printf("angle_rate = %f\n", angle_rate);
    } else if (c == 'S') {
        angle_rate += 1.0;
        if (ac_verb2()) printf("angle_rate = %f\n", angle_rate);
    } else if (c == 'x') {
        //x_trans -= 0.5f;
        x_trans = sub_5_percent(x_trans);
        if (ac_verb2()) printf("x_trans = %f\n", x_trans);
    } else if (c == 'X') {
        //x_trans += 0.5f;
        x_trans = add_5_percent(x_trans);
        if (ac_verb2()) printf("x_trans = %f\n", x_trans);
    } else if (c == 'y') {
        //y_trans -= 0.5f;
        y_trans = sub_5_percent(y_trans);
        if (ac_verb2()) printf("y_trans = %f\n", y_trans);
    } else if (c == 'Y') {
        //y_trans += 0.5f;
        y_trans = add_5_percent(y_trans);
        if (ac_verb2()) printf("y_trans = %f\n", y_trans);
    } else if (c == 'z') {
        //z_trans -= 0.5f;
        z_trans = sub_5_percent(z_trans);
        if (ac_verb2()) printf("z_trans = %f\n", z_trans);
    } else if (c == 'Z') {
        //z_trans += 0.5;
        z_trans = add_5_percent(z_trans);
        if (ac_verb2()) printf("z_trans = %f\n", z_trans);
    } else if (c == '1') {
       x_axis = 1.0;
       y_axis = 0.0;
       z_axis = 0.0;
       sxyz = 1;
    } else if (c == '2') {
       x_axis = 0.0;
       y_axis = 1.0;
       z_axis = 0.0;
       sxyz = 1;
    } else if (c == '3') {
       x_axis = 0.0;
       y_axis = 0.0;
       z_axis = 1.0;
       sxyz = 1;
    } else if (c == '4') {
       x_axis = 1.0;
       y_axis = 1.0;
       z_axis = 0.0;
    } else if (c == '5') {
       x_axis = 1.0;
       y_axis = 0.0;
       z_axis = 1.0;
       sxyz = 1;
    } else if (c == '6') {
       x_axis = 0.0;
       y_axis = 1.0;
       z_axis = 1.0;
       sxyz = 1;
   } else if (c == '7') {
       x_axis = 1.0;
       y_axis = 1.0;
       z_axis = 1.0;
       sxyz = 1;
    } else if (c == 'd') {
        WritePPM( window_width, window_height, 0 );
    } else if (c == 'D') {
        WritePPM( window_width, window_height, 1 );
    } else if (c == 'v') {
        v = ac_get_verbosity();
        if (v) {
            v--;
            ac_set_verbosity(v);
        }
    } else if (c == 'V') {
        v = ac_get_verbosity();
        v++;
        ac_set_verbosity(v);
    }
    if (sxyz && ac_verb2()) printf("%c: set x,y,z axis %.1f,%.1f,%.1f\n", c, x_axis, y_axis, z_axis);
}

char *base_name( char *name )
{
    int i, len, c;
    char *bn = name;
    len = (int)strlen(name);
    for (i = 0; i < len; i++) {
        c = name[i];
        if ((c == '/')||(c == '\\'))
            bn = &name[i+1];
    }
    return bn;
}

void give_cmd_help(char *name)
{
    printf("\n");
    printf("%s: version 1.14, compiled %s, at %s\n", name, __DATE__, __TIME__ );
    printf("\n");

    printf("USAGE: %s [options] [ac_file]\n", name );

    printf("\nOPTIONS:\n");
    printf(" --help    (-h or -?) = This help and exit(2)\n");
    printf(" --tex path      (-t) = Add a texture path.\n");
    printf(" --verb num      (-v) = Set verbosity 0-9. (def=%d)\n", ac_get_verbosity());
    printf(" --notex         (-n) = Make no attempt to load any textures.\n");
    printf(" --dump file     (-d) = Just a DUMP of the ac object to a file.\n");
    printf(" --export file   (-e) = Export the ac object as a Three.js (json) file, and exit.\n");
    printf(" These options only apply to the export of the json Three.js\n");
    printf(" --addnorm       (-a) = Add 'normal' generation to export. (def=0)\n");
    printf(" --scale float   (-s) = Set the 'scale' used in the export. (def=%f)\n", ac_get_scale());

    printf("\nIf no 'ac_file' given, then the default [%s] will be loaded.\n", acFileName);
    printf("\n");

    printf("AIM: To load an AC3D file, with textures, and display the model in a GL\n");
    printf("     context, and allow some rotation, sizing and positioning of the model\n");
    printf("     through keyboard input. When running '?' to show keys, with\n");
    printf("     ESC or q to exit program.\n");
    printf("\n");

    printf("Enjoy... aborting with error level 2\n");
    exit(2);
}

int process_command(int argc, char *argv[])
{
    int i, c, i2;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
            case 'a':   /* --addnorm */
                ac_set_add_normals(1);
                break;
            case 'd':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    dump_file = STRING(sarg);
                } else {
                    printf("%s: ERROR: a file name must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                do_dump =1;
                break;
            case 'e':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    dump_file = STRING(sarg);
                } else {
                    printf("%s: ERROR: a file name must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                do_dump = 2;
                break;
            case 's':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    ac_set_scale((float)atof(sarg));
                } else {
                    printf("%s: ERROR: a float value must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                break;
            case '?':
            case 'h':
                give_cmd_help(base_name(argv[0]));
                break;
            case 'n':   /* --notex */
                ac_set_load_textures(0);
                break;
            case 't':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    if (is_file_or_directory ( sarg ) == DT_DIR) {
                        ac_add_texture_paths(sarg); /* add to texture paths */
                    } else {
                        printf("%s: ERROR: Unable to stat directory [%s]! aborting....\n", mod_name, sarg);
                        goto Bad_Arg;
                    }
                } else {
                    printf("%s: ERROR: a path must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                break;
            case 'v':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    ac_set_verbosity( atoi(sarg) );
                } else {
                    printf("%s: ERROR: a verbosity value must follow [%s]\n", mod_name, arg);
                    goto Bad_Arg;
                }
                break;
            default:
Bad_Arg:
                printf("%s: ERROR: Unknown argument [%s]... aborting...\n", mod_name, arg );
                return 1;
                break;
            }
        } else {
            acFileName = STRING(arg);
        }
    }

    if (is_file_or_directory ( acFileName ) == DT_FILE)
        ac_set_file_name(acFileName);   /* to be used as alternative path to a texture */
    else {
        printf("%s: ERROR: Unable to stat file [%s]! aborting....\n", mod_name, acFileName);
        return 1;
    }
    return 0;
}

void testing() 
{
    int res = read_config();
    res = write_config();
    exit(res);
}

static BBOX bb;
void set_model_size_pos(ACObject *ob)
{
    if (!ac_set_bounding_box(ob, &bb))
        return;
    float r = bb.radius;
    ACPoint p = bb.center;
    float fDistance = r / 0.57735f; // where 0.57735f is tan(30 degrees)
    //double dNear = fDistance - r;
    //double dFar = fDistance + r;
    // The glFrustum function multiplies the current matrix by a perspective matrix.
    // void glFrustum(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar);
    // left   The coordinate for the left-vertical clipping plane.
    // right  The coordinate for the right-vertical clipping plane.
    // bottom The coordinate for the bottom-horizontal clipping plane.
    // top    The coordinate for the bottom-horizontal clipping plane. 
    // zNear  The distances to the near-depth clipping plane. Must be positive.
    // zFar   The distances to the far-depth clipping planes. Must be positive.
    //glFrustum(-r, +r, +r, -r, dNear, dFar); // ignoring aspect ratio of the window for now!
    // All we need to do is to position the camera back along the z axis to where we want it -
    // glMatrixMode(GL_MODELVIEW);
    // glLoadIdentity();
    // gluLookAt(0.0f, 0.0f, fDistance, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
    eyex = 0.0f;
    eyey = 0.0f;
    eyez = fDistance;
    centrex = 0.0f;
    centrey = 0.0f;
    centrez = 0.0f;
    upx = 0.0f;
    upy = 1.0f;
    upz = 0.0f;
    // and then move the model to align its centre with the origin -
    // glTranslatef(-p.x, -p.y, -p.z);
    // glTranslatef ( x_trans, y_trans, z_trans ) ;
    x_trans = -p.x;
    y_trans = -p.y;
    z_trans = -p.z;
}

// debug file : C:\FG\18\blendac3d\c172p\c172pm.ac
// or a very simple one : C:\FG\18\ac2glview\data\cube.ac
// C:\FG\18\ac2glview\build\tempac3d.ac
int main(int argc, char *argv[])
{
    ACObject *ob;

    //testing();

    if (process_command(argc,argv))
        return 1;

    read_config();

    glutInit(&argc, argv);

    glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize(window_width, window_height);

    sprintf(title_buff,"%s - %s", base_name(argv[0]), base_name(acFileName));
    glutCreateWindow(title_buff);

    glutReshapeFunc(reshape_gfx);
    glutDisplayFunc(drawscene);
    glutKeyboardFunc(keyfn);
    glutIdleFunc(drawscene);

    init_gfx();

    /* NOTE: Some (or all) the above GL init MUST be done before this 
     *   since this can include image texture loading which results 
     *   in a call to  gluBuild2DMipmaps(...)
     */

    ob = ac_load_ac3d(acFileName);

    if (ob == NULL) {
		printf("%s: ERROR: failed to load %s! exiting 1.\n", mod_name, acFileName);
        return 1;
    }

    if (do_dump) {
        FILE *fp = fopen(dump_file,"w");
        if (!fp) {
    		printf("%s: ERROR: failed to create %s! exiting 1.\n", mod_name, dump_file);
            return 1;
        }
        if (do_dump == 2)
            ac_export_threejs(ob,fp);
        else
            ac_dump_simple(ob,fp);
        fclose(fp);
        ac_object_free(ob);
   		printf("%s: Object dumped to %s! exiting 0.\n", mod_name, dump_file);
        return 0;
    }

    if (!got_config_item && set_mod_siz_pos)
        set_model_size_pos(ob);

	display_list = ac_display_list_render_object(ob);

    if (ac_verb2()) key_help();
    last_secs = time(0);
    begin_secs = get_seconds();
    prev_secs  = begin_secs;

    // ******************************
	glutMainLoop(); // run GLUT
    // ******************************

    return 0; /* do no think we ever reach here... */
}

/* eof - main.cxx */
