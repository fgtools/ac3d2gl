/* 
 * loadppm.cxx
 *
 * read in a PPM or PGM image and display it, full size
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <malloc.h>

#ifdef _MSC_VER
#include <Windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

/* variables */

#define MAXLINE 80	/* maximum length of a line of text */

static GLint windW, windH;	/* size of the window on the screen */

static GLubyte *Picture;	/* Array of pixels */

static int filetype;
enum {PGM, PPM};	/* possible file types */

/* Read in picture from a PPM or PGM file */
static void readPPM (char *filename, GLubyte **pic) 
{
    FILE *fp;
	char line[MAXLINE];
	int i, j, size, rowsize;
	GLubyte *ptr;

    /* Read in file type */
    fp = fopen(filename, "rb");
    if (!fp) {
	    printf("Error: Unable to OPEN file %s as input! aborting...\n", filename);
	    exit(1);
    }
    fgets (line, MAXLINE, fp); /* 1st line: PPM or PGM */
    if (line[1] == '5')
	    filetype = PGM;
    else if (line[1] == '6')
	    filetype = PPM;
    else {
	    printf("Error: file %s not PPM or PGM! aborting...\n", filename);
        fclose(fp);
	    exit(1);
    }

    /* Read in width and height, & allocate space */
    fgets (line, MAXLINE, fp); /* 2nd line: width height */
    if (sscanf(line, "%d %d", &windW, &windH) == 2) {
        if (windW && windH) {
            /* have a width and height - use them */
            printf ("File: %s image width is %d, height is %d\n", filename, windW, windH);
        } else {
    	    printf("Error: file %s line 2 has no width=%d or no height=%d! aborting...\n", 
                filename, windW, windH );
            fclose(fp);
	        exit(1);
        }
    } else {
	    printf("Error: file %s line 2 not width and height! aborting...\n", filename);
        fclose(fp);
	    exit(1);
    }

    if (filetype == PGM) {
	    size = windH * windW;		/* greymap: 1 byte per pixel */
	    rowsize = windW;
    } else /* filetype == PPM */ {
        size = windH * windW * 3;	/* pixmap: 3 bytes per pixel */
	    rowsize = windW * 3;
    }
    *pic = (GLubyte *)malloc (size);
    if (!*pic) {
	    printf("Error: memory failed allocation of %d byes! aborting...\n", size);
        fclose(fp);
	    exit(1);
    }

    /* Read in maximum value (ignored!) */
    fgets (line, MAXLINE, fp); /* 3rd line */

    /* Read in the pixel array row-by-row: 1st row = top scanline */
    ptr = *pic + (windH-1) * rowsize;   /* get to last row */
    for (i = windH; i > 0; i--) {
	    j = fread((void *)ptr, 1, rowsize, fp);
        if (j != rowsize) {
    	    printf("Error: file %s read failed! Reuested %d, but got %d! aborting...\n", filename,
                rowsize, j);
            fclose(fp);
            free(*pic);
	        exit(1);
        }
	    ptr -= rowsize; /* back up to top row */
    }
    fclose(fp); 
}

/* Draw the picture on the screen */
void Draw(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glRasterPos2i(0, 0);

	if (filetype == PGM)	/* greymap: use as illumination values */
		glDrawPixels(windW, windH, GL_LUMINANCE, GL_UNSIGNED_BYTE, Picture);
	else // filetype == PPM
		glDrawPixels(windW, windH, GL_RGB, GL_UNSIGNED_BYTE, Picture);

    glutSwapBuffers();
}

/* Resize the picture */

void Resize(GLint w, GLint h)
{
    // no need to keep size
    //	windW = w;
    //	windH = h;
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // always paint image at origin size
    gluOrtho2D(0, windW, 0, windH);
    glMatrixMode(GL_MODELVIEW);
}

static void Key(unsigned char key, int x, int y)
{
    if ((key == 0x1b) || (key == 'q')) {
        printf("Quitting program...\n");
		exit(0);
    } else {
        printf("Unused key value 0x%x! Only ESC or q to exit.\n", key);
    }
}


/* Main program */

/* *************************************************** *
 * WIN32 - needs access to glut32.dll, something like  *
 * PATH=%PATH%;C:\FG\18\3rdParty\bin                   *
 * or to where ever glut32.dll resides                 *
 ***************************************************** */
int main(int argc, char **argv)
{
	char title[MAXLINE];
	/* Read in the file (allocates space for Picture) */
	if (argc < 2) {
		printf ("Enter the name of a PPM or PGM file: ");
		//scanf("%s", filename);
		//readPPM ((char *)filename, &Picture);
        return 1;
	} else {
        /* just assume first argument is a file name! */
		readPPM (argv[1], &Picture);
	}

    /* Initialization: create window */
    glutInit(&argc, argv);
	glutInitWindowPosition(0, 0); 
	glutInitWindowSize(windW, windH);
	//glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE); // no use DOUBLE
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    sprintf(title,"Image %s %dx%d", argv[1], windW, windH);
    if (glutCreateWindow(title) == GL_FALSE) {
        printf("Error: glutCreateWindow failed! aborting...\n");
		exit(1);
    }

    glutReshapeFunc(Resize); // start as PPM size, but allow resizing
    glutDisplayFunc(Draw);   // draw the image
	glutKeyboardFunc(Key);   // keyboard input

    // these could go in Draw
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    printf("Displaying %s - ESC to quit program\n", title);
    glutMainLoop();

    return 0;
}

/* eof */
