/* 
   texture.cxx
 */

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

#include "ac3d.hxx"

#ifndef SEEK_SET
#  define SEEK_SET 0
#endif

#define GLuint unsigned int
#define GLint int
#define GL_FALSE (0)
#define GL_TRUE (!GL_FALSE)

#define ALPHA_NONE   (0x0000)       /* no alpha info */
#define ALPHA_OPAQUE (0x0001<<0)    /* alpha = 1 */
#define ALPHA_INVIS  (0x0001<<1)    /* alpha = 0 */
#define ALPHA_TRANSP (0x0004<<2)    /* 0 < alpha < 1 */

static const char *mod_name = "texture.cxx";
static char _s_file_name_buff[264];

/******************************************************************************/

typedef struct _rawImageRec {
    unsigned short imagic;
    unsigned short type;
    unsigned short dim;
    unsigned short sizeX, sizeY, sizeZ;
    unsigned long min, max;
    unsigned long wasteBytes;
    char name[80];
    unsigned long colorMap;
    FILE *file;
    unsigned char *tmp, *tmpR, *tmpG, *tmpB, *tmpA;
    unsigned long rleEnd;
    GLuint *rowStart;
    GLint *rowSize;
} rawImageRec;


/******************************************************************************/

Private void ConvertShort(unsigned short *array, long length)
{
    unsigned long b1, b2;
    unsigned char *ptr;

    ptr = (unsigned char *)array;
    while (length--) {
	b1 = *ptr++;
	b2 = *ptr++;
	*array++ = (unsigned short)((b1 << 8) | (b2));
    }
}

Private void ConvertLong(GLuint *array, long length)
{
    unsigned long b1, b2, b3, b4;
    unsigned char *ptr;

    ptr = (unsigned char *)array;
    while (length--) {
	b1 = *ptr++;
	b2 = *ptr++;
	b3 = *ptr++;
	b4 = *ptr++;
	*array++ = (b1 << 24) | (b2 << 16) | (b3 << 8) | (b4);
    }
}

Private rawImageRec *RawImageOpen(char *fileName)
{
    union {
	int testWord;
	char testByte[4];
    } endianTest;
    rawImageRec *raw;
    int swapFlag;
    int x;
    size_t size;

    endianTest.testWord = 1;
    if (endianTest.testByte[0] == 1) {
	swapFlag = GL_TRUE;
    } else {
	swapFlag = GL_FALSE;
    }

    size = sizeof(rawImageRec);
    raw = (rawImageRec *)malloc(size);
    if (raw == NULL) {
	    fprintf(stderr, "%s: Out of memory, allocating %ld bytes for rawImageRec!\n",
            mod_name, size);
	    return(NULL);
    }
    if ((raw->file = fopen(fileName, "rb")) == NULL) {
        /* FAILED to open file... 
           try the path of the *.ac file,
           or others supplied */
        char *fn = ac_get_texture_paths();
        if (ac_verb9()) {
            printf("%s: RawImageOpen: Failed to open [%s] %s\n", mod_name, fileName,
            (fn ? "but have paths to try..." : "and have no paths to try!") );
        }
        if (fn) {
            int len, c;
            char *tb = _s_file_name_buff;
            while (*fn) {
                len = (int)strlen(fn);
                strcpy(tb,fn);
                c = tb[len-1];  // get last char
                if ( !((c == '/')||(c =='\\')) ) {
#if _MSC_VER
                    strcat(tb,"\\");
#else
                    strcat(tb,"/");
#endif
                }
                strcat(tb,fileName);
                raw->file = fopen(tb, "rb");
                fn = fn + len + 1;  /* get to next path, if any - 
                                      a double ZERO terminated list */
                if (raw->file != NULL) {
                    fileName = tb;
                    break; /* found a texture file */
                }
                if (ac_verb9()) {
                    printf("[%s] also failed!\n", tb);
                }
            }
            if (raw->file == NULL) {
                perror(fileName);
	            return(NULL);
            }
        } else {
            perror(fileName);
	        return(NULL);
        }
    }
    if (ac_verb9()) printf("Loading [%s]\n", fileName);

    fread(raw, 1, 12, raw->file);

    if (swapFlag) {
	ConvertShort(&raw->imagic, 6);
    }

    size = raw->sizeX * 256;
    if (ac_verb9()) printf("%s: Allocationg %ld bytes for image read...\n", mod_name, (size * 5));
    raw->tmp = (unsigned char *)malloc(raw->sizeX*256);
    raw->tmpR = (unsigned char *)malloc(raw->sizeX*256);
    raw->tmpG = (unsigned char *)malloc(raw->sizeX*256);
    raw->tmpB = (unsigned char *)malloc(raw->sizeX*256);
    raw->tmpA = (unsigned char *)malloc(raw->sizeX*256);
    if (raw->tmp == NULL || raw->tmpR == NULL || raw->tmpG == NULL ||
	raw->tmpB == NULL) {
        if (raw->tmp) free(raw->tmp);
        if (raw->tmpR) free(raw->tmpR);
        if (raw->tmpG) free(raw->tmpG);
        if (raw->tmpB) free(raw->tmpB);
        if (raw->tmpA) free(raw->tmpA);
	    fprintf(stderr, "%s: Out of memory! Allocating 5 x %ld for t+rgba\n",
            mod_name, size);
	    return(NULL);
    }

    if ((raw->type & 0xFF00) == 0x0100) {
	x = raw->sizeY * raw->sizeZ * sizeof(GLuint);
	raw->rowStart = (GLuint *)malloc(x);
	raw->rowSize = (GLint *)malloc(x);
	if (raw->rowStart == NULL || raw->rowSize == NULL) {
        if (raw->rowStart) free(raw->rowStart);
        if (raw->rowSize) free(raw->rowSize);
	    fprintf(stderr, "%s: Out of memory! Allocating 2 x %d for rows\n",
            mod_name, x);
	    return(NULL);
	}
	raw->rleEnd = 512 + (2 * x);
	fseek(raw->file, 512, SEEK_SET);
	fread(raw->rowStart, 1, x, raw->file);
	fread(raw->rowSize, 1, x, raw->file);
	if (swapFlag) {
	    ConvertLong(raw->rowStart, x/sizeof(GLuint));
	    ConvertLong((GLuint *)raw->rowSize, x/sizeof(GLint));
	}
    }
    return raw;
}

Private void RawImageClose(rawImageRec *raw)
{

    fclose(raw->file);
    free(raw->tmp);
    free(raw->tmpR);
    free(raw->tmpG);
    free(raw->tmpB);
    free(raw);
}

Private void RawImageGetRow(rawImageRec *raw, unsigned char *buf, int y, int z)
{
    unsigned char *iPtr, *oPtr, pixel;
    int count;

    if ((raw->type & 0xFF00) == 0x0100) {
	fseek(raw->file, raw->rowStart[y+z*raw->sizeY], SEEK_SET);
	fread(raw->tmp, 1, (unsigned int)raw->rowSize[y+z*raw->sizeY],
	      raw->file);

	iPtr = raw->tmp;
	oPtr = buf;
	while (1) {
	    pixel = *iPtr++;
	    count = (int)(pixel & 0x7F);
	    if (!count) {
		return;
	    }
	    if (pixel & 0x80) {
		while (count--) {
		    *oPtr++ = *iPtr++;
		}
	    } else {
		pixel = *iPtr++;
		while (count--) {
		    *oPtr++ = pixel;
		}
	    }
	}
    } else {
	fseek(raw->file, 512+(y*raw->sizeX)+(z*raw->sizeX*raw->sizeY),
	      SEEK_SET);
	fread(buf, 1, raw->sizeX, raw->file);
    }
}

Private int RawImageGetData(rawImageRec *raw, ACImage *final)
{
    unsigned char *ptr;
    int i, j;
    size_t size = (raw->sizeX+1)*(raw->sizeY+1)*raw->sizeZ;
    final->data = (unsigned char *)myalloc(size);
    if (final->data == NULL) {
	    fprintf(stderr, "%s: Out of memory! allocating %ld bytes for final.\n",
            mod_name, size);
	    return -1;
    }

    ptr = (unsigned char *)final->data;

/*
  debugf("raw image depth %d", raw->sizeZ);
*/
    if (raw->sizeZ == 1) {
	for (i = 0; i < raw->sizeY; i++) {
	    RawImageGetRow(raw, raw->tmpR, i, 0);
	    for (j = 0; j < raw->sizeX; j++) { /* packing */
		*ptr++ = *(raw->tmpR + j);
		/**ptr++ = *(raw->tmpR + j);
		 *ptr++ = *(raw->tmpR + j);
		 *ptr++ = 255;*/
	    }
	}
    }
    if (raw->sizeZ == 2) {
	for (i = 0; i < raw->sizeY; i++) {
	    RawImageGetRow(raw, raw->tmpR, i, 0);
	    RawImageGetRow(raw, raw->tmpA, i, 1);
	    for (j = 0; j < raw->sizeX; j++) { /* packing */
		*ptr++ = *(raw->tmpR + j);
		/**ptr++ = *(raw->tmpR + j);
		 *ptr++ = *(raw->tmpR + j);*/
		*ptr++ = *(raw->tmpA + j);

		final->amask |= ((*(raw->tmpA + j) == 255) ? ALPHA_OPAQUE : 0);
		final->amask |= ((*(raw->tmpA + j) == 0) ? ALPHA_INVIS : 0);
		final->amask |= (((*(raw->tmpA + j)>0) && (*(raw->tmpA + j)<255)) ? ALPHA_TRANSP : 0);
	    }
	}
    }
    else if (raw->sizeZ == 3) {
	for (i = 0; i < raw->sizeY; i++) {
	    RawImageGetRow(raw, raw->tmpR, i, 0);
	    RawImageGetRow(raw, raw->tmpG, i, 1);
	    RawImageGetRow(raw, raw->tmpB, i, 2);
	    for (j = 0; j < raw->sizeX; j++) { /* packing */
		*ptr++ = *(raw->tmpR + j);
		*ptr++ = *(raw->tmpG + j);
		*ptr++ = *(raw->tmpB + j);
		/**ptr++ = 255;*/
	    }
	}
    }
    else if (raw->sizeZ == 4) {
	for (i = 0; i < raw->sizeY; i++) {
	    RawImageGetRow(raw, raw->tmpR, i, 0);
	    RawImageGetRow(raw, raw->tmpG, i, 1);
	    RawImageGetRow(raw, raw->tmpB, i, 2);
	    RawImageGetRow(raw, raw->tmpA, i, 3);
	    for (j = 0; j < raw->sizeX; j++) { /* packing */
		*ptr++ = *(raw->tmpR + j);
		*ptr++ = *(raw->tmpG + j);
		*ptr++ = *(raw->tmpB + j);
		*ptr++ = *(raw->tmpA + j);

		final->amask |= ((*(raw->tmpA + j) == 255) ? ALPHA_OPAQUE : 0);
		final->amask |= ((*(raw->tmpA + j) == 0) ? ALPHA_INVIS : 0);
		final->amask |= (((*(raw->tmpA + j)>0) && (*(raw->tmpA + j)<255)) ? ALPHA_TRANSP : 0);
	    }
	}
    }
    return 0;   // success
}

/* This was initially set at 100!!! From Steve Bakers 1998 ac_to_gl program, he uses 
 *  #define MAX_TEXTURES 10000    /==* This *ought* to be enough! *==/
 *  Ok, for now will try half that ;=))
 */
#define MAX_TEXTURES 5000
static ACImage texture[MAX_TEXTURES+1];
static int num_texture = 0;

Prototype ACImage *ac_get_texture(int ind)
{
    return(&texture[ind]);
}


Prototype int ac_load_rgb_image(char *fileName)
{
    rawImageRec *raw;
    ACImage *final;
    int res;

    if (ac_verb9()) printf("%s: Loading texture: %s\n", mod_name, fileName);

    raw = RawImageOpen(fileName);
    if (raw == NULL)
    {
        fprintf(stderr, "%s: error opening rgb file [%s]\n", mod_name, fileName);
        return(-1);
    }

    final = &texture[num_texture];
    if (num_texture == MAX_TEXTURES)
    {
    	printf("%s: ERROR: out of texture space - change MAX_TEXTURES %d in texture.c\n", mod_name, MAX_TEXTURES);
	    exit(1);
    }

    final->width = raw->sizeX;
    final->height = raw->sizeY;
    final->depth = raw->sizeZ;

    res = RawImageGetData(raw, final);
    RawImageClose(raw); // close and free memory
    if (res) {
        // failed, oh well
        return -1;
    }

    if (ac_verb9()) printf("%s: loaded texture %dx%d (%d)\n", mod_name, final->width, final->height, final->depth);

    num_texture++;
    return(num_texture - 1);
}

/* eof - texture.cxx */
