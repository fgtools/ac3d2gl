/* ======================================================================
    modules : ac3d.cxx
    from : README.txt
    AC3DVIEW version 1.14 

    All the files here are free of copyright so you can use them as you 
    please.  If you do use them them please give credit to AC3D. www.ac3d.org

    1.11 added cdecl Prototype for WIN32 
    1.12 added ac_object_free
    1.13 reduced the default rotation speed; fixed fatal bug with argc when file 
         name was passed to program on commandline
    1.14 2013/05/06 - geoff
        Add keyboard interface to control position, orientation and rotation of
        view, and added a WritePPM dump capability
        Added 'crease %f' and 'subdiv %d' to ac3d.c parsing

   ====================================================================== */
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <vector>

#include "ac3d.hxx"

static const char *mod_name = "ac3d.cxx";

static int line = 0;
static char buff[MX_LINE_BUFF+1];
static ACObject *first_ob = 0;
static ACMaterial palette[MX_MATERIALS+1];
static int num_palette = 0;
static int startmatindex = 0;
static char *file_name = 0; // input file name
static int paths_len = 0;
static char *texture_paths = 0; // any added texture paths
static int verbosity = 1;
static int show_sscan_warn = 0;
static int do_texture_load = 1;
static int add_export_norms = 0;
static float def_scale = 1.0f;
static int next_id = 0;

Prototype int ac_set_verbosity( int v )
{ 
    int i = verbosity;
    verbosity = v;
    return i;
}
Prototype int ac_get_verbosity( void ) { return verbosity; }
Prototype int ac_verb1( void ) { return (verbosity >= 1 ? 1 : 0); }
Prototype int ac_verb2( void ) { return (verbosity >= 2 ? 1 : 0); }
Prototype int ac_verb5( void ) { return (verbosity >= 5 ? 1 : 0); }
Prototype int ac_verb9( void ) { return (verbosity >= 9 ? 1 : 0); }

Prototype int ac_load_textures()  { return do_texture_load; }
Prototype void ac_set_load_textures(int v) { do_texture_load  = v; }
Prototype void ac_set_add_normals(int v)   { add_export_norms = v; }
Prototype float ac_get_scale() { return def_scale; }
Prototype void ac_set_scale(float f) { def_scale = f; }
Prototype int ac_palette_get_mat_size() { return num_palette; }

/* ------------------------------------------------------------
 * add a user given texture path
 * also include the path to the .ac file
 * ------------------------------------------------------------ */
Prototype void ac_add_texture_paths( char *path )
{
    int len = (int)strlen(path);
    if (len) {
        if (texture_paths) {
            texture_paths = (char *)myrealloc(texture_paths, paths_len + len + 1);
            if (!texture_paths) {
                printf("%s: ERROR: memory re-allocation FAILED %d bytes! aborting...\n",
                    mod_name, paths_len + len + 1);
                exit(1);
            }
            strcpy(&texture_paths[paths_len-1],path);
            paths_len += len + 1;
            texture_paths[paths_len] = 0;
        } else {
            paths_len = len + 2;
            texture_paths = (char *)myalloc( len + 2 );
            if (!texture_paths) {
                printf("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                    mod_name, len + 2);
                exit(1);
            }
            strcpy(texture_paths,path);
            texture_paths[len] = 0;     /* double ZERO termination */
            texture_paths[len+1] = 0;
        }
    }
}


Prototype void ac_set_file_name( char *name )
{ 
    int i, c, len;
    file_name = STRING(name);
    len = (int)strlen(file_name);
    for (i = len - 1; i >= 0; i--) {
        c = file_name[i];
        if ((c == '/')||(c == '\\')) {
            file_name[i] = 0; /* zero it */
            ac_add_texture_paths(file_name);
            file_name[i] = (char)c; /* put it back */
            break;
        }
    }
}

/* note: this can be a double zero terminated list of paths */
Prototype char *ac_get_texture_paths(void) { return texture_paths; }

ACMaterial *ac_palette_get_material(int id)
{
    if (id < num_palette) // ensure value in range
        return(&palette[id]);
    return 0;
}


Boolean read_line(FILE *f)
{
    fgets(buff, MX_LINE_BUFF, f);
    line++;
    return(TRUE);
}


#define MX_TOKEN_PTRS 30
static int tokc = 0;
static char *tokv[MX_TOKEN_PTRS+1];

/** bung '\0' chars at the end of tokens and set up the array (tokv) and count (tokc)
	like argv argc **/
Prototype int get_tokens(char *s, int *argc, char *argv[])
{
    char *p = s;
    char *st;
    char c;
    //int n;
    int tc;

    tc = 0;
    while ((c=*p) != 0)
    {
	    if ((c != ' ') && (c != '\t') && (c != '\n') && ( c != 13))
	    {
	        if (c == '"')
	        {
		        c = *p++;
		        st = p;
		        while ((c = *p) && ((c != '"')&&(c != '\n')&& ( c != 13)) )
		        {
		            if (c == '\\')
			        strcpy(p, p+1);
		            p++;
		        }

		        *p=0;

		        argv[tc++] = st;

                if (tc >= MX_TOKEN_PTRS) {
                    printf("%s: ERROR: Maximum tokens reached %d. Change MX_TOKEN_PTRS and recompile! aborting...\n",
                        mod_name, tc);
                    exit(1);
                }
	        }
	        else
	        {
		        st = p;
		        while ((c = *p) && ((c != ' ') && (c != '\t') && (c != '\n') && ( c != 13)) )
		            p++;

		        *p=0;

		        argv[tc++] = st;

                if (tc >= MX_TOKEN_PTRS) {
                    printf("%s: ERROR: Maximum tokens reached %d. Change MX_TOKEN_PTRS and recompile! aborting...\n",
                        mod_name, tc);
                    exit(1);

                }
	        }			
	    }
	    p++;
    }

    *argc = tc;
    return(tc);
}

/* ------------------------------------------------------------------------
    Allocate a new object, and set the defaults
   ------------------------------------------------------------------------ */
ACObject *new_object()
{
    size_t size = sizeof(ACObject);
    ACObject *ob = (ACObject *)myalloc(size);
    if (!ob) {
        printf("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                    mod_name, (int)size);
        exit(1);
    }
    memset(ob,0,size);  /* zero it all */
    next_id++;
    ob->order = next_id;    // set a unique id/order of this object
    ob->loc.x = ob->loc.y = ob->loc.z = 0.0;
    ob->name = ob->url = NULL;
    ob->data = NULL;
    ob->vertices = NULL;
    ob->num_vert = 0;
    ob->surfaces = NULL;
    ob->num_surf = 0;
    ob->texture = -1; /* NO texture */
    ob->texture_repeat_x = ob->texture_repeat_y = 1.0;
    ob->texture_offset_x = ob->texture_offset_y = 0.0;
    ob->kids = NULL;
    ob->num_kids = 0;
    ob->matrix[0] = 1;
    ob->matrix[1] = 0;
    ob->matrix[2] = 0;
    ob->matrix[3] = 0;
    ob->matrix[4] = 1;
    ob->matrix[5] = 0;
    ob->matrix[6] = 0;
    ob->matrix[7] = 0;
    ob->matrix[8] = 1;
    ob->crease = 61.0f;  /* 2013/05/06 added - def from OSG ac3d.cpp */
    ob->subdiv = -1;     /* 2013/05/06 added - -1 means not existing */
    if (!first_ob) {
        first_ob = ob;  /* store the FIRST object */
    }
    return(ob);
}

/* ---------------------------------------------------------------
   Free an object
   --------------------------------------------------------------- */
void ac_object_free(ACObject *ob)
{
    int n, s;

	for (n = 0; n < ob->num_kids; n++)
		ac_object_free(ob->kids[n]);

	if (ob->vertices)
		myfree(ob->vertices);

	for (s = 0; s < ob->num_surf; s ++)
	{
		myfree(ob->surfaces[s].vertref);
		myfree(ob->surfaces[s].uvs);
	}

	if (ob->surfaces)
		myfree(ob->surfaces);

	if (ob->data)
		myfree(ob->data);

	if (ob->url)
		myfree(ob->url);

	if (ob->name)
		myfree(ob->name);

	myfree(ob);

}

Prototype void ac_free_objects()   /* free ALL memory */
{
    int n;
    if (first_ob) {
        ac_object_free(first_ob); // this will also free all 'kids'
        first_ob = 0;
        for (n = 0; n < num_palette; n++) {
            ACMaterial m = palette[n];
            myfree(m.name);
        }
    }
    next_id = 0;
    num_palette = 0;
    startmatindex = 0;
}


void init_surface(ACSurface *s)
{
    s->vertref = NULL;
    s->uvs = NULL;
    s->num_vertref = 0;
    s->flags = 0;
    s->mat = 0;
    s->mindx = 0;
    s->normal.x = 0.0;
    s->normal.y = 0.0;
    s->normal.z = 0.0; 
}

/* ---------------------------------------------------------------------
    Given 3 vector points (x1,y1,z1), (x2,y2,z2), (x3,y3,z3), then
    normal_x = (y2 - y1) * (z3 - z1) - (z2 - z1) * (y3 - y1);
    normal_y = (z2 - z1) * (x3 - x1) - (y2 - y1) * (z3 - z1);
    normal_z = (x2 - x1) * (y3 - y1) - (x2 - x1) * (x3 - x1);
    length   = sqrt( normal_x * normal_x + normal_y * normal_y + normal_z * normal_z )
    if (length > 0.0) // normalise - reduce to unit 1 = x + y + z
        normal_x /= lenght; normal_y /= length; normal_y /= length;
   --------------------------------------------------------------------- */
void tri_calc_normal(ACPoint *v1, ACPoint *v2, ACPoint *v3, ACPoint *n)
{
    double len;

    n->x = (v2->y - v1->y) * (v3->z - v1->z) - (v3->y - v1->y) * (v2->z - v1->z);
    n->y = (v2->z - v1->z) * (v3->x - v1->x) - (v3->z - v1->z) * (v2->x - v1->x);
    n->z = (v2->x - v1->x) * (v3->y - v1->y) - (v3->x - v1->x) * (v2->y - v1->y);
    len = sqrt(n->x * n->x + n->y * n->y + n->z * n->z);

    if (len > 0)
    {
    	n->x /= (float)len;
	    n->y /= (float)len;
	    n->z /= (float)len;  
    }

}

/* ------------------------------------------------------------
   Read an AC3D surface entry.
   "SURF", "mat", "refs" - exit as end of refs count
   Technically these should be in strict order... but presently 
   not checked, except 'refs' assumed to be the last, 
   and is forgiving if some other token found
   ----------------------------------------------------------- */
ACSurface *read_surface(FILE *f, ACSurface *s, ACObject *ob)
{
    char t[64]; /* was 20! */

    init_surface(s);

    while (!feof(f))
    {
        read_line(f);
        sscanf(buff, "%s", t);
        if (streq(t, "SURF"))
        {
            int flgs;
            if (get_tokens(buff, &tokc, tokv) != 2)
            {
		        printf("%s: %d: WARNING: SURF should be followed by one flags argument\n",
                    mod_name, line);
            }
            else
            {
                flgs = strtol(tokv[1], NULL, 0);
		        s->flags = flgs;
            }
        }
        else
	    if (streq(t, "mat"))
	    {
		    int mindx, res;
            
		    res = sscanf(buff, "%s %d", t, &mindx);
            if ((res != 2) && show_sscan_warn) {
                printf("%s: WARNING: sscanf of '%s' not 2! got %d\n",
                    mod_name, buff, res);
            }
		    s->mat = mindx+startmatindex;
            s->mindx = mindx;
	    }
	    else
		if (streq(t, "refs"))
		{
		    int num, n, len;
		    int ind, res;
		    float tx, ty;
  
            /* why no check of count? */
		    res = sscanf(buff, "%s %d", t, &num);        
            if ((res != 2) && show_sscan_warn) {
                printf("%s: WARNING: sscanf of '%s' not 2! got %d\n",
                    mod_name, buff, res);
            }
		    s->num_vertref = num;
		    s->vertref = (int *)malloc( num * sizeof(int));
		    s->uvs = (ACUV *)malloc( num * sizeof(ACUV));

		    for (n = 0; n < num; n++)
		    {
                /* why no check of count? */
			    res = fscanf(f, "%d %f %f\n", &ind, &tx, &ty);
                if ((res != 3) && show_sscan_warn) {
                    printf("%s: WARNING: sscanf of '%s' not 2! got %d\n",
                        mod_name, buff, res);
                }
                line++;
			    s->vertref[n] = ind;
			    s->uvs[n].u = tx;
			    s->uvs[n].v = ty;
		    }

		    /** calc surface normal - 
                why if only >= 3??? and 
                why only the first 3 vertices used to get the normal for the whole surface?
                20130516: Now check the 'index' is in range of the vertices array **/
		    if (s->num_vertref >= 3) 
            {
                int ind1 = s->vertref[0];
                int ind2 = s->vertref[1];
                int ind3 = s->vertref[2];
                len = ob->num_vert;
                //if (len) {
                if ((ind1 < len) && (ind2 < len) && (ind3 < len)) {
    			    tri_calc_normal((ACPoint *)&ob->vertices[ind1], 
					    (ACPoint *)&ob->vertices[ind2], 
					    (ACPoint *)&ob->vertices[ind3],
                        (ACPoint *)&s->normal);
                } else {
                    if (len) {
            	        printf("%s: %d: WARNING: 'refs %d' but index to 'numvert %d' out of range! Got %d %d %d - No normal gen'd.\n",
                            mod_name, line, num, len, ind1, ind2, ind3 );
                    } else {
            	        printf("%s: %d: WARNING: got 'refs %d' but no earlier 'numvert ?'! So no normal generated.\n",
                            mod_name, line, num);
                    }
                }
            }
		    return(s);
		}
		else 
        {
	        printf("%s: %d: WARNING: ignoring '%s'\n",
                mod_name, line, t);
        }
    }   /* while (!feof(f)) */

    return(NULL);
}

void ac_object_calc_vertex_normals(ACObject *ob)
{
    int s, v, vr;

    /** for each vertex in this object **/
    for (v = 0; v < ob->num_vert; v++) {
	    ACNormal n = {0, 0, 0};
	    int found = 0;

	    /** go through each surface **/
	    for (s = 0; s < ob->num_surf; s++) {
	        ACSurface *surf = &ob->surfaces[s];

	        /** check if this vertex is used in this surface **/
	        /** if it is, use it to create an average normal **/
	        for (vr = 0; vr < surf->num_vertref; vr++) {
		        if (surf->vertref[vr] == v) {
		            n.x+=surf->normal.x;
		            n.y+=surf->normal.y;
		            n.z+=surf->normal.z;
		            found++;
		        }
            }
	    }
	    if (found > 0) {
	        n.x /= found;
	        n.y /= found;
	        n.z /= found;
	    }
	    ob->vertices[v].normal = n;
    }

}

Prototype const char *ac_objecttype_to_string(int t)
{
    if (t == OBJECT_WORLD) return "world";
    if (t == OBJECT_NORMAL) return "poly";
    if (t == OBJECT_GROUP) return "group";
    if (t == OBJECT_LIGHT) return "light";
    return "poly";  // assume normal???
}


int string_to_objecttype(char *s)
{
    if (streq("world", s))
        return(OBJECT_WORLD);
    if (streq("poly", s))
        return(OBJECT_NORMAL);
    if (streq("group", s))
        return(OBJECT_GROUP);
    if (streq("light", s))
        return(OBJECT_LIGHT);
    return(OBJECT_NORMAL);
}

/* ----------------------------------------------------------------
   Primary AC3D parsing
    "MATERIAL", "OBJECT", "data", "name", "texture", "texrep",
	"texoff", "rot", "loc", "url", "numvert", "numsurf",
    "crease", "kids"

   ---------------------------------------------------------------- */
ACObject *ac_load_object(FILE *f, ACObject *parent)
{
    ACObject *ob = NULL;
    int     tc, res;
    char    type[32];
    char    strg[32];
    char    *pstr;
	int     len, n, num;
    char    t[32];
    float   rot[9];

    while (!feof(f)) 
    {
        read_line(f);

	    res = sscanf(buff, "%s", t);
        if ((res != 1) && show_sscan_warn) {
            printf("%s: WARNING: sscanf of '%s' not 1! got %d\n",
                mod_name, buff, res);
        }

        if (streq(t, "MATERIAL"))
	    {
            // float shi, tran;
            ACMaterial m;
	        tc = get_tokens(buff, &tokc, tokv); 
            if ( tc != 22)
	        {
                printf("%s: %d: WARNING: expected 21 params after \"MATERIAL\", got %d!\n",
                    mod_name, line, tc );
	        }
            else
	        {
                if (num_palette >= MX_MATERIALS) {
                    printf("%s: ERROR: Reached maximum materials %d. Increase MX_MATERIALS and recompile!\n", 
                        mod_name, num_palette);
                    exit(1);
                }
                m.name = STRING(tokv[1]);
		        m.rgb.r = (float)atof(tokv[3]);
		        m.rgb.g = (float)atof(tokv[4]);
		        m.rgb.b = (float)atof(tokv[5]);

		        m.ambient.r = (float)atof(tokv[7]);
		        m.ambient.g = (float)atof(tokv[8]);
		        m.ambient.b = (float)atof(tokv[9]);

		        m.emissive.r = (float)atof(tokv[11]);
		        m.emissive.g = (float)atof(tokv[12]);
		        m.emissive.b = (float)atof(tokv[13]);

		        m.specular.r = (float)atof(tokv[15]);
		        m.specular.g = (float)atof(tokv[16]);
		        m.specular.b = (float)atof(tokv[17]);

		        m.shininess = (float)atof(tokv[19]);
		        m.transparency = (float)atof(tokv[21]);

		        // shi = (float)atof(tokv[6]);
		        // tran = (float)atof(tokv[7]);

		        palette[num_palette++] = m;

	        }
	    }
        else
	    if (streq(t, "OBJECT"))
        {
		    ob = new_object();

		    res = sscanf(buff, "%s %s", strg, type);
            if ((res != 2) && show_sscan_warn) {
                printf("%s: WARNING: sscanf of '%s' not 2! got %d\n",
                    mod_name, buff, res);
            }
		    ob->type = string_to_objecttype(type);
        }
	    else
		if (streq(t, "data"))
		{
            tc = get_tokens(buff, &tokc, tokv); 
		    if ( tc != 2) 
            {
    			printf("%s: %d: WARNING: expected 'data <number>'! got %d tokens\n", mod_name, line, tc);
            }
		    else
		    {
			    len = atoi(tokv[1]);
			    if (len > 0)
			    {
			        pstr = (char *)myalloc(len+1);
                    if (!pstr) {
                        printf("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                            mod_name, len + 1);
                        exit(1);
                    }
			        res = fread(pstr, len, 1, f);  // read the data length (from next line)
			        pstr[len] = 0;           // zero terminate the string
			        fscanf(f, "\n");        // get to next line
                    line++;
			        ob->data = STRING(pstr);
			        myfree(pstr);
			    }
		    }
		}
		else
	    if (streq(t, "name"))
	    {
			tc = get_tokens(buff, &tokc, tokv);
			if (tc != 2)
			{
			    printf("%s: %d: WARNING: expected quoted name (got %d tokens)\n",
                    mod_name, line, tc);
			}
			else {
			    ob->name = STRING(tokv[1]);
            }
	    }
	    else
		if (streq(t, "texture"))
		{
            tc = get_tokens(buff, &tokc, tokv);
            if (tc != 2) {
				printf("%s: %d: WARNING: expected quoted texture name (got %d tokens)\n", 
                    mod_name, line, tc);
            }
            else if (ac_load_textures())
			{
				ob->texture = ac_load_texture(tokv[1]);
			}
		}
		else
		if (streq(t, "texrep"))
		{
            tc = get_tokens(buff, &tokc, tokv); 
		    if ( tc != 3) {
			    printf("%s: %d: WARNING: expected 'texrep <float> <float>' (got %d tokens)\n", 
                    mod_name, line, tc);
            }
            else
			{
                ob->texture_repeat_x = (float)atof(tokv[1]);
				ob->texture_repeat_y = (float)atof(tokv[2]);
			}
		}
		else
		if (streq(t, "texoff"))
        {
            tc = get_tokens(buff, &tokc, tokv); 
            if ( tc != 3) {
				printf("%s: %d: WARNING: expected 'texoff <float> <float>' got %d tokens\n",
                    mod_name, line, tc);
            }
            else
            {
                ob->texture_offset_x = (float)atof(tokv[1]);
				ob->texture_offset_y = (float)atof(tokv[2]);
            }
		}
		else
	    if (streq(t, "rot"))
	    {
			int n;
            /* why no check of count? */
			res = sscanf(buff, "%s %f %f %f %f %f %f %f %f %f", strg, 
                &rot[0], &rot[1], &rot[2], &rot[3], &rot[4], &rot[5], &rot[6], &rot[7], &rot[8] );
            if ((res != 10) && show_sscan_warn) {
                printf("%s: WARNING: sscanf of '%s' not 10! got %d\n",
                    mod_name, buff, res);
            }
            for (n = 0; n < 9; n++) {
                ob->matrix[n] = rot[n];
            }
        }
        else
        if (streq(t, "loc"))
        {
            /* why no check of count? */
            res = sscanf(buff, "%s %f %f %f", strg,
                &ob->loc.x, &ob->loc.y, &ob->loc.z);
            if ((res != 4) && show_sscan_warn) {
                printf("%s: WARNING: sscanf of '%s' not 4! got %d\n",
                    mod_name, buff, res);
            }
        }
        else
        if (streq(t, "url"))
        {
            tc = get_tokens(buff, &tokc, tokv); 
            if ( tc != 2) {
                printf("%s: %d: WARNING: expected one arg to url (got %d tokens)\n", 
                    mod_name, line, tc);
            }
            else
            {
                ob->url = STRING(tokv[1]);
            }
        }
        else
        if (streq(t, "numvert"))
        {
            num = 0;
            /* why no check of count? */
            res = sscanf(buff, "%s %d", strg, &num);
            if ((res != 2) && show_sscan_warn) {
                printf("%s: WARNING: sscanf of '%s' not 2! got %d\n",
                    mod_name, buff, res);
            }
            if (num > 0)
            {
                ob->num_vert = num;
                ob->vertices = (ACVertex *)myalloc(sizeof(ACVertex)*num);
                if (!ob->vertices) {
                    printf("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                        mod_name, (int)(sizeof(ACVertex)*num));
                    exit(1);
                }
                for (n = 0; n < num; n++)
                {
                    ACVertex p;
                    res = fscanf(f, "%f %f %f\n", &p.x, &p.y, &p.z);
                    if ((res != 3) && show_sscan_warn) {
                        printf("%s: WARNING: sscanf of '%s' not 3! got %d\n",
                            mod_name, buff, res);
                    }
                    line++;
                    ob->vertices[n] = p;
                }
            }
        }
        else
        if (streq(t, "numsurf"))
        {
            num = 0;
            /* why no check of count? */
			res = sscanf(buff, "%s %d", strg, &num);
			if (num > 0)
            {
                ob->num_surf = num;
				ob->surfaces = (ACSurface *)myalloc(sizeof(ACSurface) * num);
                if (!ob->surfaces) {
                    printf("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                        mod_name, (int)(sizeof(ACSurface) * num));
                    exit(1);
                }
                for (n = 0; n < num; n++)
                {
                    ACSurface *news = read_surface(f, &ob->surfaces[n], ob);
                    if (news == NULL)
                    {
                        printf("%s: %d: WARNING: error whilst reading surface\n",
                            mod_name, line);
                        return(NULL);
                    }
                }
            }
        }
        else
        if (streq(t, "crease")) /** 2013/05/06 - added 'crease %f' **/
        {
            tc = get_tokens(buff, &tokc, tokv); 
            if ( tc != 2 ) {
                printf("%s: %d: WARNING: expected one arg to crease (got %d tokens)\n", 
                    mod_name, line, tc);
            }
            else
            {
                ob->crease = (float)atof(tokv[1]);
            }

        }
        else
        if (streq(t, "subdiv")) /** 2013/05/06 - added 'subdive %d' **/
        {
            tc = get_tokens(buff, &tokc, tokv); 
            if ( tc != 2 ) {
                printf("%s: %d: WARNING: expected one arg to subdiv (got %d tokens)\n", 
                    mod_name, line, tc);
            }
            else
            {
                ob->subdiv = atoi(tokv[1]);
            }

        }
        else
        if (streq(t, "kids")) /** 'kids' is the last token in an object **/
        {
            num = 0;
            /* why no check of count? */
			res = sscanf(buff, "%s %d", t, &num);
			
			if (num != 0)
            {
                ob->kids = (ACObject **)myalloc(num * sizeof(ACObject *) );
                if (!ob->kids) {
                    printf("%s: ERROR: memory allocation FAILED %d bytes! aborting...\n",
                        mod_name, (int)(num * sizeof(ACObject *)));
                    exit(1);

                }
                ob->num_kids = num;

                for (n = 0; n < num; n++)
                {
                    ACObject *k = ac_load_object(f, ob);
                    ob->kids[n] = k;    /* NULL or not, set the value */
                    if (k == NULL)
                    {
                        printf("%s: %d: WARNING: error reading expected child object %d of %d\n",
                            mod_name, line, n+1, num);
                        ob->num_kids = n;   /* 2013/05/06 REDUCE expected 'kids' count to this */
                        return(ob);
                    }
                }
            }
            return(ob);
        } else {
            printf("%s: %d: WARNING: unparsed token '%s'\n",
                mod_name, line, t );
        }
    }   /*  while (!feof(f)) */

    return(ob);
}

void ac_calc_vertex_normals(ACObject *ob)
{
    int n;
    
    ac_object_calc_vertex_normals(ob);
    if (ob->num_kids) {
	    for (n = 0; n < ob->num_kids; n++) {
	        ac_calc_vertex_normals(ob->kids[n]);
        }
    }
}

/* =========================================================================
   Load an AC3D *.ac file, storing the results in an ACObject structure.
   That structure can then be used to generate a GL display list
   through ac_display_list_render_object(ob).
   ========================================================================= */
ACObject *ac_load_ac3d(char *fname)
{
    FILE *f = fopen(fname, "r");
    ACObject *ret = NULL;

    if (f == NULL)
    {
	    printf("can't open %s\n", fname);
	    return(NULL);
    }

    read_line(f); /* read first line of file */

    if (strncmp(buff, "AC3D", 4))
    {
        /* NO AC3D 'signature', so */
    	printf("%s: ac_load_ac3d('%s') is NOT a valid AC3D file.", mod_name, fname);
	    fclose(f);
	    return(0);
    }


    startmatindex = num_palette;


    ret = ac_load_object(f, NULL);

	
    fclose(f);

    ac_calc_vertex_normals(ret);

    return(ret);
}

/* =================================================================
   A 'simple' text 'dump' of the loaded ac3d object
   ================================================================= */
Prototype void ac_dump_simple(ACObject *ob, FILE *fp)
{
    int n, sr;

    fprintf(fp, "OBJECT name %s\nloc %f %f %f\nnum_vert %d\n",
	   ob->name, ob->loc.x, ob->loc.y, ob->loc.z, ob->num_vert);

    /* show vertices */
    for (n=0; n < ob->num_vert; n++) {
    	fprintf(fp, "\tv %f %f %f\n", ob->vertices[n].x, ob->vertices[n].y, ob->vertices[n].z);
    }
    /* show normals */
    for (n=0; n < ob->num_vert; n++) {
    	fprintf(fp, "\tn %f %f %f\n", ob->vertices[n].normal.x, ob->vertices[n].normal.y, ob->vertices[n].normal.z);
    }

    /* show surfaces */
    fprintf(fp, "num_surf %d\n", ob->num_surf);
    for (n=0; n < ob->num_surf; n++) {
	    ACSurface *s = &ob->surfaces[n];
	    fprintf(fp, "surface %d, %d refs, mat %d\n", (n+1), s->num_vertref, s->mat);
        for (sr = 0; sr < s->num_vertref; sr++) {
			    ACVertex *v = &ob->vertices[s->vertref[sr]];
                fprintf(fp," %d: %d %f %f %f\n", (sr+1), s->vertref[sr], v->x, v->y, v->z );
        }
    }

    /* show any kids */
    if (ob->num_kids > 0) {
	    for (n = 0; n < ob->num_kids; n++) {
	        ac_dump_simple(ob->kids[n], fp); 
        }
    }
			
}

/* =================================================================================
   *********************************************************************************

   EXPERIMENTAL ONLY!

    Attempt to 'dump' .ac file to a Three.js (json) file, for loading the model 
    using WebGL, but NOT all completed yet. At present the Three_min.js parser still 
    sometimes bombs when trying to load this output ;=(( but it seems very 'close'...
    This is mostly emprically coded... just guessing from what works and what does 
    not!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    And there is lots of dead code, failed experiments, etc... needs a lot of clean 
    up, one day, maybe...

   *********************************************************************************
   ================================================================================= */

/* store values in a simple std::vector<> */
typedef std::vector<ACPoint> vACP;
typedef std::vector<ACNormal> vACN;
//typedef std::vector<ACVertex> vACV;
typedef std::vector<int> vINT;

/* bounding x,y,z box */
//typedef struct BBOX_t {
//    int num_verts;
//    float minx, maxx;
//    float miny, maxy;
//    float minz, maxz;
//    ACPoint center; /* model center */
//}BBOX, *PBBOX;
// static BBOX bbox;
static int index_offset = 0;

void ac_dump_vector(ACObject *ob, vACP &vacp, vACN &vacn, vINT &vaci)
{
    int n, s, sr;
    ACPoint acp;
    ACNormal acn;
    for (n=0; n < ob->num_vert; n++) {
        acp.x = ob->vertices[n].x;
        acp.y = ob->vertices[n].y;
        acp.z = ob->vertices[n].z;
        vacp.push_back(acp);
        acn.x = ob->vertices[n].normal.x;
        acn.y = ob->vertices[n].normal.y;
        acn.z = ob->vertices[n].normal.z;
        vacn.push_back(acn);
    }

    for (s = 0; s < ob->num_surf; s++) {
		ACSurface *surf = &ob->surfaces[s];
        //ACNormal acn = surf->normal; 	    /** get surface normal **/
        //vacn.push_back(acn);
	    //for (sr = 0; sr < surf->num_vertref; sr++) {
		    //ACVertex *v = &ob->vertices[surf->vertref[sr]];
            //vacv.push_back(*v);
        //    vaci.push_back(surf->vertref[sr]);
        //}
	    //if ( surf && (surf->num_vertref >= 3) ) {
            //glNormal3fv((GLfloat *)&surf->normal);
            //ACNormal acn = surf->normal;
            //vacn.push_back(acn);
		    for (sr = 0; sr < surf->num_vertref; sr++) {
			    //ACVertex *v = &ob->vertices[surf->vertref[sr]];
			    //if (ob->texture > -1) {
			    //	float tu = surf->uvs[sr].u;
			    //	float tv = surf->uvs[sr].v;
			    //	float tx = ob->texture_offset_x + tu * ob->texture_repeat_x;
			    //	float ty = ob->texture_offset_y + tv * ob->texture_repeat_y;
			    //	glTexCoord2f(tx, ty);
			    //}
			    //if (surf->flags & SURFACE_SHADED)
			    //	glNormal3fv((GLfloat *)&v->normal);
                //glVertex3fv((GLfloat *)v);
                //vacv.push_back(*v);
                //vaci.push_back(surf->vertref[sr]);
                vaci.push_back( (index_offset + surf->vertref[sr]) );
            }
        //}
    }

    index_offset += ob->num_vert;   // bump to next linear index

    /* now do the 'kids', if any */
    for (n = 0; n < ob->num_kids; n++) {
        ac_dump_vector(ob->kids[n], vacp, vacn, vaci); 
    }
}

/* ----------------------------------------------------------
   NOT really sure I know what I am doing here ;=((
   NEED TO BE CHECKED, and fixed if required
   Eventually NOT USED!!!!!!!!!!!!!!!!!!!!!!
   ---------------------------------------------------------- */
void translate_verts( vACP &vacp, ACPoint t )
{
    int ii, max = vacp.size();
    ACPoint *pacp;
    for (ii = 0; ii < max; ii++) {
        pacp = &vacp[ii];
        pacp->x += t.x;
        pacp->y += t.x;
        pacp->z += t.x;
    }
}

void set_bbox( vACP &vacp,  PBBOX pbb)
{
    int ii, max = vacp.size();
    ACPoint acp;
    //PBBOX pbb = &bbox;
    if (max) {
        acp = vacp[0]; /* get and set initial values */
        pbb->maxx = pbb->minx = acp.x;
        pbb->maxy = pbb->miny = acp.y;
        pbb->maxz = pbb->minz = acp.z;
    } else {
        pbb->maxx = pbb->minx = 0;
        pbb->maxy = pbb->miny = 0;
        pbb->maxz = pbb->minz = 0;
    }
    for (ii = 1; ii < max; ii++) {
        acp = vacp[ii];
        /* x */
        if (pbb->maxx < acp.x) pbb->maxx = acp.x;
        if (pbb->minx > acp.x) pbb->minx = acp.x;
        /* y */
        if (pbb->maxy < acp.y) pbb->maxy = acp.y;
        if (pbb->miny > acp.y) pbb->miny = acp.y;
        /* z */
        if (pbb->maxz < acp.z) pbb->maxz = acp.z;
        if (pbb->minz > acp.z) pbb->minz = acp.z;
    }

    /* set the CENTER */
    pbb->center.x = -(pbb->minx + ((pbb->maxx - pbb->minx) / 2));
    pbb->center.y = -(pbb->miny + ((pbb->maxy - pbb->miny) / 2));
    pbb->center.z = -(pbb->minz + ((pbb->maxz - pbb->minz) / 2));

    /* is this correct - TODO */
    translate_verts( vacp, pbb->center );
}

enum Face_Bits {
    No_Bits  = 0x00,
    QuadBit  = 0x01,
    MatBit   = 0x02,
    FaceUv   = 0x04,
	VertUv   = 0x08,
    FaceNorm = 0x10,
	VertNorm = 0x20,
	FaceColr = 0x40,
    VertColr = 0x80
};


// YOWEE - Each set starts with a FLAG value
//  $type = ${$ra}[$offset++];
//	$isQuad              = isBitSet( $type, 0 );
//	$hasMaterial         = isBitSet( $type, 1 );
//	$hasFaceUv           = isBitSet( $type, 2 );
//	$hasFaceVertexUv     = isBitSet( $type, 3 );
//	$hasFaceNormal       = isBitSet( $type, 4 );
//	$hasFaceVertexNormal = isBitSet( $type, 5 );
//	$hasFaceColor	     = isBitSet( $type, 6 );
//	$hasFaceVertexColor  = isBitSet( $type, 7 );
// $max = 3;
// if ($isQuad) { $max = 4; }
// for ($i = 0; $i < $max; $i++) {
//      $vert[$i] = ${$ra}[$offset++]; }
// if ($hasMaterial) $mat = ${$ra}[$offset++];
// if ($hasFaceUv) {
//     for ($i = 0; $i < $nUvLayers; $i++) {
//         $uvIndex = ${$ra}[$offset++];
//         $u = ${$uvLayers}[$uvIndex * 2 + 0];
//         $v = ${$uvLayers}[$uvIndex * 2 + 1];
//  }}
// if ($hasFaceVertexUv) {
//     for ($i = 0; $i < $nUvLayers; $i++) {
//         for ($j = 0; $j < $nVertices; $j++) {
//            $uvIndex = ${$ra}[$offset++];
//            $u = ${$uvLayers}[$uvIndex * 2 + 0];
//            $v = ${$uvLayers}[$uvIndex * 2 + 1];
//  }}}
// if ( $hasFaceNormal ) {
//   $normalIndex = ${$ra}[ $offset ++ ] * 3;
//   #normal = new THREE.Vector3();
//   $normal.x = ${$normals}[ $normalIndex ++ ];
//   $normal.y = ${$normals}[ $normalIndex ++ ];
//   $normal.z = ${$normals}[ $normalIndex ];
// }
//if ( $hasFaceVertexNormal ) {
//  for ( $i = 0; $i < $nVertices; $i++ ) {
//    my $normalIndex = ${$ra}[ $offset ++ ] * 3;
//    #normal = new THREE.Vector3();
//    $normal.x = ${$normals}[ $normalIndex ++ ];
//    $normal.y = ${$normals}[ $normalIndex ++ ];
//    $normal.z = ${$normals}[ $normalIndex ++ ];
// }}
// if ( $hasFaceColor ) {
//    my $colorIndex = ${$ra}[ $offset ++ ];
//    $color = ${$colors}[ $colorIndex ] );
// }
// if ( $hasFaceVertexColor ) {
//    for ( $i = 0; $i < $nVertices; $i++ ) {
//       my $colorIndex = ${$ra}[ $offset ++ ];
//       $color = ${$colors}[ colorIndex ] );
//       #face.vertexColors.push( color );
//  }}
// from cube.ac
// SURF 0X20
// mat 1
// refs 4
// 0 0 0
// 1 0 0
// 2 0 0
// 3 0 0
// SURF 0X20
// mat 1
// refs 4
// 4 0 0
// 7 0 0
// 6 0 0
// 5 0 0
// SURF 0X20
// mat 1
// refs 4
// 0 0 0
// 4 0 0
// 5 0 0
// 1 0 0

// stored in the SURF structure as
//  s->num_vertref = num; = count 4
//	s->vertref = (int *)malloc( num * sizeof(int)); = store of indexes
//  s->uvs = (ACUV *)malloc( num * sizeof(ACUV)); = store of uvs x,y
//  for (n = 0; n < num; n++) {
//      fscanf(f, "%d %f %f\n", &ind, &tx, &ty); line++;
//      s->vertref[n] = ind;
//		s->uvs[n].u = tx;
//		s->uvs[n].v = ty;
//  }
// three.js python puts 35,0,1,2,3,0,0,1,2,3, 35,4,7,6,5,0,4,5,6,7, 35,0,4

void ac_add_faces( ACObject *ob, FILE *fp, Face_Bits fb, int lev )
{
    static int vert_off = 0;
    static int done_out = 0;
    int s, num, n, bits;
    bits = (int)fb;
    if (ob->num_surf > 3) bits |= QuadBit;
    bits |= MatBit;
    for (s = 0; s < ob->num_surf; s++) {
		ACSurface *surf = &ob->surfaces[s];
        if (done_out) fprintf(fp,",");
        fprintf(fp,"%d",bits);
        num = surf->num_vertref;
        for(n = 0; n < num; n++) {
            fprintf(fp,",%d", (vert_off + surf->vertref[n]));
        }
        if (bits & MatBit) {
            fprintf(fp,",%d",surf->mat);
        }
        if (bits & VertNorm) {
            for(n = 0; n < num; n++) {
                fprintf(fp,",%d",surf->vertref[n]);
            }
        }

        done_out = 1;
    }

    vert_off += ob->num_vert;
    //norm_off += ob->num_norm;
    /* now do the 'kids', if any */
    for (n = 0; n < ob->num_kids; n++) {
        ac_add_faces(ob->kids[n], fp, fb, (lev + 1)); 
    }
}

void get_surface_count(ACObject *ob, int *pcnt)
{
    int n;
    *pcnt += ob->num_surf;
    for (n = 0; n < ob->num_kids; n++) {
        get_surface_count(ob->kids[n], pcnt); 
    }
}


Prototype void ac_export_threejs(ACObject *ob, FILE *fp)
{
#define fprt(a) fprintf(fp,a) /* just a little macro to save typing */
    static char _s_buf[1024];
    char *tb = _s_buf;
    vACP vacp;
    vACN vacn;
    //vACV vacv;
    vINT vaci;
    size_t max, ii, max2;
    //size_t max3;
    ACPoint acp;
    ACNormal acn;
    int nSurfs = 0;
    //ACVertex acv;
    //PBBOX pbb = &bbox;

    // accumulate the information
    get_surface_count( ob, &nSurfs);
    ac_dump_vector(ob, vacp, vacn, vaci);

    // NO, no good! set_bbox( vacp, pbb); // calculate vectors

    max  = vacp.size();
    max2 = vacn.size();
    //max3 = vaci.size();

    fprt( "{\n" );
    fprt( "\n" );
	fprt( "\t\"metadata\" :\n" );
	fprt( "\t{\n" );
	fprt( "\t\t\"formatVersion\" : 3.1,\n" );
	fprt( "\t\t\"generatedBy\"   : \"AC3DView Exporter\",\n" );
    sprintf(tb,"\t\t\"vertices\"      : %d,\n", (int)max );
    fprintf(fp,"%s",tb);
    sprintf(tb,"\t\t\"faces\"         : %d,\n", nSurfs   );
    fprintf(fp,"%s",tb);
    if (add_export_norms && max2)
        sprintf(tb,"\t\t\"normals\"       : %d,\n", (int)max2 );
    else
        sprintf(tb,"\t\t\"normals\"       : 0,\n", (int)max2 );
    fprintf(fp,"%s",tb);
    fprt( "\t\t\"colors\"        : 0,\n" );
	fprt( "\t\t\"uvs\"           : [],\n" );
	fprt( "\t\t\"materials\"     : 1,\n" );
	fprt( "\t\t\"morphTargets\"  : 0,\n" );
	fprt( "\t\t\"bones\"         : 0\n" );
	fprt( "\t},\n" );
    fprt( "\n" );
    sprintf(tb,"\t\"scale\" : %f,\n", ac_get_scale()); /* what should the 'scale' be? */
    fprintf(fp,"%s",tb);
    fprt( "\n" );
    fprt( "\t\"vertices\" : [" );

    // add vertices
    for (ii = 0; ii < max; ii++) {
        acp = vacp[ii];
        fprintf(fp,"%f,%f,%f", acp.x, acp.y, acp.z);
        if ((ii + 1) < max) fprt(",");
    }

    fprt( "],\n" );
    fprt( "\n" );
	fprt( "\t\"morphTargets\" : [],\n" );
    fprt( "\n" );
	fprt( "\t\"normals\" : [" );

    if (add_export_norms && max2) {
        // add normals
        for (ii = 0; ii < max2; ii++) {
            acn = vacn[ii];
            fprintf(fp,"%f,%f,%f", acn.x, acn.y, acn.z);
            if ((ii + 1) < max2) fprt(",");
        }
    }
    fprt( "],\n" );
    fprt( "\n" );
	fprt( "\t\"colors\" : [],\n" );
    fprt( "\n" );
	fprt( "\t\"uvs\" : [],\n" );
    fprt( "\n" );
	fprt( "\t\"faces\" : [" );

    Face_Bits fb = ((add_export_norms && max2) ? VertNorm : No_Bits);
    // add faces
    ac_add_faces( ob, fp, fb, 0 );

    fprt( "],\n");
    fprt( "\n" );
	fprt( "\t\"bones\" : [],\n" );
    fprt( "\n" );
	fprt( "\t\"skinIndices\" : [],\n" );
    fprt( "\n" );
	fprt( "\t\"skinWeights\" : [],\n" );
    fprt( "\n" );
    fprt( "\t\"animation\" : {}\n" );
    fprt( "\n" );
    fprt( "}\n" );
#undef fprt
}

static void ac_set_vertices(ACObject *ob, PBBOX pbb)
{
    int n;
    for (n = 0; n < ob->num_vert; n++) {
        ACVertex p = ob->vertices[n];
        if (pbb->num_verts == 0) {
            pbb->maxx = pbb->minx = p.x;
            pbb->maxy = pbb->miny = p.y;
            pbb->maxz = pbb->minz = p.z;
        } else {
            if (p.x > pbb->maxx) pbb->maxx = p.x;
            if (p.x < pbb->minx) pbb->minx = p.x;
            if (p.y > pbb->maxy) pbb->maxy = p.y;
            if (p.y < pbb->miny) pbb->miny = p.y;
            if (p.z > pbb->maxz) pbb->maxz = p.z;
            if (p.z < pbb->minz) pbb->minz = p.z;
        }
        pbb->num_verts++;
    }
}

static void ac_set_kids(ACObject *ob, PBBOX pbb)
{
    int n;
    ac_set_vertices(ob,pbb);
    for (n = 0; n < ob->num_kids; n++)
        ac_set_kids( ob->kids[n], pbb );

}

Prototype int ac_set_bounding_box(ACObject *ob, PBBOX pbb)
{
    int n;
    float maxx, maxy, maxz, r;
    pbb->num_verts = 0;
    ac_set_vertices(ob,pbb);
    for (n = 0; n < ob->num_kids; n++)
        ac_set_kids( ob->kids[n], pbb );
    if (pbb->num_verts) { /* set the CENTER */
        // glTranslatef(-(max.x + min.x)/2.0f,-(max.y + min.y)/2.0f,-(max.z + min.z)/2.0f);
        ACPoint p;
        maxx = pbb->maxx;
        maxy = pbb->maxy;
        maxz = pbb->maxz;
        p.x = ((pbb->minx + maxx) / 2.0f);
        p.y = ((pbb->miny + maxy) / 2.0f);
        p.z = ((pbb->minz + maxz) / 2.0f);
        pbb->center = p;
        r = sqrt((maxx - p.x)*(maxx - p.x) + 
                 (maxy - p.y)*(maxy - p.y) +
                 (maxz - p.z)*(maxz - p.z));
        pbb->radius = r;
        // float fDistance = r / 0.57735f; // where 0.57735f is tan(30 degrees)
        // double dNear = fDistance - r;
        // double dFar = fDistance + r;
        // glFrustum(-r, +r, +r, -r, dNear, dFar); // ignoring aspect ratio of the window for now!
        // All we need to do is to position the camera back along the z axis to where we want it -
        // glMatrixMode(GL_MODELVIEW);
        // glLoadIdentity();
        // gluLookAt(0.0f, 0.0f, fDistance, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        // and then move the model to align its centre with the origin -
        // glTranslatef(-p.x, -p.y, -p.z);
    }
    return pbb->num_verts;
}

/* eof - ac3d.cxx */
