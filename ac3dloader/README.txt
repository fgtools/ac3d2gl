AC3DVIEW version 1.14 - 20130504

See the root README file for further details.

This is some sample code to load and view an AC3D file.  AC3D is 3D modeling software and 
AC3D models (.ac files) contain the data for three dimensional objects.  The .ac file format
is a simple ASCII text format and is explained at the main website - www.ac3d.org. 
 
This code would be a good basis for any application that loads and displays AC3D files e.g.
games, simulation, etc...


SOME THINGS NOT HANDLED FROM THE .ac FILE

1) This loader does not use the rotation matrices from the AC3D
file (if you create a file using AC3D, it will not have any).

2) Polygons are not triangluated.  If there are polygons with > 4 vertices
and they are concave, you may not get what you see in AC3D.  (to fix this
just select the surface/s in AC3D and Surface->triangulate).

3) Lights from the .ac file are not drawn (there is a single light in the scene).

4) The object crease angle attribute is ignored.


COMPILING 

CMake - the current build system is CMake - http://www.cmake.org/ - and 
the root CMakeLists.txt should be used to build all in this package.

Change directory to the 'out of source' build folder - To build all with CMake, 
in root, do
    cd build
    cmake ..
    cmake --build . --config Release (or just make in unix)
    
It also contains some windows batch file, and a build-me.sh script.

The following tools are very DEPRECIATED!
WINDOWS - This release comes with Visual C++ workspace and project files.
LINUX AND OTHER PLATFORMS - a Makefile is provided which you will probably 
need to edit.

 
REQUIREMENTS

GLUT - simple interface/utility library for making windows with GL
FreeGLUT is now the standard on most platforms.
GL/GLU are also now usually standard on most platforms.
CMake will attempt to find the packages automatically, and abort if it 
fails.

USING THE CODE IN YOUR OWN APPLICATIONS

ac3d.c/ac3d.h, render.c and texture.c compile into a library ac2gl, and should 
be pretty easy to link with your program.

API

ACObject *ac_load_ac3d(char *fname) - loads a file
int ac_display_list_render_object(ACObject *ob) - returns a display list id 
(you must have inited GLUT and a context before calling this - otherwise
the id will be 0)
Once this is done, you can simply call glCallList(display_list_id) to render

NOTES

Note that the texture loader is for .rgb (SGI image format) - xv and Paintshop Pro 
save this.

Although the structures and rendering is similar to the internals of AC3D,
it is not the same.  Don't attempt to access the internals of AC3D with 
structures like this.  This code was written very quickly with a considerable
amount of it pasted from the original AC3D code. It may not check for errors
in the AC3D file as well as the actual AC3D loader.  AC3D files saved from
AC3D will be OK though.

The render is by no means optimal.  The renderer inside AC3D is considerably faster
but much less easy to read and understand.

You will need to edit the Makefile in order to compile it.  You need OpenGL
or Mesa and glut (for main.c). But as stated above this is depreciated. The 
current preferred build tool is cmake.

start with 'ac3dview <filename.ac>'
sample file included 'ac3dview test.ac'
(includes a texture ac3dlogo.rgb)

All the files here are free of copyright so you can use them as you please.  If you do 
use them then please give credit to AC3D. www.ac3d.org

1.11 added cdecl Prototype for WIN32 
1.12 added ac_object_free
1.13 reduced the default rotation speed; fixed fatal bug with argc when file name was passed to program on commandline
1.14 Add keyboard interface to control position, orientation and rotation of
         view, and added a WritePPM dump capability, and then a loadppm to display the 
         dump in a GLut conext - geoff

See the root README file for further details.

# eof
