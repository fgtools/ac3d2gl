/* file : ac3d.hxx */
#ifndef _AC3D_HXX_
#define _AC3D_HXX_


typedef struct ACPoint_t {
    float x, y, z;
} ACPoint, *PACPoint;


typedef struct ACNormal_t {
    float x, y, z;
} ACNormal, *PACNormal;


typedef struct ACVertex_t {
    float x, y, z;
    ACNormal normal;
} ACVertex, *PACVertex;

typedef struct ACUV_t {
    float u, v;
} ACUV, *PACUV;


typedef struct ACSurface_t {
    int *vertref;
    ACUV *uvs;
    int num_vertref;
    ACNormal normal;
    int flags;
    int mat;
    int mindx;
} ACSurface, *PACSurface;

typedef struct ACObject_t {
    int     order;  // just the order of creation - also unique id
    ACPoint loc;
    char    *name;
    char    *data;
    char    *url;
    ACVertex *vertices;
    int     num_vert;

    ACSurface *surfaces;
    int     num_surf;
    float   texture_repeat_x, texture_repeat_y;
    float   texture_offset_x, texture_offset_y;
    float   crease;   /* 2013/05/06 added */
    int     subdiv;   /* 2013/05/06 added */
    int     num_kids;
    struct ACObject_t **kids;
    float   matrix[9];
    int     type;
    int     texture;
} ACObject, *PACObject;

typedef struct ACCol_t {
    float r, g, b, a;
} ACCol, *PACCol;

typedef struct Material_t {
    ACCol rgb; /* diffuse **/
    ACCol ambient;
    ACCol specular;
    ACCol emissive;
    float shininess;
    float transparency;
    char *name;
} ACMaterial, *PACMaterial;

typedef struct ACImage_t {
    unsigned short width, height, depth;    
    void *data; 
    int index;
    char *name;
    int amask;
    char *origname; /** do not set - set automatically in texture_read function **/
} ACImage, *PACImage;

/* bounding x,y,z box */
typedef struct BBOX_t {
    int num_verts;
    float minx, maxx;
    float miny, maxy;
    float minz, maxz;
    ACPoint center; /* model center */
    float radius;
}BBOX, *PBBOX;

#define OBJECT_WORLD 999
#define OBJECT_NORMAL 0
#define OBJECT_GROUP 1
#define OBJECT_LIGHT 2

#define SURFACE_SHADED (1<<4)
#define SURFACE_TWOSIDED (1<<5)

#define SURFACE_TYPE_POLYGON (0)
#define SURFACE_TYPE_CLOSEDLINE (1)
#define SURFACE_TYPE_LINE (2)

#ifdef WIN32
#ifdef AC2GL_STATIC
#define Prototype
#else
#define Prototype  __declspec( dllexport )
#endif
#else
#define Prototype
#endif

#define Private static
#define Boolean int

#ifndef TRUE
#define FALSE (0)
#define TRUE (!FALSE)
#endif

#define STRING(s)  (char *)(strcpy((char *)myalloc(strlen(s)+1), s))
#define streq(a,b)  (!strcmp(a,b))

/* MACRO for memory allocation, re-allocation and feeing */
#define myalloc malloc
#define myfree free
#define myrealloc realloc

#define MX_LINE_BUFF    255
#define MX_MATERIALS    255

Prototype ACObject *ac_load_ac3d(char *filename);
Prototype ACMaterial *ac_palette_get_material(int index);
Prototype ACImage *ac_get_texture(int ind);
Prototype int ac_load_texture(char *name);
Prototype int ac_load_rgb_image(char *fileName);
Prototype void ac_prepare_render();
Prototype int ac_display_list_render_object(ACObject *ob);
Prototype void ac_object_free(ACObject *ob);
Prototype void ac_dump_simple(ACObject *ob, FILE *fp); /* 2013/05/06 - exposed, and added output file */

/* 2013/05/06 - additional functions */
Prototype void ac_set_file_name( char *name );
Prototype void ac_add_texture_paths( char *path );
Prototype char *ac_get_texture_paths(void);
Prototype int ac_set_verbosity( int v );
Prototype int ac_get_verbosity( void );
Prototype int ac_verb1( void );
Prototype int ac_verb2( void );
Prototype int ac_verb5( void );
Prototype int ac_verb9( void );
Prototype void ac_export_threejs(ACObject *ob, FILE *fp); 
/* 2013/05/12 - got a core dump when loading textures, so added -n to inhibit loads */
Prototype int ac_load_textures();   /*  { return do_texture_load; } */
Prototype void ac_set_load_textures(int v); /* { do_texture_load = v; } */
Prototype void ac_set_add_normals(int v); /* { add_export_norms = v; } */
Prototype float ac_get_scale(); /* { return def_scale; } */
Prototype void ac_set_scale(float f); /* { def_scale = f; } */
Prototype void ac_free_objects();   /* free ALL memory */
Prototype int ac_palette_get_mat_size();
Prototype const char *ac_objecttype_to_string(int t);
Prototype int ac_set_bounding_box(ACObject *ob, PBBOX pbb);

#endif /* #ifndef _AC3D_HXX_ */
/* eof - ac3d.hxx */
