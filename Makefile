
LIBS    = -L/usr/X11R6/lib -L/usr/freeware/lib32 -lglut -lGLU -lGL -lX11 -lXmu -lm
CXX      = g++ -g

all: ac_to_gl ac3dmain

ac_to_gl: ac_to_gl.cxx
	${CXX} -o ac_to_gl ac_to_gl.cxx

ac3dmain.o: ac3dmain.cxx
	${CXX} -c ac3dmain.cxx

ac3dmain: ac3dmain.o ac3d.cxx 
	${CXX} ac3dmain.o ac3d.cxx -o $@ ${LIBS}

ac3d.cxx: ac3d.ac
	./ac_to_gl < ac3d.ac > ac3d.cxx

clean:
	rm -f ac3d.cxx ac3dmain.o ac3dmain ac_to_gl
