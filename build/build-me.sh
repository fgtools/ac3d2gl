#!/bin/sh
#< build-me.sh for ac3d2gl project
BN=`basename $0`
echo "$BN: Building the ac3d2gl project..."
echo "$BN: If the build fails, and good option to add is -DCMAKE_VERBOSE_MAKEFILE=TRUE"
echo "$BN: Then all compile and link flags can be seen and checked..."
echo "$BN: And to add debug symbols for gdb use -DCMAKE_BUILD_TYPE=Debug and -g is added."

cmake .. $@
if [ ! "$?" = "0" ]; then
    echo "ERROR: CMake config or generation FAILED!"
    exit 1
fi

cmake --build . --config Release
if [ ! "$?" = "0" ]; then
    echo "ERROR: Cmake build Release FAILED!"
    exit 1
fi

echo "Appears a successful build..."


