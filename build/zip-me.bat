@setlocal
@REM 20130612 - final?
@set TMPVER=06
@set TMPFIL=ac3dloader
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=ac3d2gl.sdf
@if EXIST %TMPFIL% goto NOWAY
@set TMPFIL=ac3d2gl.sln
@if EXIST %TMPFIL% goto NOWAY
@set TMPFIL=ac3dmain.dir
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=ac_to_gl.dir
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=Win32
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=harness.dir
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=ufomain.dir
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=ipch
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=Debug
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=ac3dloader
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=CMakeFiles
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=Release
@if EXIST %TMPFIL%\nul goto NOWAY
@set TMPFIL=CMakeCache.txt
@if EXIST %TMPFIL%\nul goto NOWAY
@if NOT EXIST ..\..\ac3d2gl\build\zip-me.bat goto BADDIR
@cd ..
@set TMPFIL=*.bak
@if EXIST %TMPFIL% goto GOTBAK
@set TMPFIL=CMakeModules\*.bak
@if EXIST %TMPFIL% goto GOTBAK
@set TMPFIL=ac3dloader\*.bak
@if EXIST %TMPFIL% goto GOTBAK

@cd ..
@set TMPZIP=zips
@if NOT EXIST %TMPZIP%\nul goto NOZD
@set TMPZIP=zips\ac3d2gl-%TMPVER%.zip
@set TMPOPT=-a
@if NOT EXIST %TMPZIP% goto DNOPT
@call dirmin %TMPZIP%
@echo WARNING: This is an UPDATE ONLY
@echo Are you SURE you want to continue?
@pause
@set TMPOPT=-u
:DNOPT

@call ZIP8 %TMPOPT% -P -r -o %TMPZIP% ac3d2gl\*.*
@if NOT EXIST %TMPZIP% goto NOZIP

@goto END

:NOZIP
@echo.
@echo ERROR: No zip %TMPZIP% created!
@echo Maybe zip8 tool does NOT exist, or ?????
@echo.
@goto END

:NOWAY
@echo.
@echo ERROR: Found %TMPFIL%!
@echo Must do a FULL cmake clean BEFORE zipping sources...
@echo.
@goto END

:GOTBAK
@echo.
@echo ERROR: Found %TMPFIL% file!
@echo Must do a FULL clean BEFORE zipping sources...
@echo.
@goto END


:BADDIR
@echo.
@echo ERROR: Can NOT file self by ..\..\ac3d2gl\build\zip-me.bat!
@echo Not in correct directory structure...
@echo.
@goto END

:NOZD
@echo.
@echo ERROR: Can NOT find zip dir %TMPZIP% in %CD%!
@echo Not in correct directory structure...
@echo.
@goto END

:END
