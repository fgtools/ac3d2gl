@setlocal
@set TMPBGN=%TIME%
@set TMPLOG=bldlog-1.txt

@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=C:\FG\18\3rdParty
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@echo Begin %TMPBGN% > %TMPLOG%
@echo All output to %TMPLOG%...

cmake .. %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Debug  >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

cmake --build . --config Release  >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@type %TMPLOG%
@fa4 "***" %TMPLOG%
@echo Appears a successful build... see %TMPLOG%
@call elapsed %TMPBGN%

@echo Continue with INSTALL?
@pause

cmake --build . --config Debug  --target INSTALL

cmake --build . --config Release  --target INSTALL

@call elapsed %TMPBGN%
@echo All done... see %TMPLOG%

@goto END

:ERR1
@echo ERROR: Cmake config or geneation FAILED!
@goto ISERR

:ERR2
@echo ERROR: Cmake build Debug FAILED!
@goto ISERR

:ERR1
@echo ERROR: Cmake build Release FAILED!
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof

