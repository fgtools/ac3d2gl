@setlocal
@set TMPEXE=Release\showaero.exe
@if NOT EXIST %TMPEXE% goto ERR1
@set TMPDLL=glut32.dll
@set TMP3RD=C:\FG\18\3rdParty\bin
@if NOT EXIST %TMP3RD%\%TMPDLL% goto ERR2

cd ..
@if NOT EXIST aerostar\nul goto ERR3
cd aerostar
@set TMPEXE=..\build\Release\showaero.exe
@if NOT EXIST %TMPEXE% goto ERR1
@echo RUnning in %CD%

@set PATH=%PATH%;%TMP3RD%
%TMPEXE%

@goto END

:ERR1
@echo Can NOT locate %TMPEXE%! Check name, location and FIX ME!
@goto END

:ERR2
@echo Can NOT locate %TMP3RD%\%TMPDLL%! Check name, location and FIX ME!
@goto END

:ERR3
@echo Directory structure not as expected. Can not locate aerostar folder!
@goto END


:END

