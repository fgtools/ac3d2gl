@setlocal
@set TMPEXE=Release\ac3dmain.exe
@if NOT EXIST %TMPEXE% goto ERR1
@set TMPDLL=glut32.dll
@set TMP3RD=C:\FG\18\3rdParty\bin
@if NOT EXIST %TMP3RD%\%TMPDLL% goto ERR2

@set PATH=%PATH%;%TMP3RD%
%TMPEXE%

@goto END

:ERR1
@echo Can NOT locate %TMPEXE%! Check name, location and FIX ME!
@goto END

:ERR2
@echo Can NOT locate %TMP3RD%\%TMPDLL%! Check name, location and FIX ME!
@goto END



:END
