#!/bin/sh
#< zip-me.sh - 20130505
BN=`basename $0`

TMPVER="06" # 20130512
#TMPVER="02"

wait_for_input()
{
    if [ "$#" -gt "0" ] ; then
        echo "$1"
    fi
    echo -n "$BN: Enter y to continue : "
    read char
    if [ "$char" = "y" -o "$char" = "Y" ]
    then
        echo "$BN: Got $char ... continuing ..."
    else
        if [ "$char" = "" ] ; then
            echo "$BN: Aborting ... no input!"
        else
            echo "$BN: Aborting ... got $char!"
        fi
        exit 1
    fi
}

ask()
{
    wait_for_input "$BN: *** CONTINUE ***"
}

TMPFIL="../../ac3d2gl/build/zip-me.sh"
if [ ! -f "$TMPFIL" ]; then
    echo "$BN: Can NOT find self with [$TMPFIL]"
    echo "$BN: Directory structure NOT as expected!"
    exit 1
fi

# names of hopefully most of the build products
TMPFILS="ac3dmain ac_to_gl bldlog-1.txt harness config.h Makefile ufomain CMakeCache.txt cmake_install.cmake"
TMPFILS="$TMPFILS ac3dmaind ac_to_gld harnessd ufomaind"

for arg in $TMPFILS; do
    if [ -f "$arg" ]; then
        echo "$BN: ERROR: Found file [$arg]!"
        echo "$BN: Do FULL cmake clean before zipping source"
        exit 1
    fi
done

# names of the build directories created
TMPDIRS="ac3dloader CMakeFiles"
for arg in $TMPDIRS; do
    if [ -d "$arg" ]; then
        echo "$BN: ERROR: Found directory [$arg]!"
        echo "$BN: Do FULL cmake clean before zipping source"
        exit 1
    fi
done

cd ..

FND=`find . -name "*~" -type f`
if [ -z "$FND" ]; then
    echo "$BN: Appears no backup files found..."
else
    echo "$BN: FOUND backup files!"
    echo "$FND"
    echo "$BN: Do a full CLEAN before zipping.."
    exit 1
fi

FND=`find . -name "*.bak" -type f`
if [ -z "$FND" ]; then
    echo "$BN: Appears no bak files found..."
else
    echo "$BN: FOUND .bak files!"
    echo "$FND"
    echo "$BN: Do a full CLEAN before zipping.."
    exit 1
fi

cd ..
TMPZIP="zips"
if [ ! -d "$TMPZIP" ]; then
    echo "$BN: Can NOT locate $TMPZIP directory in $(pwd)!"
    exit 1
fi

TMPZIP="zips/ac3d2gl-$TMPVER.zip"
TMPOPT=""

if [ -f "$TMPZIP" ]; then
    ls -l $TMPZIP
    echo "$BN: Zip file already exists!"
    echo "$BN: Are you sure you want to update it?"
    ask
    TMPOPT="-u"
fi
 
echo "$BN: Doing 'zip $TMPOPT -r $TMPZIP ac3d2gl/*'"
zip $TMPOPT -r $TMPZIP ac3d2gl/*
 
if [ ! -f "$TMPZIP" ]; then
    echo "$BN: Appears zip tool FAILED! No $TMPZIP file"
    exit 1
fi

ls -l $TMPZIP
echo "$BN: Done zip..."

# eof

