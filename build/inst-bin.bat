@setlocal

@set TMPSRC=ac3dloader\Release
@set TMPDST=C:\MDOS

@set TMPFIL=ac3dview.exe
@call :DO_ONE

@set TMPFIL=loadppm.exe
@call :DO_ONE

@set TMPSRC=Release
@set TMPFIL=ac_to_gl.exe
@call :DO_ONE

@set TMPFIL=ac_to_3j.exe
@call :DO_ONE

@goto END


:DO_ONE
@if NOT EXIST %TMPSRC%\%TMPFIL% goto MISSING
@if NOT EXIST %TMPDST%\nul goto NODST
@if EXIST %TMPDST%\%TMPFIL% (
@echo NOTE: Will COPY 
@call dirmin %TMPSRC%\%TMPFIL%
@echo and OVERWRITE
@call dirmin %TMPDST%\%TMPFIL%
@fc4 -v0 -q -b %TMPSRC%\%TMPFIL% %TMPDST%\%TMPFIL% >nul
@if ERRORLEVEL 1 (
@echo Files are different...
) else (
@echo Files are exactly the SAME - No copy done...
@goto :EOF
)
@ask Continue with COPY?
@if ERRORLEVEL 1 goto DOCOPY
@goto :EOF
)

:DOCOPY

copy %TMPSRC%\%TMPFIL% %TMPDST%\%TMPFIL%

@goto :EOF


:NODST
@echo ERROR: Can find DESTINATION %TMPDST%
@goto END



:MISSING
@echo ERROR: Can NOT locate %TMPSRC%\%TMPFIL%!
@goto END


:END
