#!/bin/sh
#< inst-bin.sh - 20130512
BN=`basename $0`

wait_for_input()
{
    if [ "$#" -gt "0" ] ; then
        echo "$1"
    fi
    echo -n "$BN: Enter y to continue : "
    read char
    if [ "$char" = "y" -o "$char" = "Y" ]
    then
        echo "$BN: Got $char ... continuing ..."
    else
        if [ "$char" = "" ] ; then
            echo "$BN: Aborting ... no input!"
        else
            echo "$BN: Aborting ... got $char!"
        fi
        exit 1
    fi
}

ask()
{
    wait_for_input "$BN: *** CONTINUE? ***"
}

DO_ONE()
{
    if [ ! -f "$TMPSRC/$TMPFIL" ]; then
        echo "ERROR: Can NOT locate $TMPSRC/$TMPFIL!"
        exit 1
    fi

    if [ ! -d "$TMPDST" ]; then
        echo "ERROR: Can find DESTINATION $TMPDST!"
        exit 1
    fi

    if [ -f "$TMPDST/$TMPFIL" ]; then
        echo "Copy"
        dirmin $TMPSRC/$TMPFIL
        echo "overwriting"
        dirmin $TMPDST/$TMPFIL
        ask
    fi
    echo "Doing: 'cp -v -a $TMPSRC/$TMPFIL $TMPDST/$TMPFIL'"
    cp -v -a $TMPSRC/$TMPFIL $TMPDST/$TMPFIL
}

TMPSRC="ac3dloader"
TMPDST="$HOME/bin"

TMPFIL="ac3dview"
DO_ONE

TMPFIL="loadppm"
DO_ONE

TMPSRC="."
TMPFIL="ac_to_gl"
DO_ONE

# eof
