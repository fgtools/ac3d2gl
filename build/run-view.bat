@setlocal
@set TMPEXE=ac3dloader\Release\ac3dview.exe
@if NOT EXIST %TMPEXE% goto ERR1
@set TMPDLL=glut32.dll
@set TMP3RD=C:\FG\18\3rdParty\bin
@if NOT EXIST %TMP3RD%\%TMPDLL% goto ERR2

@set TMPAC=
@if "%~1x" == "x" goto GOTCMD
@set TMPAC=%1
:GOTCMD

@cd ..
@set TMPEXE=build\%TMPEXE%
@if NOT EXIST %TMPEXE% goto ERR1

@set PATH=%PATH%;%TMP3RD%
@REM %TMPEXE% folly.ac
@REM %TMPEXE% ufo.ac
%TMPEXE% %TMPAC%

@goto END

:ERR1
@echo Can NOT locate %TMPEXE%! Check name, location and FIX ME!
@goto END

:ERR2
@echo Can NOT locate %TMP3RD%\%TMPDLL%! Check name, location and FIX ME!
@goto END



:END
