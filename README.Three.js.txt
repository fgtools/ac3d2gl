File: README.Three.js.txt - 20130525

AIM: To convert an AC3D model file .ac to Three.js JSON .js file.

Taking the fgdata/AI/.../f16.ac - call it f16AI.ac

Blender Test : in C:\FG\18\blendac3d
Start blender, select [+] New scene
1: Import f16AI.ac
2: Export f16AI.dae - exit
Start blender, select [+] New scene
3: Import f16AI.dae
4: Export f16AI.js

Materials: How are MATERIAL entries treated?

    Example: f16AE.ac -> f16AE.dae -> f16AC.js
    AC3D:
MATERIAL "ac3dmat1.008" rgb 1 1 1  amb 1 1 1  emis 0 0 0  spec 0.5 0.5 0.5  shi 10  trans 0
    DAE:
    <effect id="ac3dmat1_008-effect">
      <profile_COMMON>
        <technique sid="common">
          <phong>
            <emission>
              <color sid="emission">0 0 0 1</color>
            </emission>
            <ambient>
              <color sid="ambient">1 1 1 1</color>
            </ambient>
            <diffuse>
              <color sid="diffuse">0.8 0.8 0.8 1</color>
            </diffuse>
            <specular>
              <color sid="specular">0.05 0.05 0.05 1</color>
            </specular>
            <shininess>
              <float sid="shininess">50</float>
            </shininess>
            <index_of_refraction>
              <float sid="index_of_refraction">1</float>
            </index_of_refraction>
          </phong>
        </technique>
      </profile_COMMON>
    </effect>
    3J - REPEATED MULTIPLE TIMES - all with the SAME NUMBER
    So HOW TO CONVERT
MATERIAL "ac3dmat1.008" rgb 1 1 1  amb 1 1 1  emis 0 0 0  spec 0.5 0.5 0.5  shi 10  trans 0
    TO: ?????????????????
	{
		"DbgColor" : 15597568,
		"DbgIndex" : 1,
		"DbgName" : "ac3dmat1_008",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorDiffuse" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorSpecular" : [0.02499999850988388, 0.02499999850988388, 0.02499999850988388],
		"depthTest" : true,
		"depthWrite" : true,
		"shading" : "Lambert",
		"specularCoef" : 50,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	},

Some wierd 'conversion' going on here????????????

Another example - only change spec and shi
AC3D:
MATERIAL "ac3dmat1.012" rgb 1 1 1  amb 1 1 1  emis 0 0 0  spec 0.735 0.815 0.835  shi 60  trans 0
DAE:
              <color sid="emission">0 0 0 1</color>
              <color sid="ambient">1 1 1 1</color>
              <color sid="diffuse">0.8 0.8 0.8 1</color>
              <color sid="specular">0.441 0.489 0.501 1</color>
              <float sid="shininess">50</float>
              <float sid="index_of_refraction">1</float>
3JS:
	"materials" : [	{
		"DbgColor" : 15658734,
		"DbgIndex" : 0,
		"DbgName" : "ac3dmat1_012",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorDiffuse" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorSpecular" : [0.22050000727176666, 0.24450001120567322, 0.25050002336502075],
		"depthTest" : true,
		"depthWrite" : true,
		"shading" : "Lambert",
		"specularCoef" : 50,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	},

Reading the Three.js python script - export_threejs.py - see
C:\FG\18\threejs\utils\exporters\blender\2.66\scripts\addons\io_mesh_threejs\export_threejs.py
            material = materials[m.name]
            material['colorDiffuse'] = [m.diffuse_intensity * m.diffuse_color[0],
                                        m.diffuse_intensity * m.diffuse_color[1],
                                        m.diffuse_intensity * m.diffuse_color[2]]
            material['colorSpecular'] = [m.specular_intensity * m.specular_color[0],
                                         m.specular_intensity * m.specular_color[1],
                                         m.specular_intensity * m.specular_color[2]]
            material['colorAmbient'] = [m.ambient * material['colorDiffuse'][0],
                                        m.ambient * material['colorDiffuse'][1],
                                        m.ambient * material['colorDiffuse'][2]]
            material['transparency'] = m.alpha
            
Maybe looking at the import_threejs.py
        name = m.get("DbgName", "material_%d" % i)
        colorAmbient = m.get("colorAmbient", None)
        colorDiffuse = m.get("colorDiffuse", None)
        colorSpecular = m.get("colorSpecular", None)
        alpha = m.get("transparency", 1.0)
        specular_hardness = m.get("specularCoef", 0)
        if colorDiffuse:
            setColor(material.diffuse_color, colorDiffuse)
            material.diffuse_intensity = 1.0
        if colorSpecular:
            setColor(material.specular_color, colorSpecular)
            material.specular_intensity = 1.0
        if alpha < 1.0:
            material.alpha = alpha
            material.use_transparency = True
        if specular_hardness:
            material.specular_hardness = specular_hardness

More examples: (form same fileS)
f16AI.ac
     17 <MATERIAL "ac3dmat13.003" rgb 0.325 0.251 0.075  amb 0.217 0.217 0.217  emis 0 0 0  spec 1 1 1  shi 75  trans 0.827>
f16AI.js
  1,280 <               "DbgName" : "ac3dmat1_003",>
  1,280 <               "blending" : "NormalBlending",>
  1,280 <               "colorAmbient" : [0.0, 0.0, 0.0],>
  1,280 <               "colorDiffuse" : [0.0, 0.0, 0.0],>
  1,280 <               "colorSpecular" : [0.5, 0.5, 0.5],>
  1,280 <               "depthTest" : true,>
  1,314 <               "DbgName" : "ac3dmat13_003",>
  1,314 <               "blending" : "NormalBlending",>
  1,314 <               "colorAmbient" : [0.20799999547004688, 0.16064000369071962, 0.047999999642372115],>
  1,314 <               "colorDiffuse" : [0.20799999547004688, 0.16064000369071962, 0.047999999642372115],>
  1,314 <               "colorSpecular" : [0.375, 0.375, 0.375],>
  1,314 <               "depthTest" : true,>
  1,348 <               "DbgName" : "ac3dmat13_003",>
  1,348 <               "blending" : "NormalBlending",>
  1,348 <               "colorAmbient" : [0.20799999547004688, 0.16064000369071962, 0.047999999642372115],>
  1,348 <               "colorDiffuse" : [0.20799999547004688, 0.16064000369071962, 0.047999999642372115],>
  1,348 <               "colorSpecular" : [0.375, 0.375, 0.375],>
  1,348 <               "depthTest" : true,>
f16AI.ac
MATERIAL "ac3dmat1.008" rgb 1 1 1  amb 1 1 1  emis 0 0 0  spec 0.5 0.5 0.5  shi 10  trans 0
MATERIAL "ac3dmat14" rgb 0.8 0.8 0.8  amb 0.8 0.8 0.8  emis 0 0 0  spec 0.5 0.5 0.5  shi 10 trans 0
MATERIAL "ac3dmat12" rgb 0.266667 0.266667 0.266667  amb 0.266667 0.266667 0.266667  emis 0 0 0  spec 0.5 0.5 0.5  shi 10  trans 0    

It all seem too bizzre ??????????

# eof

