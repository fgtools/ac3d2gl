
#ifndef _MSC_VER
#include "config.h"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef WIN32
#include <windows.h>
#else
#if defined(HAVE_CLOCK_GETTIME) || defined(HAVE_GETTIMEOFDAY)
#include <sys/time.h>
#endif
#endif
#include <math.h>
#include <time.h>
#include <GL/glut.h>

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

extern void load_sgi_texture ( char *fname ) ;
extern void draw_ac3d_model  ( void (*func)(char *) ) ;

static float p = 0.0f ;
static float x_trans = 0.0;
static float y_trans = 0.0;
static float z_trans = -10.0;  // was -3.0
static int frames = 0;
#define MX_FRAMES 10
static int frames_per_sec[MX_FRAMES] = {0};
static int next_frame = 0;
// static time_t last_sec = 0;
static double last_secs = 0.0;
static double angle_rate = 25.0; // change at rate of ?? degrees per second
static double begin_secs, prev_secs;
static float x_rot = 15.0;
static float y_rot = 10.0;
static float z_rot =  5.0;
static int elap_secs = 0;

///////////////////////////////////////////////////////////////////////////////
#define USE_PERF_COUNTER
///////////////////////////////////////////////////////////////////////////////
#if (defined(WIN32) && defined(USE_PERF_COUNTER))
// QueryPerformanceFrequency( &frequency ) ;
// QueryPerformanceCounter(&timer->start) ;
double get_seconds()
{
    static double dfreq;
    static bool done_freq = false;
    static bool got_perf_cnt = false;
    if (!done_freq) {
        LARGE_INTEGER frequency;
        if (QueryPerformanceFrequency( &frequency )) {
            got_perf_cnt = true;
            dfreq = (double)frequency.QuadPart;
        }
        done_freq = true;
    }
    double d;
    if (got_perf_cnt) {
        LARGE_INTEGER counter;
        QueryPerformanceCounter (&counter);
        d = (double)counter.QuadPart / dfreq;
    }  else {
        DWORD dwd = GetTickCount(); // milliseconds that have elapsed since the system was started
        d = (double)dwd / 1000.0;
    }
    return d;
}

#else // !WIN32
double get_seconds()
{
    double t1;
#if defined(HAVE_CLOCK_GETTIME)
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	//printf("The time is %s" "and %ld nanoseconds.\n",
	//    ctime(&ts.tv_sec), ts.tv_nsec);
    t1 = (double)(ts.tv_sec+((double)ts.tv_usec/1000000.0));
#elif defined(HAVE_GETTIMEOFDAY)
	struct timeval tv;
	gettimeofday(&tv, 0);
	//printf("The time is %s" "and %ld microseconds.\n",
	//    ctime(&tv.tv_sec), tv.tv_usec);
    t1 = (double)(tv.tv_sec+((double)tv.tv_usec/1000000.0));
#endif
    return t1;
}
#endif // WIN32 y/n
///////////////////////////////////////////////////////////////////////////////


static void displayfn (void)
{
  double curr = get_seconds();
  double elap = curr - prev_secs;

  /* Clear the screen */
  glClearColor ( 0.0f, 0.4f, 0.0f, 1.0f ) ;
  glClear      ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) ;
  glMatrixMode ( GL_PROJECTION ) ;
  glLoadIdentity () ;
  gluPerspective ( 60.0f, 1, 1, 1000 ) ;
  glMatrixMode ( GL_MODELVIEW ) ;
  glLoadIdentity () ;
  glEnable  ( GL_LIGHT0 ) ;
  glEnable ( GL_DEPTH_TEST ) ;


  // glTranslate( x, y, z ) - Specify the x, y, and z coordinates of a translation vector. 
  // If the matrix mode is either GL_MODELVIEW or GL_PROJECTION, all objects drawn after a call to glTranslate are translated.
  glTranslatef ( x_trans, y_trans, z_trans ) ;

  // elap is seconds since last, angle_rate is desired degrees per second
  // p += 4.0f ;
  p += (float) ( elap * angle_rate );
  if (p >= 360.0) p -= 360.0f;
  glRotatef( p, x_rot, y_rot , z_rot );

  draw_ac3d_model ( load_sgi_texture ) ;

  glutSwapBuffers   () ;
#if (!defined(FREEGLUT) && !defined(USE_IDLE_FUNC))
  /* this does not appear to work with freeglut */
  glutPostRedisplay () ;
#endif

  frames++;
  if ((curr - last_secs) >= 1.0) {
      frames_per_sec[next_frame] = frames;
      frames = 0;
      last_secs = curr;
      next_frame++;
      if (next_frame >= MX_FRAMES)
          next_frame = 0;
      elap_secs++;
  }
  prev_secs = curr;
}

static void key_help()
{
    int i, tframes, max;
    if (elap_secs > MX_FRAMES)
        max = MX_FRAMES;
    else if (elap_secs)
        max = elap_secs;
    else
        max = 1;
    tframes = 0;
    for (i = 0; i < max; i++) {
        tframes += frames_per_sec[i];
    }
    tframes /= max;

    printf("Keyboard Help - to size and position the model.\n");
    printf(" ESC or q = Quit\n");
    printf(" s/S      = Slow/Speed up rotation speed. angle_rate=%f\n", angle_rate);
    printf(" x/X      = Left/Right - x translation factor (%f)\n", x_trans);
    printf(" y/Y      = Up/Down    - y translation factor (%f)\n", y_trans);
    printf(" z/Z      = Near/Far   - z translation factor (%f)\n", z_trans);
    printf(" ?/h      = This help, frame rate and current angle...\n");
    if (tframes)
        printf(" fps=%d, rotation angle=%f, secs %d\n", tframes, p, elap_secs);
}

static void keyfn ( unsigned char c, int i1, int i2 )
{
    if ((c == 'q')||(c == 0x1b))
        exit ( 0 ) ;

    if ((c == '?')||(c == 'h')) {
        key_help();
    } else if (c == 's') {
        angle_rate -= 1.0;
        printf("angle_rate = %f\n", angle_rate);
    } else if (c == 'S') {
        angle_rate += 1.0;
        printf("angle_rate = %f\n", angle_rate);
    } else if (c == 'x') {
        x_trans -= 0.5f;
        printf("x_trans = %f\n", x_trans);
    } else if (c == 'X') {
        x_trans += 0.5f;
        printf("x_trans = %f\n", x_trans);
    } else if (c == 'y') {
        y_trans -= 0.5f;
        printf("y_trans = %f\n", y_trans);
    } else if (c == 'Y') {
        y_trans += 0.5f;
        printf("y_trans = %f\n", y_trans);
    } else if (c == 'z') {
        z_trans -= 0.5f;
        printf("z_trans = %f\n", z_trans);
    } else if (c == 'Z') {
        z_trans += 0.5;
        printf("z_trans = %f\n", z_trans);
    }
}

int main ( int argc, char **argv )
{
  key_help();
  begin_secs = get_seconds();
  prev_secs  = begin_secs;
  glutInitWindowPosition(   0,   0 ) ;
  glutInitWindowSize    ( 640, 480 ) ;
  glutInit              ( &argc, argv ) ;
  glutInitDisplayMode   ( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH ) ;
  glutCreateWindow      ( "ac_to_gl Test Harness"  ) ;
  glutDisplayFunc       ( displayfn ) ;
  glutKeyboardFunc      ( keyfn ) ;
#if (defined(FREEGLUT) || defined(USE_IDLE_FUNC))
  /* seem freeglut changed the API!?!? */
  glutIdleFunc          ( displayfn ) ;
#endif
  glutMainLoop () ;
  return 0 ;
}


/* eof - harness.cxx */
