@setlocal
@set TMPIN=aerostar700.ac
@set TMPOUT=aerostar700.cxx

@set TMPEXE=..\build\Release\ac_to_gl.exe


@echo Running in %CD%

@if NOT EXIST %TMPIN% goto ERR1
@if EXIST %TMPOUT% goto ERR2
@if NOT EXIST %TMPEXE% goto ERR3

%TMPEXE% <%TMPIN% >%TMPOUT%

@goto END

:ERR1
@echo ERROR: Can NOT locate %TMPIN%
@goto END

:ERR2
@echo ERROR: %TMPOUT% ALREADY EXISTS!
@echo Delete or rename it first
@goto END

:ERR3
@echo ERROR: Can NOT locate %TMPEXE%
@goto END

:END

