/* this file is free Software under the GNU General Public License (GPL) */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#ifdef WIN32
#include <windows.h>
#include <sys/timeb.h>
#else
#include <unistd.h>
#include <sys/time.h>
#endif
#include <math.h>
#include <GL/glut.h>

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

#define RADIANS_TO_DEGREES  (180.0 / 3.1412) 

#include "Array.h"

extern void draw_ac3d_model(void (*func)(char *));

class Vec3f
{ 
public:
   float x;
   float y;
   float z;   
   Vec3f() { x = 0; y = 0; z = 0; }
   Vec3f(float xIn, float yIn, float zIn)
       { x = xIn; y = yIn; z = zIn; } 
};

template <class X> class interpolator_class  {
protected:
   Array<float> keys;
   Array<X> values;
   
public:
   void append(float key, X keyValue) {
      keys.append(key);
      values.append(keyValue);
   }
   void check_begin_and_end() {
      // check if there are values
      if (values.size() == 0) {
         keys.append(0);
         static X zero;
         values.append(zero);
      }
      // check if first value has key == 0
      if (keys[0] != 0.0) {
         if (keys[0] < 0) {
            fprintf(stderr, "Interpolator has first time key < 0, exiting\n");
            exit(1);
         } else {
            // insert first key value
            values.resize(values.size()+1);
            int i;
            for (i = keys.size()-1; i > 0; i--)
               keys[i] = keys[i-1];
            for (i = values.size()-1; i > 0; i--)
               values[i] = values[i-1];
            keys[0] = 0;
         }
      }      
      if (keys[keys.size()-1] != 1) {
         if (keys[0] > 1) {
            fprintf(stderr, "Interpolator has last time key > 1, exiting\n");
            exit(1);
         } else {
            // append last key value
            keys.append(1);
            values.append(values[values.size()-1]);
         }
      }      
         
   }
   ~interpolator_class() {
      keys.resize(0);
      values.resize(0);
      }
};

#define delta(var, pos) \
  ((values[pos+1].var-values[pos].var)/(keys[pos+1]-keys[pos]) * \
   (fraction-keys[pos]))

class position_interpolator_class : public interpolator_class<Vec3f>
{
public:
   Vec3f interpolate(float fraction) {
      Vec3f ret = values[0];
      for (int position=0; position<keys.size(); position++) {
         if (fraction>keys[position]) {
            ret.x = values[position].x + delta(x, position);
            ret.y = values[position].y + delta(y, position);
            ret.z = values[position].z + delta(z, position);
            }
         }
      return ret;
   }
};

class Rotation
{ 
public:
   float x;
   float y;
   float z;   
   float s;
   Rotation() { x = 0; y = 0; z = 0; s = 0; }
   Rotation(float xIn, float yIn, float zIn, float sIn)
       { x = xIn; y = yIn; z = zIn; s = sIn; } 
};

class orientation_interpolator_class : public interpolator_class<Rotation>
{
public:
   Rotation interpolate(float fraction) {
      Rotation ret=values[0];
      for (int position=0; position < keys.size(); position++) {
         if (fraction>keys[position]) {
            ret.x = values[position].x + delta(x, position);
            ret.y = values[position].y + delta(y, position);
            ret.z = values[position].z + delta(z, position);
            ret.s = values[position].s + delta(s, position);
            }
      }
      return ret;
   }
   
};

class timer {
private:
   double starttime;
   double interval;
public:
   timer() {
      interval=10;
      starttime=gettime();
   } 
   double gettime(void) {
#ifdef WIN32
      struct _timeb timebuffer;
      _ftime( &timebuffer ); // time = Time in seconds since midnight (00:00:00), January 1, 1970, UTC, 
        // millitm = Fraction of a second in milliseconds
      return (double) timebuffer.time + 1.e-3 * (double) timebuffer.millitm;
#else
      struct timeval tp;
      gettimeofday(&tp,NULL);
      return((double)tp.tv_sec+(double)tp.tv_usec/(double)1000000);  
#endif
   }
   float getfraction(void) {
      return (float)(fmod(gettime()-starttime,interval)/interval);
   }
};

timer TimeSensor;

position_interpolator_class PositionInterpolator;
orientation_interpolator_class OrientationInterpolator;

int   idata_from_key = 0;
float fdata_from_key = 0.0;
static GLboolean tracking = GL_FALSE;
static int spindx, spindy;
static int startx, starty;
static int curx, cury;
static int prevx, prevy;       /* to get good deltas using glut */
static time_t bgn_time;
static int callbacks;
static int frames_per_sec = 0;
// static float fraction;
static Vec3f position, prevpos;
static Rotation orientation, prevorien;
static float angle = 0.0001f;
static float x_axis = 0.0f;
static float y_axis = 0.0f;
static float z_axis = 1.0f;
static float change = 0.0001f;
static float x_pos = 0.0f;
static float y_pos = 0.0f;
static float z_pos = -10.0f;

static void displayfn (void)
{
   /* Clear the screen */
   glClearColor(0.0, 0.0, 0.0, 1.0 );
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(60.0f, 1, 1, 1000);
   glMatrixMode (GL_MODELVIEW);
   glLoadIdentity();
   glEnable(GL_LIGHT0);
   glEnable(GL_DEPTH_TEST);

   //glTranslatef(0, 0, -10); // VRML Viewpoint 0 0 -10
   //fraction = TimeSensor.getfraction();
   //position = PositionInterpolator.interpolate(fraction);
   //glTranslatef(position.x, position.y, position.z);
   glTranslatef(x_pos, y_pos, z_pos);

   // glRotatef(Angle,Xvector,Yvector,Zvector) is responsible for rotating the object around an axis. 
   // Angle represents how much you would like to spin the object. 
   // Xvector, Yvector and Zvector parameters together represent the vector about which the rotation will occur.
   //orientation = OrientationInterpolator.interpolate(TimeSensor.getfraction());
   //orientation = OrientationInterpolator.interpolate(fraction);

   angle += change; // radians
   if (( angle * RADIANS_TO_DEGREES ) > 360.0 )
       angle = 0;

   orientation.s = angle; // positive rotates anit-clockwise, negative clockwise
   orientation.x = x_axis; // rotate around the X axis
   orientation.y = y_axis; // Y-axis
   orientation.z = z_axis;

   glRotatef( (float)(orientation.s * RADIANS_TO_DEGREES),
             orientation.x , orientation.y , orientation.z );

   draw_ac3d_model (NULL);

   glutSwapBuffers();
#if (!defined(FREEGLUT) && !defined(USE_IDLE_FUNC))
   glutPostRedisplay();
#endif
   callbacks++;
   time_t curr = time(0);
   if (curr != bgn_time) {
       frames_per_sec = callbacks;
       bgn_time = curr;
       callbacks = 0;
   }

   prevpos = position;
   prevorien = orientation;
}

static void key_help()
{
    int tframes = frames_per_sec;
    printf("Keyboard Help - to size and position the model.\n");
    printf(" ESC or q = Quit\n");
    printf(" s/S      = Slow/Speed up rotation speed. angle_rate=%f\n", change);
    printf(" 1-7      = Set x,y,x axis %.1f,%.1f,%.1f\n", x_axis, y_axis, z_axis);
    printf(" x/X      = Change x position (%.1f)\n", x_pos);
    printf(" y/Y      = Change y position (%.1f)\n", y_pos);
    printf(" z/Z      = Change z position (%.1f)\n", z_pos);
    printf(" ?/h      = This help and frame rate...\n");
    if (tframes) {
       printf("FPS %d, a=%f orientation x,y,z = %.1f, %.1f, %.1f\n", frames_per_sec,
            orientation.s * RADIANS_TO_DEGREES, 
            orientation.x , orientation.y , orientation.z );
    }
    printf("Rotate(change=%f,x=%.1f,y=%.1f,z=%.1f) Translate(x=%.1f,y=%.1f,z=%.1f)\n",
             change, x_axis, y_axis, z_axis, x_pos, y_pos, z_pos );
}


static void keyfn ( unsigned char c, int i1, int i2)
{
    int show = 0;
   if ((c == '?')||( c == 'h')) {
       key_help();
   } else if ((c == 'q')||(c == 0x1b)) {
       if (c == 'q')
           printf("%c pressed, exit\n",c);
       else
           printf("ESC pressed, exit\n");
       exit(0);
   } else if (c == '1') {
       x_axis = 1.0f;
       y_axis = 0.0f;
       z_axis = 0.0f;
       show = 1;
   } else if (c == '2') {
       x_axis = 0.0f;
       y_axis = 1.0f;
       z_axis = 0.0f;
       show = 1;
   } else if (c == '3') {
       x_axis = 0.0f;
       y_axis = 0.0f;
       z_axis = 1.0f;
       show = 1;
   } else if (c == '4') {
       x_axis = 1.0f;
       y_axis = 1.0f;
       z_axis = 0.0f;
       show = 1;
   } else if (c == '5') {
       x_axis = 1.0f;
       y_axis = 0.0f;
       z_axis = 1.0f;
       show = 1;
   } else if (c == '6') {
       x_axis = 0.0f;
       y_axis = 1.0f;
       z_axis = 1.0f;
       show = 1;
   } else if (c == '7') {
       x_axis = 1.0f;
       y_axis = 1.0f;
       z_axis = 1.0f;
       show = 1;
   } else if (c == 's') {
       change -= 0.0001f;
       show = 1;
   } else if (c == 'S') {
       change += 0.0001f;
       show = 1;
   } else if (c == 'r') {
       change = 0.0f;
       angle = 0.0f;
       x_axis = 0.0f;
       y_axis = 0.0f;
       z_axis = 1.0f;
       show = 1;
   } else if (c == 'x') {
       x_pos -= 1.0f;
       show = 1;
   } else if (c == 'X') {
       x_pos += 1.0f;
       show = 1;
   } else if (c == 'y') {
       y_pos -= 1.0f;
       show = 1;
   } else if (c == 'Y') {
       y_pos += 1.0f;
       show = 1;
   } else if (c == 'z') {
       z_pos -= 1.0f;
       show = 1;
   } else if (c == 'Z') {
       z_pos += 1.0f;
       show = 1;
   } else {
       printf("%c pressed, unused... ESC or q to exit\n",c);
   }
   if (show) {
       printf("Rotate(change=%f,x=%.1f,y=%.1f,z=%.1f) Translate(x=%.1f,y=%.1f,z=%.1f)\n",
             change, x_axis, y_axis, z_axis, x_pos, y_pos, z_pos );
   }

}

static void mousemove(int x, int y)
{
    prevx = curx;
    prevy = cury;
    curx = x;
    cury = y;
    if (curx != startx || cury != starty) {
        glutPostRedisplay();
        startx = curx;
        starty = cury;
    }
}


int main ( int argc, char **argv )
{
    tracking = GL_FALSE;
    spindx = spindy = startx = starty = curx = cury = prevx = prevy = 0;  /* to get good deltas using glut */
    bgn_time = time(0);
    callbacks = 0;

   glutInit(&argc, argv);
   glutInitWindowPosition(0, 0);
   glutInitWindowSize(640, 480);
   glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
   glutCreateWindow("ac_to_gl VRML Aequivalent");
   glutDisplayFunc(displayfn);
#if (defined(FREEGLUT) || defined(USE_IDLE_FUNC))
   /* seem freeglut changed the API!?!? */
   glutIdleFunc(displayfn);
#endif
   glutKeyboardFunc(keyfn);
   glutMotionFunc(mousemove);

   // ========================================================================
   // Did not understand any of this, so replaced it with my own ;=))
   // key's des PositionInterpolator's im VRML files hier einfuellen
   PositionInterpolator.append(0, Vec3f(0, 0, 0));
   PositionInterpolator.append(1, Vec3f(0, 0, 0));
   // key's des OrientationInterpolator's im VRML files hier einfuellen
   OrientationInterpolator.append(0, Rotation(0, 0, 1, 0));
   OrientationInterpolator.append(1, Rotation(0, 0, 1, 0));
   PositionInterpolator.check_begin_and_end();
   OrientationInterpolator.check_begin_and_end();
   // ========================================================================

   key_help();

   glutMainLoop();

   return 0;
}

/* eof - ac3dmain.cxx */

