/* ============================================================================
    Convert AC3D file to Three.js json code

    2013/05/24 12:07:02 - geoff - reports _at_ geoffair _dot_ info

    From NOTICE file
    NOTICE:  This ac_to_3j distribution contains source code that is
    placed into the public domain without copyright. These programs are freely
    distributable without licensing fees.  These programs are provided without
    guarantee or warrantee expressed or implied.

    If you use ac_to_3j in a commercial or shareware product, it would be nice
    if you gave credit where it is due.  If you make any modifications or
    improvements to ac_to_3j, I would greatly appreciate a copy of the
    improved code.

    The 'ac_to_3j' program reads an '.ac' file from
    standard input and emits a Three.js json file on standard
    output:

    eg ac_to_3j <folly.ac >folly.js

    The resulting file can be load by Three.js, and displayed 
    using WebGL.

    The json out form is - This is a Three.js export from blender with the simple cube model.
{
	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.66 Exporter",
		"vertices"      : 8,
		"faces"         : 6,
		"normals"       : 8,
		"colors"        : 0,
		"uvs"           : [],
		"materials"     : 1,
		"morphTargets"  : 0,
		"bones"         : 0
	},
	"scale" : 1.000000,
	"materials" : [	{
		"DbgColor" : 15658734,
		"DbgIndex" : 0,
		"DbgName" : "Material",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorDiffuse" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorSpecular" : [0.5, 0.5, 0.5],
		"depthTest" : true,
		"depthWrite" : true,
		"shading" : "Lambert",
		"specularCoef" : 50,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	}],
	"vertices" : [1,-1,-1,1,-1,1,-1,-1,1,-1,-1,-1,1,1,-1,0.999999,1,1,-1,1,1,-1,1,-1],
	"morphTargets" : [],
	"normals" : [0.577349,-0.577349,-0.577349,0.577349,-0.577349,0.577349,-0.577349,-0.577349,0.577349,-0.577349,-0.577349,-0.577349,0.577349,0.577349,-0.577349,-0.577349,0.577349,-0.577349,-0.577349,0.577349,0.577349,0.577349,0.577349,0.577349],
	"colors" : [],
	"uvs" : [],
	"faces" : [35,0,1,2,3,0,0,1,2,3,35,4,7,6,5,0,4,5,6,7,35,0,4,5,1,0,0,4,7,1,35,1,5,6,2,0,1,7,6,2,35,2,6,7,3,0,2,6,5,3,35,4,0,3,7,0,4,0,3,5],
	"bones" : [],
	"skinIndices" : [],
	"skinWeights" : [],
	"animation" : {}
}
    The 'faces' array starts with a bit flag
     QuadBit = 1;
     MatBit  = 2;
     FUvBit  = 4;
     VUvBit  = 8;
     FNorms  = 16;
     VNorms  = 32;
     FColor  = 64;
     VColor  = 128;
     followed by indexes into the appropriate array. So 35 indicates a QuadBit 1, so is indexes in sets 
     of 4 (0,1,2,3), Has MatBit 2, so the 4th item in the array, (0) is the material, and it has 
     vNorms (0,1,2,3), and has no vUvBit and no Vcolor bits. This value can change for each set.

     The general style of an AC3D .ac file is to give an array of vertices, then a 
     set of SURFaces with refs of 3, 4 or more items indexing into the array of vertices. 
     So here it is necessary to accumualate the array of vertices for each OBJECT, and then 
     adjust the index of the faces into the extended array of vertices.
     A later effort would be to also accumulate the uv values into an extended array,
     calculate a set of normals for each surface, and maybe even later add the materials...

   ============================================================================ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#ifdef _MSC_VER
#include <locale>
#include <iostream>
#endif
#include <time.h>
#include <string>
#include <vector>

using namespace std;

static const char *mod_name = "ac_to_3j";

static FILE *in_file = stdin;
static FILE *out_file = stderr;
static FILE *json_out = stdout;
static char *file_in = 0;
static char *out_json = 0;
#define MMX_BUFFER 2048

static int verbosity = 1;
int verb1() { return ((verbosity >= 1) ? 1 : 0); }
int verb2() { return ((verbosity >= 2) ? 1 : 0); }
int verb5() { return ((verbosity >= 5) ? 1 : 0); }
int verb9() { return ((verbosity >= 9) ? 1 : 0); }

void pgm_exit( int v )
{
    exit(v);
}

#ifndef TRUE
#define TRUE  1
#define FALSE 0
#endif

// faces bits
#define QuadBit 1
#define MatBit  2
#define FUvBit  4
#define VUvBit  8
#define FNorms  16
#define VNorms  32
#define FColor  64
#define VColor  128

#ifdef _WIN32
inline int strncasecmp(const char* lhs, const char* rhs, size_t sz)
{
    int iret;
    char* l = new char[sz];
    char* r = new char[sz];
    size_t i;
    for (i = 0; i < sz; i++) {
        l[i] = tolower(lhs[i]);
        r[i] = tolower(rhs[i]);
    }
    iret = strncmp(l, r, sz);
    delete l;
    delete r;
    return iret;
}
#endif

/* A single vertex */
typedef struct tagVERTEX {
    double x, y, z; // vertext x,y,z value
    int i;      // associated index
} VERTEX, *PVERTEX;

/* A triangle made up of three vertices */
typedef struct tagTRI {
    VERTEX v[3];
}TRI, *PTRI;

typedef struct tagFACE {
    int index[4];
    int mat;
    int nind[4];
    // flags
    int isquad; // is quad else tri
    int addmat; // add mat
    int addnorm; // add normals
    int addfuv; // add face uv
    int addvuv; // add vect uv
    int addfn;  // add face norms
    int addvnm; // add vector norms
    int addfcl; // add face color
    int addvcl; // add vect color
}FACE, *PFACE;

typedef std::vector<VERTEX> vVERT;
typedef std::vector<TRI>    vTRI;
typedef std::vector<int>    vINT;
typedef std::vector<FACE>   vFACE;

static vVERT vVertices;
static vTRI  vTris;
static vFACE vFaces;

static int extended_index = 0;

static int line_count = 0;
/* only show these warninging once per file */
static int shown_data = 0;
static int shown_crease = 0;
static int shown_texoff = 0;

///////////////////////////////////////////////////////////////////////////////////////////////
////////// Triangulation stuff
//////////////////////////////////////////////////////////////////////////////////////////////

#define COUNTER_CLOCKWISE 0
#define CLOCKWISE 1

/*
 * orientation
 *
 * Return either clockwise or counter_clockwise for the orientation
 * of the polygon.
	int n;                  	 Number of vertices 
	vertex v[];             	 The vertex list 
 */
int orientation( int n, VERTEX v[] )
{
	double area;
	int i;

	/* Do the wrap-around first */
	area = v[n-1].x * v[0].y - v[0].x * v[n-1].y;

	/* Compute the area (times 2) of the polygon */
	for (i = 0; i < n-1; i++)
    	area += v[i].x * v[i+1].y - v[i+1].x * v[i].y;

	if (area >= 0.0)
    	return COUNTER_CLOCKWISE;
	else
    	return CLOCKWISE;
} /* End of orientation */

/*
 * determinant
 *
 * Computes the determinant of the three points.
 * Returns whether the triangle is clockwise or counter-clockwise.
	int p1, p2, p3;         	The vertices to consider
	vertex v[];             	The vertex list
 */
int determinant( int p1, int p2, int p3, VERTEX v[] )
{
	double x1, x2, x3, y1, y2, y3;
	double determ;

	x1 = v[p1].x;
	y1 = v[p1].y;
	x2 = v[p2].x;
	y2 = v[p2].y;
	x3 = v[p3].x;
	y3 = v[p3].y;

	determ = (x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1);
	if (determ >= 0.0)
    	return COUNTER_CLOCKWISE;
	else
    	return CLOCKWISE;
} /* End of determinant */

/*
 * distance2
 *
 * Returns the square of the distance between the two points
	float x1, y1, x2, y2;
 */
double distance2( double x1, double y1, double x2, double y2 )
{
	double xd, yd;           	/* The distances in X and Y */
	double dist2;            	/* The square of the actual distance */

	xd = x1 - x2;
	yd = y1 - y2;
	dist2 = xd * xd + yd * yd;

	return dist2;
} /* End of distance2 */

/*
 * no_interior
 *
 * Returns 1 if no other point in the vertex list is inside
 * the triangle specified by the three points.  Returns
 * 0 otherwise.
	int p1, p2, p3;         	The vertices to consider
	vertex v[];             	The vertex list
	int vp[];               	The vertex pointers (which are left)
	int n;                  	Number of vertices
	int poly_or;            	Polygon orientation
 */
int no_interior( int p1, int p2, int p3, VERTEX v[], int *pvp, int n, int poly_or )
{
	int i;                  	/* Iterative counter */
	int p;                  	/* The test point */

	for (i = 0; i < n; i++) {
    	p = pvp[i];          	/* The point to test */
    	if ((p == p1) || (p == p2) || (p == p3))
        	continue;       	/* Don't bother checking against yourself */
    	if (   (determinant( p2, p1, p, v ) == poly_or)
        	|| (determinant( p1, p3, p, v ) == poly_or)
        	|| (determinant( p3, p2, p, v ) == poly_or) ) {
        	continue;       	/* This point is outside */
    	} else {
        	return 0;       	/* The point is inside */
    	}
	}
	return 1;               	/* No points inside this triangle */
} /* End of no_interior */

#define BIG_NUM 1.0e30  	/* A number bigger than we expect to find here */

void save_triangle( TRI &t )
{
    vTris.push_back(t);
}

#define TRI_OK     0
#define TRI_FAILED 1

int triangulation_of_poly( int n, VERTEX v[], vTRI &tri )
{
	int prev, cur, next;    	/* Three points currently being considered */
	int count;              	/* How many vertices left */
	int min_vert;           	/* Vertex with minimum distance */
	int i;                  	/* Iterative counter */
	double dist;             	/* Distance across this one */
	double min_dist;         	/* Minimum distance so far */
	int poly_orientation;   	/* Polygon orientation */
	TRI t;             	        /* Triangle structure */
    int detres, noint;
    int *pvp = new int[n+1];
    //int *pdr = new int[n+1];
    //int *pni = new int[n+1];

	if (!pvp) {
    	fprintf( stderr, "Error, memroy FAILED! On %d points...", n );
        pgm_exit(1);
	}

	poly_orientation = orientation( n, v );
	for (i = 0; i < n; i++) {
    	pvp[i] = i;          	/* Put vertices in order to begin */
    }
    /* Slice off clean triangles until nothing remains */
	count = n;
	while (count > 3) {
    	min_dist = BIG_NUM;    	/* A real big number */
    	min_vert = 0;       	/* Just in case we don't find one... */
    	for (cur = 0; cur < count; cur++) {
        	prev = cur - 1;
        	next = cur + 1;
        	if (cur == 0)   	/* Wrap around on the ends */
            	prev = count - 1;
        	else if (cur == count - 1)
            	next = 0;
        	/* Pick out shortest distance that forms a good triangle */
            detres = (determinant( pvp[prev], pvp[cur], pvp[next], v ) == poly_orientation) ? 1 : 0;
            noint  = no_interior( pvp[prev], pvp[cur], pvp[next], v, pvp, count, poly_orientation );
            dist = distance2( v[pvp[prev]].x, v[pvp[prev]].y, v[pvp[next]].x, v[pvp[next]].y );
            //pdr[cur] = detres;
            //pni[cur] = noint;
        	if (detres && /* Same orientation as polygon */
            	noint  &&  /* No points inside */
            	(dist < min_dist) )
        	{   /* Better than any so far */
            	min_dist = dist;
            	min_vert = cur;
        	}
    	} /* End of for each vertex (cur) */

    	/* The following error should "never happen". */
    	if (min_dist == BIG_NUM) {
        	fprintf( stderr, "Start cnt %d, now %d.\n", n, count );
        	fprintf( stderr, "Error: Didn't find a triangle.\n" );
            //pgm_exit(1);
            return TRI_FAILED;
        }
    	prev = min_vert - 1;
    	next = min_vert + 1;
    	if (min_vert == 0)  	/* Wrap around on the ends */
        	prev = count - 1;
    	else if (min_vert == count - 1)
        	next = 0;

        /* Output this triangle */
        t.v[0].x = v[pvp[prev]].x;
    	t.v[0].y = v[pvp[prev]].y;
    	t.v[0].z = v[pvp[prev]].z;
        t.v[0].i = v[pvp[prev]].i;

    	t.v[1].x = v[pvp[min_vert]].x;
    	t.v[1].y = v[pvp[min_vert]].y;
    	t.v[1].z = v[pvp[min_vert]].z;
    	t.v[1].i = v[pvp[min_vert]].i;

    	t.v[2].x = v[pvp[next]].x;
    	t.v[2].y = v[pvp[next]].y;
    	t.v[2].z = v[pvp[next]].z;
    	t.v[2].i = v[pvp[next]].i;

    	save_triangle( t );
        tri.push_back(t);

        /* Remove the triangle from the polygon */
    	count -= 1;
    	for (i = min_vert; i < count; i++)
        	pvp[i] = pvp[i+1];
            
	} /* while (count > 3) */


    /* Output the final triangle */
    t.v[0].x = v[pvp[0]].x;
    t.v[0].y = v[pvp[0]].y;
    t.v[0].z = v[pvp[0]].z;
   	t.v[0].i = v[pvp[0]].i;

    t.v[1].x = v[pvp[1]].x;
    t.v[1].y = v[pvp[1]].y;
    t.v[1].z = v[pvp[1]].z;
   	t.v[1].i = v[pvp[1]].i;

    t.v[2].x = v[pvp[2]].x;
    t.v[2].y = v[pvp[2]].y;
    t.v[2].z = v[pvp[2]].z;
   	t.v[2].i = v[pvp[2]].i;

    save_triangle( t );
    tri.push_back(t);

    // release the pointers...
    delete pvp;
    //delete pdr;
    //delete pni;
    return TRI_OK;
    
} /* End of draw_poly */

/// end triangluation /////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////
/////// FILE IO
////////////////////////////////////////////////////////////////////
void log_prt( char *cp )
{
    fprintf(out_file, "%s", cp);
    if (out_file != stderr)
        fprintf(stderr, "%s", cp);
}

void prt_json( char *cp )
{
    fprintf(json_out, "%s", cp);
}
#ifdef _MSC_VER
#define _C_T _cdecl
#else
#define _C_T
#endif

int _C_T sprtf( const char * pf, ... )
{
   static char _s_sprtfbuf[MMX_BUFFER+1];
   char * pb = _s_sprtfbuf;
   int   i;
   va_list arglist;
   va_start(arglist, pf);
   //i = vsprintf( pb, pf, arglist );
#ifdef _MSC_VER
   i = vsprintf_s(pb,MMX_BUFFER,pf,arglist);
   if (i < 0) {
       fprintf(stderr, "WARNING: vsprintf_s FAILED\n");
       return i;
   }
#else
   i = vsnprintf(pb,MMX_BUFFER,pf,arglist);
#endif
   va_end(arglist);
   log_prt(pb);
   return i;
}

int _C_T json( const char * pf, ... )
{
   static char _s_sprtfbuf[MMX_BUFFER+1];
   char * pb = _s_sprtfbuf;
   int   i;
   va_list arglist;
   va_start(arglist, pf);
   //i = vsprintf( pb, pf, arglist );
#ifdef _MSC_VER
   i = vsprintf_s(pb,MMX_BUFFER,pf,arglist);
   if (i < 0) {
       fprintf(stderr, "WARNING: vsprintf_s FAILED\n");
       va_end(arglist);
       return i;
   }
#else
   i = vsnprintf(pb,MMX_BUFFER,pf,arglist);
#endif
   va_end(arglist);
   prt_json(pb);
   return i;
}


void normalize ( float *v )
{
  float mag = (float) sqrt ( v[0]*v[0] + v[1]*v[1] + v[2]*v[2] );

  if ( mag == 0.0f ) {
    v[0] = v[1] = 0.0f ;
    v[2] = 1.0f ;
    return ;
  }

  v[0] /= mag ;
  v[1] /= mag ;
  v[2] /= mag ;
}

void crossProduct ( float *dst, float *a, float *b )
{
  dst[0] = a[1] * b[2] - a[2] * b[1];
  dst[1] = a[2] * b[0] - a[0] * b[2];
  dst[2] = a[0] * b[1] - a[1] * b[0];
}

void makeNormal ( float *dst, float *a, float *b, float *c )
{
  float ab[3];
  ab[0] = b[0]-a[0];
  ab[1] = b[1]-a[1];
  ab[2] = b[2]-a[2];
  normalize ( ab );
  float ac[3];
  ac[0] = c[0]-a[0];
  ac[1] = c[1]-a[1];
  ac[2] = c[2]-a[2];
  normalize ( ac );
  crossProduct ( dst, ab, ac );
  normalize ( dst ); /* XXX DO WE REALLY NEED THIS LAST Normalize? XXX */
}

#define MAX_TEXTURES 10000    /* This *ought* to be enough! */
char *texture_fnames [ MAX_TEXTURES ];

int num_materials = 0;
int num_textures  = 0;
int num_vtabs     = 0;  // number of vertex tables
float *vtab       = 0;
int last_vtab_cnt = 0;  // number of vertices in this 'vtab'

int do_material ( char *s );
int do_object   ( char *s );
int do_name     ( char *s );
int do_data     ( char *s );
int do_texture  ( char *s );
int do_texrep   ( char *s );
int do_rot      ( char *s );
int do_loc      ( char *s );
int do_url      ( char *s );
int do_numvert  ( char *s );
int do_numsurf  ( char *s );
int do_surf     ( char *s );
int do_mat      ( char *s );
int do_refs     ( char *s );
int do_kids     ( char *s );
/* additions - still to be converted to GL */
int do_crease   ( char *s ); /* 2013/05/02 12:38:26 - add as 'crease 60.000'' */ 
int do_texoff   ( char *s ); /* 2013/05/06 - add as 'texoff 0 -0.299' */

int do_obj_world ( char *s );
int do_obj_poly  ( char *s );
int do_obj_group ( char *s );
int do_obj_light ( char *s );  /* 2013/05/06 add this object type */

#define PARSE_CONT   0
#define PARSE_POP    1

struct Tag
{
  const char *token ;
  int (*func) ( char *s );
} ;

 
void skip_spaces ( char **s )
{
  while ( **s == ' ' || **s == '\t' )
    (*s)++ ;
}

int trim_tail ( char *s )
{
    int len = (int)strlen(s);
    while (len) {
        len--;
        if (s[len] > ' ')
            break;
        s[len] = 0;
    }
    return len;
}

void skip_quotes ( char **s )
{
    skip_spaces ( s );
    if ( **s == '\"' )   {
        (*s)++ ;
        char *t = *s;
        while ( *t != '\0' && *t != '\"' ) t++;
        if ( *t != '\"' )
            fprintf ( stderr, "%d: %s: Mismatched double-quote ('\"') in '%s'\n", line_count, mod_name, *s );
        *t = '\0' ;
    }
    else
        fprintf ( stderr, "%d: %s: Expected double-quote ('\"') in '%s'\n", line_count, mod_name, *s );
}



int search ( Tag *tags, char *s )
{
    skip_spaces ( &s );
    for ( int i = 0 ; tags[i].token != NULL ; i++ ) {
        if ( strncasecmp ( tags[i].token, s, strlen(tags[i].token) ) == 0 ) {
            s += strlen ( tags[i].token );
            skip_spaces ( &s );
            return (*(tags[i].func))( s );
        }
    }

    /* 20130506 - sometime can be a blank line, especially at the end of the file, so */
    if (trim_tail(s) == 0)
        return PARSE_CONT;

    fprintf ( stderr, "%d: ac_to_gl: Unrecognised token '%s'\n", line_count, s );
    pgm_exit ( 1 );
    return PARSE_CONT;
}

Tag top_tags [] = {
  { "MATERIAL", do_material },
  { "OBJECT"  , do_object   }
};


Tag object_tags [] = {
  { "name"    , do_name     },
  { "data"    , do_data     },
  { "texture" , do_texture  },
  { "texrep"  , do_texrep   },
  { "rot"     , do_rot      },
  { "loc"     , do_loc      },
  { "url"     , do_url      },
  { "numvert" , do_numvert  },
  { "numsurf" , do_numsurf  },
  { "kids"    , do_kids     },
  { "crease"  , do_crease   },  /* 2013/05/02 12:38:26 - add as 'crease 60.000'' */
  { "texoff"  , do_texoff   },  /* 2013/05/06 - add as 'texoff 0 -0.299' */
  /* always last */
  { NULL, NULL }
};

Tag surf_tag [] = {
  { "SURF"    , do_surf     },
  { NULL, NULL }
};

Tag surface_tags [] = {
  { "mat"     , do_mat      },
  { "refs"    , do_refs     },
  { NULL, NULL }
};

Tag obj_type_tags [] = {
  { "world", do_obj_world },
  { "poly" , do_obj_poly  },
  { "group", do_obj_group },
  { "light", do_obj_light }, /* 2013/05/06 add this object type */
  { NULL, NULL }
};

#define OBJ_WORLD  0
#define OBJ_POLY   1
#define OBJ_GROUP  2
#define OBJ_LIGHT  3

int do_obj_world ( char *s ) { return OBJ_WORLD ; } 
int do_obj_poly  ( char *s ) { return OBJ_POLY  ; }
int do_obj_group ( char *s ) { return OBJ_GROUP ; }
int do_obj_light ( char *s ) { return OBJ_LIGHT ; } /* 2013/05/06 add this object type */

int inside_begin_end = FALSE ;
int last_flags       = -1 ;
int last_num_kids    = -1 ;
int current_flags    = -1 ;
int texture_enabled  = FALSE ;
int need_texture     = FALSE ;

void doBegin ( int flags )
{
    if ( last_flags == -1 )
        last_flags = ~flags ;
    if ( inside_begin_end ) {
/*
    if ( last_flags == flags )
      return ;
*/
        //printf ( "  glEnd ();\n" );
    }

    if ( ( (flags>>4) & 0x01 ) != ((last_flags>>4) & 0x01 ) )
    {
        if ( (flags>>4) & 0x01 ) {
            /* Smooth Shaded */ ;
        } else {
            /* Flat Shaded */ ;
        }
    }

    if ( ( (flags>>4) & 0x02 ) != ((last_flags>>4) & 0x02 ) )
    {
        if ( (flags>>4) & 0x02 ) {
            //printf ( "  glDisable ( GL_CULL_FACE );\n" );
        } else {
            //printf ( "  glEnable ( GL_CULL_FACE );\n" );
        }
    }

    if (( last_flags & 0x0F ) == 0   /* GL_POLYGON */ &&
        (      flags & 0x0F ) != 0   /* Line of some sort */ ) {
        // printf ( "  glDisable ( GL_LIGHTING );\n" );
    } else if (( last_flags & 0x0F ) != 0   /* Line of some sort */ &&
               (      flags & 0x0F ) == 0   /* GL_POLYGON */ ) {
        // printf ( "  glEnable ( GL_LIGHTING );\n" );
    }
    last_flags = flags;

    switch ( flags & 0x0F )
    {
    case 0:
        //printf ( "  glBegin ( GL_POLYGON    );\n" ); 
        break;
    case 1:
        //printf ( "  glBegin ( GL_LINE_LOOP  );\n" ); 
        break;
    case 2:
        //printf ( "  glBegin ( GL_LINE_STRIP );\n" ); 
        break;
    default:
        fprintf ( stderr, "%d: %s: Illegal surface type 0x%02x\n", line_count, mod_name, flags );
        break;
    }

    inside_begin_end = TRUE ;
}

void not_inside_begin_end ()
{
    if ( inside_begin_end ) {
        //printf ( "  glEnd ();\n" );
    }
    inside_begin_end = FALSE ;
}

/* ---------------------------------------------------------------
   How to read this and generate a json section
   AC3D:
   MATERIAL "Material" rgb 0.8000 0.8000 0.8000  amb 1.0000 1.0000 1.0000  emis 0.0000 0.0000 0.0000 \
        spec 0.5000 0.5000 0.5000  shi 50 trans 0.0000
    3J JSON:
	"materials" : [	{
		"DbgColor" : 15658734,
		"DbgIndex" : 0,
		"DbgName" : "Material",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorDiffuse" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorSpecular" : [0.5, 0.5, 0.5],
		"depthTest" : true,
		"depthWrite" : true,
		"shading" : "Lambert",
		"specularCoef" : 50,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	}],
    
    --------------------------------------------------------------- */
// static int mat_index = 0;
typedef struct tagMAT {
    int DbgIndex;   // = mat_index
    char *DbgName;
    float colorAmbient[3];
    float colorDiffuse[3];
    float colorSpecular[3];
}MAT, *PMAT;

int do_material ( char *s )
{
    char name [ 1024 ];
    float rgb [3];
    float amb [3];
    float emis[3];
    float spec[3];
    int   shi ;
    float trans ;

    if ( sscanf ( s, "%s rgb %f %f %f amb %f %f %f emis %f %f %f spec %f %f %f shi %d trans %f",
        name,
        &rgb [0], &rgb [1], &rgb [2],
        &amb [0], &amb [1], &amb [2],
        &emis[0], &emis[1], &emis[2],
        &spec[0], &spec[1], &spec[2],
        &shi,
        &trans ) != 15 )
    {
        fprintf ( stderr, "%d: %s: Can't parse this MATERIAL:\n", line_count, mod_name );
        trim_tail(s);
        fprintf ( stderr, "%s: MATERIAL %s\n", mod_name, s );
    }
    else
    {
        char *nm = name ;

        skip_quotes ( &nm );

        not_inside_begin_end ();

        //printf ( "\n" );

        if ( *nm != '\0' ) {
            // printf ( "  /* Material #%d %s */\n", num_materials, nm );
        } else {
            // printf ( "  /* Material #%d [Not Named]*/\n", num_materials );
        }

        //printf ( "  glNewList ( %d + materials_base, GL_COMPILE );\n",  num_materials );
        //printf ( "  {\n" );
        //printf ( "    GLfloat rgb [ 4 ];\n" );
        //printf ( "    rgb[0]=%ff; rgb[1]=%ff; rgb[2]=%ff; rgb[3]=%ff;\n", amb[0],amb[1],amb[2],1.0f );
        //printf ( "    glMaterialfv ( GL_FRONT_AND_BACK, GL_AMBIENT, rgb );\n" );
        //printf ( "    rgb[0]=%ff; rgb[1]=%ff; rgb[2]=%ff; rgb[3]=%ff;\n", rgb[0],rgb[1],rgb[2],1.0f-trans );
        //printf ( "    glMaterialfv ( GL_FRONT_AND_BACK, GL_DIFFUSE, rgb );\n" );
        //printf ( "    rgb[0]=%ff; rgb[1]=%ff; rgb[2]=%ff; rgb[3]=%ff;\n", spec[0],spec[1],spec[2],1.0f );
        //printf ( "    glMaterialfv ( GL_FRONT_AND_BACK, GL_SPECULAR, rgb );\n" );
        //printf ( "    rgb[0]=%ff; rgb[1]=%ff; rgb[2]=%ff; rgb[3]=%ff;\n", emis[0],emis[1],emis[2],1.0f );
        //printf ( "    glMaterialfv ( GL_FRONT_AND_BACK, GL_EMISSION, rgb );\n" );
        //printf ( "    glMaterialf  ( GL_FRONT_AND_BACK, GL_SHININESS, %d.0f );\n", shi );
        //printf ( "  }\n" );
        //printf ( "  glEndList ();\n" );
    }

    num_materials++;
    return PARSE_CONT;
}


int do_object   ( char *s )
{
    static int shownlight = 0;
    static int firsttime = TRUE ;
    if ( firsttime ) {
        firsttime = FALSE ;

        // printf ( "}\n" );
        // printf ( "\n" );
        // printf ( "static void draw_objects ()\n" );
        // printf ( "{\n" );
    }

    int obj_type = search ( obj_type_tags, s );  

    trim_tail(s);

    switch ( obj_type )
    {
    case OBJ_WORLD:
        //printf ( "  /* WORLD Object */\n" ); 
        break;
    case OBJ_POLY:
        //printf ( "  /* POLY  Object */\n" ); 
        break;
    case OBJ_GROUP:
        //printf ( "  /* GROUP Object */\n" ); 
        break;
    case OBJ_LIGHT: /* 2013/05/06 add this object type */
        //printf ( "  /* LIGHT Object */\n" ); 
        if (shownlight == 0)
            fprintf ( stderr, "%d: %s: LIGHT Object - TODO\n", line_count, mod_name );
        shownlight++;
        break ; 
    default: 
        fprintf ( stderr, "%d: %s: Unknown OBJECT type %s\n", line_count, mod_name, s );
        //printf ( "  /* '%s' Object? */\n", s );
        break;
    }

    char buffer [ 1024 ];

    need_texture = FALSE ;

    not_inside_begin_end ();
    // printf ( "  glPushMatrix ();\n" );

    while ( fgets ( buffer, 1024, in_file ) != NULL ) {
        line_count++;
        if ( search ( object_tags, buffer ) == PARSE_POP )
            break ;
    }
    int num_kids = last_num_kids ;

    for ( int i = 0 ; i < num_kids ; i++ ) {
        fgets ( buffer, 1024, in_file );
        line_count++;
        search ( top_tags, buffer );
    }

    not_inside_begin_end ();
    // printf ( "  glPopMatrix ();\n" );

    return PARSE_CONT ;
}


int do_name     ( char *s )
{
    skip_quotes ( &s );
    // printf ( "  /* name '%s' */\n", s );
    return PARSE_CONT ;
}

/* 2013/05/02 12:38:26 - geoff - changed behaviour
   Do NOT eat per data count, just get next line
 */
int do_data     ( char *s )
{
    int len = strtol ( s, NULL, 0 );

    if (shown_data == 0) {
        fprintf ( stderr, "%d: WARNING - 'data %d' string encountered - TODO\n",
            line_count, len);
        // fprintf ( stderr, "       un-tested and specification is weakly defined.\n" );
    }

    // printf ( "  /* data %d */\n", len );
  /* for ( int i = 0 ; i < len ; i++ )
    fgetc ( stdin ); */
    char buffer [ 1024 ];
    s = buffer;
    fgets ( s, 1024, in_file );
    line_count++;
    skip_spaces ( &s );
    trim_tail ( s );
    // printf ( "  /* %s */\n", s );

    shown_data++;

    return PARSE_CONT ;
}


int get_texture ( char *s )
{
    for ( int i = 0 ; i < num_textures ; i++ )
        if ( strcmp ( s, texture_fnames [ i ] ) == 0 )
            return i ;

    texture_fnames [ num_textures ] = new char [ strlen ( s ) + 1 ];
    strcpy ( texture_fnames [ num_textures ], s );
 
    return num_textures++ ;
}

int do_texture  ( char *s )
{
    static int last_tex = -1 ;

    skip_quotes ( &s );
  
    int tex = get_texture ( s );

    if ( tex != last_tex )
    {
        not_inside_begin_end ();
        //printf ( "  glBindTexture ( GL_TEXTURE_2D, texture_name [ %d ] ); /* \"%s\" */\n", tex, s );
 
        last_tex = tex ;
    }

    need_texture = TRUE ;

    return PARSE_CONT ;
}


int do_texrep   ( char *s )
{
    float texrep [ 2 ];

    if ( sscanf ( s, "%f %f", & texrep [ 0 ], & texrep [ 1 ] ) != 2 )
        fprintf ( stderr, "%d: %s: Illegal texrep record.\n", line_count, mod_name );

    return PARSE_CONT ;
}

int do_rot      ( char *s )
{
    float mat [ 4 ] [ 4 ];

    mat [ 0 ][ 3 ] = mat [ 1 ][ 3 ] = mat [ 2 ][ 3 ] =
    mat [ 3 ][ 0 ] = mat [ 3 ][ 1 ] = mat [ 3 ][ 2 ] = 0.0f ;
    mat [ 3 ][ 3 ] = 1.0f ; 

    if ( sscanf ( s, "%f %f %f %f %f %f %f %f %f",
        & mat [ 0 ] [ 0 ], & mat [ 0 ] [ 1 ], & mat [ 0 ] [ 2 ],
        & mat [ 1 ] [ 0 ], & mat [ 1 ] [ 1 ], & mat [ 1 ] [ 2 ],
        & mat [ 2 ] [ 0 ], & mat [ 2 ] [ 1 ], & mat [ 2 ] [ 2 ] ) != 9 )
        fprintf ( stderr, "%d: %s: Illegal rot record.\n", line_count, mod_name );

    not_inside_begin_end ();
    //  printf ( "  {\n" );
    //  printf ( "    static float rot [ 16 ] = {" );
    for ( int i = 0 ; i < 4 ; i++ ) {
        for ( int j = 0 ; j < 4 ; j++ ) {
            //printf ( "%ff,", mat [ i ][ j ] );
        }
    }
    //  printf ( "  } ;\n" );
    // printf ( "    glMultMatrixf ( rot );\n" );
    // printf ( "  }\n" );

    return PARSE_CONT ;
}

int do_loc      ( char *s )
{
    float loc [ 3 ];

    if ( sscanf ( s, "%f %f %f", & loc [ 0 ], & loc [ 1 ], & loc [ 2 ] ) != 3 ) {
        fprintf ( stderr, "%d: %s: Illegal loc record.\n", line_count, mod_name );
        fprintf ( stderr, "%s", s );
        pgm_exit ( 1 );
    }

    not_inside_begin_end ();
    // printf ( "  glTranslatef ( %ff, %ff, %ff );\n", loc[0],loc[1],loc[2] );
    return PARSE_CONT ;
}

int do_url      ( char *s )
{
    skip_quotes ( & s );
    // printf ( "/* URL: \"%s\" */\n", s );

    return PARSE_CONT ;
}

void show_vertex(VERTEX &v)
{
    sprtf(" %ff, %ff, %ff\n", v.x, v.y, v.z );
}

int do_numvert  ( char *s )
{
    char vert_buf[1024];
    VERTEX v;
    int nv = strtol ( s, NULL, 0 );
    if (vtab)
        delete vtab;    /* delete any previous */
    if (nv)
        vtab = new float [ 3 * nv ];

    extended_index += last_vtab_cnt;    // get the extended index value
    last_vtab_cnt = nv;                 // and set the current
    num_vtabs++;
    //printf( "\n" );
    //printf ( "  static float vtab_%d [ %d ] =\n", num_vtabs, nv * 3 );
    //printf ( "  {\n" );
    if (verb9()) sprtf("%d: got %d vertices\n", num_vtabs, nv );

    for ( int i = 0 ; i < nv ; i++ ) {
        fgets ( vert_buf, 1024, in_file );
        line_count++;

        if ( sscanf ( vert_buf, "%f %f %f", &vtab[3*i+0], &vtab[3*i+1], &vtab[3*i+2] ) != 3 ) {
            fprintf ( stderr, "%d: %s: Illegal vertex record.\n", line_count, mod_name );
            fprintf ( stderr, "%s", vert_buf );
            pgm_exit ( 1 );
        }
        v.x = vtab[3*i+0];
        v.y = vtab[3*i+1];
        v.z = vtab[3*i+2];
        vVertices.push_back(v);
        if (verb9()) show_vertex(v);
        // printf ( "    %ff, %ff, %ff,  /* V%d */\n", vtab[3*i+0],vtab[3*i+1],vtab[3*i+2], i );
    }

    //printf ( "  } ;\n" );
    //printf ( "\n" );

    return PARSE_CONT ;
}

int do_numsurf  ( char *s )
{
    char surf_buf [1024];
    int ns = strtol ( s, NULL, 0 );

    for ( int i = 0 ; i < ns ; i++ ) {
        fgets ( surf_buf, 1024, in_file );
        line_count++;
        search ( surf_tag, surf_buf );
    }
    return PARSE_CONT ;
}

int do_surf     ( char *s )
{
    current_flags = strtol ( s, NULL, 0 );
    char buffer [1024];

    while ( fgets ( buffer, 1024, in_file ) != NULL ) {
        line_count++;
        if ( search ( surface_tags, buffer ) == PARSE_POP )
            break ;
    }
    return PARSE_CONT ;
}

int do_mat      ( char *s )
{
    static int last_mat = -1 ;
    int mat = strtol ( s, NULL, 0 );
    if ( mat != last_mat ) {
        not_inside_begin_end ();
        //printf ( "  glCallList ( %d + materials_base ); /* Material #%d */\n", mat, mat );
        last_mat = mat ;
    }
    return PARSE_CONT ;
}

/* ----------------------------------------------------------------------------
   This is a SURFace made up of n point.
   If n == 3, then is already a triangle
   If n > 3, need to break that surface into triangle
   1: Collect the vertices that make up this surface
  int triangulation_of_poly( int n, VERTEX v[], vTRI &tri )
   ---------------------------------------------------------------------------- */
int split_poly_to_tris( int nrefs, vVERT &vVerts, vTRI &tri )
{
    size_t max = vVerts.size();
    PVERTEX pvt = new VERTEX[max];
    size_t ii;
    VERTEX v;
    for (ii = 0; ii < max; ii++) {
        v = vVerts[ii];
        pvt[ii].x = v.x;
        pvt[ii].y = v.y;
        pvt[ii].z = v.z;
        pvt[ii].i = v.i;    // index to this vertex
    }
    int res = triangulation_of_poly( nrefs, pvt, tri );
    delete pvt;
    return res;
}

int do_refs     ( char *s )
{
    int nrefs = strtol ( s, NULL, 0 );
    char buffer [ 1024 ];
    int i, index, off;
    VERTEX vert;
    vVERT vVerts;
    FACE face;

    if ( nrefs == 0 )
        return PARSE_POP ;

    if ( need_texture && ! texture_enabled )
    {
        not_inside_begin_end ();
        //printf ( "  glEnable ( GL_TEXTURE_2D );\n" );
        texture_enabled = TRUE ;
    }
    else if ( ! need_texture && texture_enabled )
    {
        not_inside_begin_end ();
        //printf ( "  glDisable ( GL_TEXTURE_2D );\n" );
        texture_enabled = FALSE ;
    }

    if (!vtab || (last_vtab_cnt == 0)) {
        // YOWEEE! got reference but no vertices - no 'vtab'!!!
        fprintf ( stderr, "%d: %s: Got ref %d record but no vertices.\n", line_count, mod_name, nrefs );
        fprintf ( stderr, "%s", buffer );
        pgm_exit ( 1 );
    }

    int   *vlist  = new int   [ nrefs ];
    float *tlistu = new float [ nrefs ];
    float *tlistv = new float [ nrefs ];
 
    doBegin ( current_flags );
    if (verb9()) sprtf("processing refs %d\n", nrefs );

    for (i = 0 ; i < nrefs ; i++ ) {
        fgets ( buffer, 1024, in_file );
        line_count++;
        if ( sscanf ( buffer, "%d %f %f", &vlist[i], &tlistu[i], &tlistv[i] ) != 3 ) {
            fprintf ( stderr, "%d: %s: Illegal ref record.\n", line_count, mod_name );
            fprintf ( stderr, "%s", buffer );
            pgm_exit ( 1 );
        }
        index = vlist[i];   // get the index
        if (index < last_vtab_cnt) {
            off = index * 3;
            vert.x = vtab[off+0];
            vert.y = vtab[off+1];
            vert.z = vtab[off+2];
            vert.i = index + extended_index;
            vVerts.push_back(vert); // accumulating the vertices list
            if (verb9()) show_vertex(vert);
        } else {
            fprintf ( stderr, "%d: %s: Invalid ref record.\n", line_count, mod_name );
            fprintf ( stderr, "%s", buffer );
            pgm_exit ( 1 );
        }
    }

    if ( nrefs >= 3 ) {
        float nrm [ 3 ];
        int i1, i2, i3;
        makeNormal ( nrm, &vtab[3*vlist[0]],&vtab[3*vlist[1]],&vtab[3*vlist[2]] );
        //printf ( "  glNormal3f ( %ff, %ff, %ff );\n", nrm[0], nrm[1], nrm[2] );
        if (nrefs == 3) {
            // so the indexes for this tri are -
            i1 = vlist[0]+extended_index;
            i2 = vlist[1]+extended_index;
            i3 = vlist[2]+extended_index;
            // and the vertices would then be 
            // vtab[3*i1] 
            // vtab[3*i2]
            // vtab[3*i3]
            memset(&face,0,sizeof(FACE));
            face.index[0] = i1;
            face.index[1] = i2;
            face.index[2] = i3;
            vFaces.push_back(face);
        } else {
            // there are more than 3 so need to triangulate this set of vertices
            vTRI tri;
            int res = split_poly_to_tris( nrefs, vVerts, tri );
            size_t max, ii;
            max = tri.size();
            ii = vVerts.size();
            if (res == TRI_OK) {
                //if (verb5()) 
                    sprtf("From %d refs, %d verts, got %d tris...\n", nrefs, (int)ii, (int)max );
            }
            for (ii = 0; ii < max; ii++) {
                TRI t = tri[ii];
                memset(&face,0,sizeof(FACE));
                face.index[0] = t.v[0].i;
                face.index[1] = t.v[1].i;
                face.index[2] = t.v[2].i;
                vFaces.push_back(face);
            }
        }
    }

    //for (i = 0 ; i < nrefs ; i++ ) {
        //printf ( "  glTexCoord2f ( %ff, %ff );\n", tlistu[i], tlistv[i] );
        //printf ( "  glVertex3fv ( & ( vtab_%d [ %d ] ) ); /* V%d */\n",
        //                                      num_vtabs, vlist[i] * 3, vlist[i] );
    //    index = vlist[i];   // get the index
    //}

    delete vlist;
    delete tlistu;
    delete tlistv;
    return PARSE_POP ;
}

int do_kids     ( char *s )
{
  last_num_kids = strtol ( s, NULL, 0 );

  return PARSE_POP ;
}

/* 2013/05/02 12:38:26 - add as 'crease 60.000'' */
int do_crease ( char *s )
{
    double d;
    skip_spaces ( & s );
    d = atof ( s );
    if (shown_crease == 0) {
        fprintf ( stderr, "%d: WARNING - 'crease %f' string encountered! TODO\n",
          line_count, d);
    }
    if ( trim_tail( s ) ) {
        //printf( "  /* crease %s */\n", s );
    }

    shown_crease++;

    return PARSE_CONT ;
}

int do_texoff   ( char *s ) /* 2013/05/06 - add as 'texoff 0 -0.299' */
{
    float off [ 2 ];
    skip_spaces ( & s );
    trim_tail(s);
    if ( sscanf ( s, "%f %f", &off[0], &off[1] ) != 2 ) {
        if (shown_texoff == 0) {
            fprintf ( stderr, "%d: WARNING - 'texoff %s' string encountered! TODO\n", line_count, s );
        }
    } else {
        if (shown_texoff == 0) {
            fprintf ( stderr, "%d: WARNING - 'texoff %f %f' string encountered! TODO\n", line_count,
                off[0], off[1] );
        }
    }
    //printf( "  /* texoff %s */\n", s );
    shown_texoff++;
    return PARSE_CONT ;
}

void preamble ()
{
    json( "{\n" );
}

static const char *ts_form = "%04d-%02d-%02d %02d:%02d:%02d UTC";
static char _s_tmbuf[128];
// Creates the UTC time string
char *get_date_time()
{
    char *ps = _s_tmbuf;
    time_t Timestamp = time(0);
    tm  *ptm;
    ptm = gmtime (& Timestamp);
    sprintf (
        ps,
        ts_form,
        ptm->tm_year+1900,
        ptm->tm_mon+1,
        ptm->tm_mday,
        ptm->tm_hour,
        ptm->tm_min,
        ptm->tm_sec );
    return ps;
}


void postamble ()
{
    size_t max = vVertices.size();
    size_t m2  = vFaces.size();
    size_t ii;
    VERTEX v;
    FACE face;
    int flag;
    json("\t\"metadata\" :\n");
	json("\t{\n");
	json("\t\t\"formatVersion\" : 3.1,\n");
	json("\t\t\"generatedBy\"   : \"%s %s\",\n", mod_name, get_date_time());
	json("\t\t\"vertices\"      : %d,\n", (int)max); 
	json("\t\t\"faces\"         : %d,\n", (int)m2);
	json("\t\t\"normals\"       : %d,\n", 0);
	json("\t\t\"colors\"        : 0,\n");
	json("\t\t\"uvs\"           : [],\n");
	json("\t\t\"materials\"     : 1,\n");
	json("\t\t\"morphTargets\"  : 0,\n");
	json("\t\t\"bones\"         : 0\n");
	json("\t},\n");

	json("\t\"scale\" : 1.000000,\n");

    json("\t\"vertices\" : [");
    for (ii = 0; ii < max; ii++) {
        v = vVertices[ii];
        if (ii) json(",");
        json("%f,%f,%f", v.x, v.y, v.z );
    }
    json("],\n");

	json("\t\"morphTargets\" : [],\n");

	json("\t\"normals\" : [],\n");

	json("\t\"colors\" : [],\n");

	json("\t\"uvs\" : [],\n");

    json("\t\"faces\" : [");
    int cnt, i;
    for (ii = 0; ii < max; ii++) {
        face = vFaces[ii];
        flag = 0;
        if (face.isquad) flag += QuadBit;
        if (face.addmat) flag += MatBit;
        if (face.addfuv) flag += FUvBit;
        if (face.addvuv) flag += VUvBit;
        if (face.addfn)  flag += FNorms;
        if (face.addvnm) flag += VNorms;
        if (face.addfcl) flag += FColor;
        if (face.addvcl) flag += VColor;
        if(ii) json(",");
        json("%d,",flag);
        cnt = 3;
        if (face.isquad) cnt = 4;
        for (i = 0; i < cnt; i++) {
            json("%d", face.index[i]);
            if ((i + 1) < cnt) json(",");
        }
        if (face.addmat) json(",%d",face.mat);
        if (face.addvnm) {
            json(",");
            for (i = 0; i < cnt; i++) {
                json("%d", face.nind[i]);
                if ((i + 1) < cnt) json(",");
            }
        }
    }

    json("],\n");

	json("\t\"bones\" : [],\n");
	json("\t\"skinIndices\" : [],\n");
	json("\t\"skinWeights\" : [],\n");
	json("\t\"animation\" : {}\n");

    json( "}\n" );
}

void give_help( char * name )
{
    printf("%s, version %s, at %s\n", name, __DATE__, __TIME__);
    printf("Usage: %s [options] [in_file]\n", name);
    printf("Or: %s <in_file >out_file\n", name);
    printf("Options\n");
    printf(" --help (-h or -?) = This help and exit(0)\n");
    printf(" --in file    (-i) = Input file. (def=stdin)\n");
    printf(" --out file   (-o) = Output file. (def=stdout)\n");
    printf(" --log file   (-l) = Set log output file. (def=stderr)\n");
    printf(" --verb num   (-v) = Set verbosity 0-9. (def=%d)\n", verbosity);
    printf("Purpose:\n");
    printf("To parse an AC3D .ac file and output Three.js json\n");
    pgm_exit(0);
}

#define IS_DIGIT(a) (( a >= '0' ) && ( a <= '9' ))

int is_digits(char *txt)
{
    size_t len = strlen(txt);
    size_t i;
    for(i = 0; i < len; i++) {
        if (!IS_DIGIT(txt[i]))
            return 0;
    }
    return (int)len;
}

int parse_command( int argc, char **argv )
{
    int i, c, i2;
    char *arg, *sarg;
    std::string s;

    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        s += " ";
        s += arg;
        c = *arg;
        if (c == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help( argv[0] ); // never returns
                break;
            case 'i':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    s += " ";
                    s += sarg;
                    in_file = fopen(sarg, "r");
                    if (!in_file) {
                        sprtf( "ERROR: Unable to open file [%s]\n", sarg);
                        return 1;
                    }
                    file_in= strdup(sarg);
                } else {
                    sprtf("Input file name must follow %s\n", arg );
                    goto Bad_Cmd;
                }
                break;
            case 'o':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    s += " ";
                    s += sarg;
                    json_out = fopen(sarg, "w");
                    if (!json_out) {
                        sprtf( "ERROR: Unable to create out file [%s]\n", sarg);
                        return 1;
                    }
                    out_json = strdup(sarg);
                } else {
                    sprtf("Output file name must follow %s\n", arg );
                    goto Bad_Cmd;
                }
                break;
            case 'l':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    s += " ";
                    s += sarg;
                    out_file = fopen(sarg, "w");
                    if (!out_file) {
                        sprtf( "ERROR: Unable to create log file [%s]\n", sarg);
                        return 1;
                    }
                } else {
                    sprtf("Output log file name must follow %s\n", arg );
                    goto Bad_Cmd;
                }
                break;
            case 'v':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    s += " ";
                    s += sarg;
                    if (is_digits(sarg)) {
                        verbosity = atoi(sarg);
                    } else {
                        sprtf("%s: ERROR: Only digit can follow [%s]\n", mod_name, arg);
                        goto Bad_Cmd;
                    }
                } else {
                    sprtf("Verbosity number must follow %s\n", arg );
                    goto Bad_Cmd;
                }
                break;
            default:
Bad_Cmd:
                sprtf("%s: ERROR: Unknown argument %s! Aborting...\n", mod_name, arg);
                return 1;
                break;
            }
        } else {
            // assume an input file name
            in_file = fopen(arg, "r");
            if (!in_file) {
                fprintf(stderr, "ERROR: Unable to open file [%s]\n", arg);
                return 1;
            }
        }
    }
    if (verb1()) sprtf("CMD: %s\n", s.c_str());
    return 0;
}

int main ( int argc, char **argv )
{
    static char buffer [ 1024 ];
    int firsttime = TRUE ;

    if (parse_command(argc,argv))
        return 1;

    if (file_in && verb1()) 
        sprtf( "%s: Loading from file [%s]\n", mod_name, file_in);

    preamble ();

    while ( fgets ( buffer, 1024, in_file ) != NULL ) {
        line_count++;
        char *s = buffer ;
        /* Skip leading whitespace */
        skip_spaces ( & s );
        /* Skip blank lines and comments */
        if ( *s < ' ' && *s != '\t' ) continue ;
        if ( *s == '#' || *s == ';' ) continue ;

        if ( firsttime ) {
            firsttime = FALSE ;
            if ( strncasecmp ( s, "AC3D", 4 ) != 0 ) {
                fprintf ( stderr, "%s: Input is not an AC3D format file.\n", mod_name );
                pgm_exit ( 1 );
            }
        }
        else
            search ( top_tags, s );
    }

    postamble ();

    if (out_json && verb1())
        sprtf("%s: Three.js json written to [%s]\n", mod_name, out_json);

    return 0 ;
}

/* eof - ac_to_3j.cxx */
