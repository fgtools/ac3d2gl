/* ============================================================================
    Convert AC3D file to GL cxx code

    2013/05/02 12:38:26 - geoff - reports _at_ geoffair _dot_ info
        Change the behaviour of a 'data nn' token. Warn but treat it as just token plus 1 
        following line...
        Add 'texoff %f %f', but no GL code - TODO.
        Added OBJECT light but no GL code - TODO.

    03/06/2004  12:46 - Maybe the date of last update???
    * 6th Oct 1998 -- First public release. - per CHANGE file

    From NOTICE file
    NOTICE:  This ac_to_gl distribution contains source code that is
    placed into the public domain without copyright. These programs are freely
    distributable without licensing fees.  These programs are provided without
    guarantee or warrantee expressed or implied.

    If you use ac_to_gl in a commercial or shareware product, it would be nice
    if you gave credit where it is due.  If you make any modifications or
    improvements to ac_to_gl, I would greatly appreciate a copy of the
    improved code.

    From README file
        Converting AC3D Files into OpenGL Source Code.
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                       by Steve Baker <sjbaker1@airmail.net>

        There are two possible approaches to making use
        of models built in AC3D within an OpenGL program.

        * Write a loader for AC3D files that is linked into
          the application program.

        * Convert each AC3D file into a chunk of C/C++ code
          that can be compiled into the application.

        This package takes the second approach.

        The 'ac_to_gl' program reads an '.ac' file from
        standard input and emits a C++ file on standard
        output:

          eg

             ac_to_gl <folly.ac >folly.c++

        The resulting file can be compiled into a '.o' file
        that will render the object when linked into a suitable
        program.

        The calling program need simply call:

           void draw_ac3d_model ( void (*func)( char * ) ) ;

        The parameter is the address of a function that
        will load the provided texture map into OpenGL.
        If the parameter is NULL then no texture will be
        loaded.

   ============================================================================ */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#ifdef _MSC_VER
#include <locale>
#include <iostream>
#endif
using namespace std;


#ifndef TRUE
#define TRUE  1
#define FALSE 0
#endif

#ifdef _WIN32
inline int strncasecmp(const char* lhs, const char* rhs, size_t sz)
{
    int iret;
  char* l = new char[sz];
  char* r = new char[sz];
  unsigned int i;

  for (i = 0; i < sz; i++)
  {
    l[i] = tolower(lhs[i]);
    r[i] = tolower(rhs[i]);
  }
  iret = strncmp(l, r, sz);
  delete l;
  delete r;
  return iret;
}
#endif

static int line_count = 0;
/* only show these warninging once per file */
static int shown_data = 0;
static int shown_crease = 0;
static int shown_texoff = 0;

void normalize ( float *v )
{
  float mag = (float) sqrt ( v[0]*v[0] + v[1]*v[1] + v[2]*v[2] ) ;

  if ( mag == 0.0f )
  {
    v[0] = v[1] = 0.0f ;
    v[2] = 1.0f ;
    return ;
  }

  v[0] /= mag ;
  v[1] /= mag ;
  v[2] /= mag ;
}

void crossProduct ( float *dst, float *a, float *b )
{
  dst[0] = a[1] * b[2] - a[2] * b[1] ;
  dst[1] = a[2] * b[0] - a[0] * b[2] ;
  dst[2] = a[0] * b[1] - a[1] * b[0] ;
}

void makeNormal ( float *dst, float *a, float *b, float *c )
{
  float ab[3] ;
  ab[0] = b[0]-a[0] ;
  ab[1] = b[1]-a[1] ;
  ab[2] = b[2]-a[2] ;
  normalize ( ab ) ;
  float ac[3] ;
  ac[0] = c[0]-a[0] ;
  ac[1] = c[1]-a[1] ;
  ac[2] = c[2]-a[2] ;
  normalize ( ac ) ;
  crossProduct ( dst, ab, ac ) ;
  normalize ( dst ) ; /* XXX DO WE REALLY NEED THIS LAST Normalize? XXX */
}

#define MAX_TEXTURES 10000    /* This *ought* to be enough! */
char *texture_fnames [ MAX_TEXTURES ] ;

int num_materials = 0 ;
int num_textures  = 0 ;
int num_vtabs     = 0 ;
float *vtab = NULL ;

int do_material ( char *s ) ;
int do_object   ( char *s ) ;
int do_name     ( char *s ) ;
int do_data     ( char *s ) ;
int do_texture  ( char *s ) ;
int do_texrep   ( char *s ) ;
int do_rot      ( char *s ) ;
int do_loc      ( char *s ) ;
int do_url      ( char *s ) ;
int do_numvert  ( char *s ) ;
int do_numsurf  ( char *s ) ;
int do_surf     ( char *s ) ;
int do_mat      ( char *s ) ;
int do_refs     ( char *s ) ;
int do_kids     ( char *s ) ;
/* additions - still to be converted to GL */
int do_crease   ( char *s ) ; /* 2013/05/02 12:38:26 - add as 'crease 60.000'' */ 
int do_texoff   ( char *s ) ; /* 2013/05/06 - add as 'texoff 0 -0.299' */

int do_obj_world ( char *s ) ;
int do_obj_poly  ( char *s ) ;
int do_obj_group ( char *s ) ;
int do_obj_light ( char *s ) ;  /* 2013/05/06 add this object type */

#define PARSE_CONT   0
#define PARSE_POP    1

struct Tag
{
  const char *token ;
  int (*func) ( char *s ) ;
} ;

 
void skip_spaces ( char **s )
{
  while ( **s == ' ' || **s == '\t' )
    (*s)++ ;
}

int trim_tail ( char *s )
{
    int len = (int)strlen(s);
    while (len) {
        len--;
        if (s[len] > ' ')
            break;
        s[len] = 0;
    }
    return len;
}

void skip_quotes ( char **s )
{
  skip_spaces ( s ) ;

  if ( **s == '\"' )
  {
    (*s)++ ;

    char *t = *s ;

    while ( *t != '\0' && *t != '\"' )
      t++ ;

    if ( *t != '\"' )
      fprintf ( stderr, "%d: ac_to_gl: Mismatched double-quote ('\"') in '%s'\n", line_count, *s ) ;

    *t = '\0' ;
  }
  else
    fprintf ( stderr, "%d: ac_to_gl: Expected double-quote ('\"') in '%s'\n", line_count, *s ) ;
}



int search ( Tag *tags, char *s )
{
  skip_spaces ( &s ) ;

  for ( int i = 0 ; tags[i].token != NULL ; i++ ) {
    if ( strncasecmp ( tags[i].token, s, strlen(tags[i].token) ) == 0 )
    {
      s += strlen ( tags[i].token ) ;

      skip_spaces ( & s ) ;

      return (*(tags[i].func))( s ) ;
    }
  }

  /* 20130506 - sometime can be a blank line, especially at the end of the file, so */
  if (trim_tail(s) == 0)
      return PARSE_CONT;

  fprintf ( stderr, "%d: ac_to_gl: Unrecognised token '%s'\n", line_count, s ) ;
  exit ( 1 ) ;
}

Tag top_tags [] =
{
  { "MATERIAL", do_material },
  { "OBJECT"  , do_object   },
} ;


Tag object_tags [] =
{
  { "name"    , do_name     },
  { "data"    , do_data     },
  { "texture" , do_texture  },
  { "texrep"  , do_texrep   },
  { "rot"     , do_rot      },
  { "loc"     , do_loc      },
  { "url"     , do_url      },
  { "numvert" , do_numvert  },
  { "numsurf" , do_numsurf  },
  { "kids"    , do_kids     },
  { "crease"  , do_crease   },  /* 2013/05/02 12:38:26 - add as 'crease 60.000'' */
  { "texoff"  , do_texoff   },  /* 2013/05/06 - add as 'texoff 0 -0.299' */
  /* always last */
  { NULL, NULL }
} ;

Tag surf_tag [] =
{
  { "SURF"    , do_surf     },
  { NULL, NULL }
} ;

Tag surface_tags [] =
{
  { "mat"     , do_mat      },
  { "refs"    , do_refs     },
  { NULL, NULL }
} ;

Tag obj_type_tags [] =
{
  { "world", do_obj_world },
  { "poly" , do_obj_poly  },
  { "group", do_obj_group },
  { "light", do_obj_light }, /* 2013/05/06 add this object type */
  { NULL, NULL }
} ;


#define OBJ_WORLD  0
#define OBJ_POLY   1
#define OBJ_GROUP  2
#define OBJ_LIGHT  3

int do_obj_world ( char *s ) { return OBJ_WORLD ; } 
int do_obj_poly  ( char *s ) { return OBJ_POLY  ; }
int do_obj_group ( char *s ) { return OBJ_GROUP ; }
int do_obj_light ( char *s ) { return OBJ_LIGHT ; } /* 2013/05/06 add this object type */


int inside_begin_end = FALSE ;
int last_flags       = -1 ;
int last_num_kids    = -1 ;
int current_flags    = -1 ;
int texture_enabled  = FALSE ;
int need_texture     = FALSE ;

void doBegin ( int flags )
{
  if ( last_flags == -1 )
    last_flags = ~flags ;

  if ( inside_begin_end )
  {
/*
    if ( last_flags == flags )
      return ;
*/

    printf ( "  glEnd () ;\n" ) ;
  }

  if ( ( (flags>>4) & 0x01 ) != ((last_flags>>4) & 0x01 ) )
  {
    if ( (flags>>4) & 0x01 )
      /* Smooth Shaded */ ;
    else
      /* Flat Shaded */ ;
  }

  if ( ( (flags>>4) & 0x02 ) != ((last_flags>>4) & 0x02 ) )
  {
    if ( (flags>>4) & 0x02 )
      printf ( "  glDisable ( GL_CULL_FACE ) ;\n" ) ;
    else
      printf ( "  glEnable ( GL_CULL_FACE ) ;\n" ) ;
  }

  if (( last_flags & 0x0F ) == 0   /* GL_POLYGON */ &&
      (      flags & 0x0F ) != 0   /* Line of some sort */ )
    printf ( "  glDisable ( GL_LIGHTING ) ;\n" ) ;
  else
  if (( last_flags & 0x0F ) != 0   /* Line of some sort */ &&
      (      flags & 0x0F ) == 0   /* GL_POLYGON */ )
    printf ( "  glEnable ( GL_LIGHTING ) ;\n" ) ;

  last_flags = flags ;

  switch ( flags & 0x0F )
  {
    case 0 : printf ( "  glBegin ( GL_POLYGON    ) ;\n" ) ; break ;
    case 1 : printf ( "  glBegin ( GL_LINE_LOOP  ) ;\n" ) ; break ;
    case 2 : printf ( "  glBegin ( GL_LINE_STRIP ) ;\n" ) ; break ;
    default: fprintf ( stderr, "%d: ac_to_gl: Illegal surface type 0x%02x\n", line_count, flags ) ;
             break ;
  }

  inside_begin_end = TRUE ;
}


void not_inside_begin_end ()
{
  if ( inside_begin_end )
    printf ( "  glEnd () ;\n" ) ;

  inside_begin_end = FALSE ;
}


int do_material ( char *s )
{
  char name [ 1024 ] ;
  float rgb [3] ;
  float amb [3] ;
  float emis[3] ;
  float spec[3] ;
  int   shi ;
  float trans ;

  if ( sscanf ( s,
  "%s rgb %f %f %f amb %f %f %f emis %f %f %f spec %f %f %f shi %d trans %f",
    name,
    &rgb [0], &rgb [1], &rgb [2],
    &amb [0], &amb [1], &amb [2],
    &emis[0], &emis[1], &emis[2],
    &spec[0], &spec[1], &spec[2],
    &shi,
    &trans ) != 15 )
  {
    fprintf ( stderr, "%d: ac_to_gl: Can't parse this MATERIAL:\n", line_count ) ;
    trim_tail(s);
    fprintf ( stderr, "ac_to_gl: MATERIAL %s\n", s ) ;
  }
  else
  {
    char *nm = name ;

    skip_quotes ( &nm ) ;

    not_inside_begin_end () ;

    printf ( "\n" ) ;

    if ( *nm != '\0' )
      printf ( "  /* Material #%d %s */\n", num_materials, nm ) ;
    else
      printf ( "  /* Material #%d [Not Named]*/\n", num_materials ) ;

    printf ( "  glNewList ( %d + materials_base, GL_COMPILE ) ;\n",
                                                  num_materials ) ;
    printf ( "  {\n" ) ;
    printf ( "    GLfloat rgb [ 4 ] ;\n" ) ;
    printf ( "    rgb[0]=%ff; rgb[1]=%ff; rgb[2]=%ff; rgb[3]=%ff;\n", amb[0],amb[1],amb[2],1.0f ) ;
    printf ( "    glMaterialfv ( GL_FRONT_AND_BACK, GL_AMBIENT, rgb ) ;\n" ) ;
    printf ( "    rgb[0]=%ff; rgb[1]=%ff; rgb[2]=%ff; rgb[3]=%ff;\n", rgb[0],rgb[1],rgb[2],1.0f-trans ) ;
    printf ( "    glMaterialfv ( GL_FRONT_AND_BACK, GL_DIFFUSE, rgb ) ;\n" ) ;
    printf ( "    rgb[0]=%ff; rgb[1]=%ff; rgb[2]=%ff; rgb[3]=%ff;\n", spec[0],spec[1],spec[2],1.0f ) ;
    printf ( "    glMaterialfv ( GL_FRONT_AND_BACK, GL_SPECULAR, rgb ) ;\n" ) ;
    printf ( "    rgb[0]=%ff; rgb[1]=%ff; rgb[2]=%ff; rgb[3]=%ff;\n", emis[0],emis[1],emis[2],1.0f ) ;
    printf ( "    glMaterialfv ( GL_FRONT_AND_BACK, GL_EMISSION, rgb ) ;\n" ) ;
    printf ( "    glMaterialf  ( GL_FRONT_AND_BACK, GL_SHININESS, %d.0f ) ;\n", shi ) ;
    printf ( "  }\n" ) ;
    printf ( "  glEndList () ;\n" ) ;
  }

  num_materials++ ;
  return PARSE_CONT ;
}


int do_object   ( char *s )
{
  static int firsttime = TRUE ;

  if ( firsttime )
  {
    firsttime = FALSE ;

    printf ( "}\n" ) ;
    printf ( "\n" ) ;
    printf ( "static void draw_objects ()\n" ) ;
    printf ( "{\n" ) ;
  }

  int obj_type = search ( obj_type_tags, s ) ;  

  trim_tail(s);

  switch ( obj_type )
  {
    case OBJ_WORLD : printf ( "  /* WORLD Object */\n" ) ; break ;
    case OBJ_POLY  : printf ( "  /* POLY  Object */\n" ) ; break ;
    case OBJ_GROUP : printf ( "  /* GROUP Object */\n" ) ; break ;
    case OBJ_LIGHT : /* 2013/05/06 add this object type */
        printf ( "  /* LIGHT Object */\n" ) ; 
        fprintf ( stderr, "%d: ac_to_gl: LIGHT Object - TODO\n", line_count ) ;
        break ; 
    default: 
        fprintf ( stderr, "%d: ac_to_gl: Unknown OBJECT type %s\n", line_count, s ) ;
        printf ( "  /* '%s' Object? */\n", s ) ;
        break;
  }

  char buffer [ 1024 ] ;

  need_texture = FALSE ;

  not_inside_begin_end () ;
  printf ( "  glPushMatrix () ;\n" ) ;

  while ( fgets ( buffer, 1024, stdin ) != NULL ) {
      line_count++;
    if ( search ( object_tags, buffer ) == PARSE_POP )
      break ;
  }
  int num_kids = last_num_kids ;

  for ( int i = 0 ; i < num_kids ; i++ )
  {
    fgets ( buffer, 1024, stdin ) ;
    line_count++;
    search ( top_tags, buffer ) ;
  }

  not_inside_begin_end () ;
  printf ( "  glPopMatrix () ;\n" ) ;

  return PARSE_CONT ;
}


int do_name     ( char *s )
{
  skip_quotes ( &s ) ;

  printf ( "  /* name '%s' */\n", s ) ;

  return PARSE_CONT ;
}

/* 2013/05/02 12:38:26 - geoff - changed behaviour
   Do NOT eat per data count, just get next line
 */
int do_data     ( char *s )
{
  int len = strtol ( s, NULL, 0 ) ;

  if (shown_data == 0) {
    fprintf ( stderr, "%d: WARNING - 'data %d' string encountered - TODO\n",
        line_count, len) ;
    // fprintf ( stderr, "       un-tested and specification is weakly defined.\n" ) ;
  }

  printf ( "  /* data %d */\n", len ) ;
  /* for ( int i = 0 ; i < len ; i++ )
    fgetc ( stdin ) ; */
    char buffer [ 1024 ] ;
    s = buffer;
    fgets ( s, 1024, stdin ) ;
    line_count++;
    skip_spaces ( &s ) ;
    trim_tail ( s );
    printf ( "  /* %s */\n", s );

    shown_data++;

    return PARSE_CONT ;
}


int get_texture ( char *s )
{
  for ( int i = 0 ; i < num_textures ; i++ )
    if ( strcmp ( s, texture_fnames [ i ] ) == 0 )
      return i ;

  texture_fnames [ num_textures ] = new char [ strlen ( s ) + 1 ] ;
  strcpy ( texture_fnames [ num_textures ], s ) ;
 
  return num_textures++ ;
}

int do_texture  ( char *s )
{
  static int last_tex = -1 ;

  skip_quotes ( &s ) ;
  
  int tex = get_texture ( s ) ;

  if ( tex != last_tex )
  {
    not_inside_begin_end () ;
    printf ( "  glBindTexture ( GL_TEXTURE_2D, texture_name [ %d ] ) ; /* \"%s\" */\n", tex, s ) ;
 
    last_tex = tex ;
  }

  need_texture = TRUE ;

  return PARSE_CONT ;
}


int do_texrep   ( char *s )
{
  float texrep [ 2 ] ;

  if ( sscanf ( s, "%f %f", & texrep [ 0 ], & texrep [ 1 ] ) != 2 )
    fprintf ( stderr, "ac_to_gl: Illegal texrep record.\n" ) ;

  return PARSE_CONT ;
}

int do_rot      ( char *s )
{
  float mat [ 4 ] [ 4 ] ;

  mat [ 0 ][ 3 ] = mat [ 1 ][ 3 ] = mat [ 2 ][ 3 ] =
    mat [ 3 ][ 0 ] = mat [ 3 ][ 1 ] = mat [ 3 ][ 2 ] = 0.0f ;
  mat [ 3 ][ 3 ] = 1.0f ; 

  if ( sscanf ( s, "%f %f %f %f %f %f %f %f %f",
        & mat [ 0 ] [ 0 ], & mat [ 0 ] [ 1 ], & mat [ 0 ] [ 2 ],
        & mat [ 1 ] [ 0 ], & mat [ 1 ] [ 1 ], & mat [ 1 ] [ 2 ],
        & mat [ 2 ] [ 0 ], & mat [ 2 ] [ 1 ], & mat [ 2 ] [ 2 ] ) != 9 )
    fprintf ( stderr, "ac_to_gl: Illegal rot record.\n" ) ;

  not_inside_begin_end () ;
  printf ( "  {\n" ) ;
  printf ( "    static float rot [ 16 ] = {" ) ;
  for ( int i = 0 ; i < 4 ; i++ )
    for ( int j = 0 ; j < 4 ; j++ )
      printf ( "%ff,", mat [ i ][ j ] ) ;
  printf ( "  } ;\n" ) ;
  printf ( "    glMultMatrixf ( rot ) ;\n" ) ;
  printf ( "  }\n" ) ;

  return PARSE_CONT ;
}

int do_loc      ( char *s )
{
  float loc [ 3 ] ;

  if ( sscanf ( s, "%f %f %f", & loc [ 0 ], & loc [ 1 ], & loc [ 2 ] ) != 3 ) {
    fprintf ( stderr, "%d: ac_to_gl: Illegal loc record.\n", line_count ) ;
    fprintf ( stderr, "%s", s );
    exit ( 1 );
  }

  not_inside_begin_end () ;
  printf ( "  glTranslatef ( %ff, %ff, %ff ) ;\n", loc[0],loc[1],loc[2] ) ;
  return PARSE_CONT ;
}

int do_url      ( char *s )
{
  skip_quotes ( & s ) ;
  printf ( "/* URL: \"%s\" */\n", s ) ;

  return PARSE_CONT ;
}

int do_numvert  ( char *s )
{
  char buffer [ 1024 ] ;

  int nv = strtol ( s, NULL, 0 ) ;
 
  delete vtab ;

  vtab = new float [ 3 * nv ] ;

  printf ( "\n" ) ;
  printf ( "  static float vtab_%d [ %d ] =\n", ++num_vtabs, nv * 3 ) ;
  printf ( "  {\n" ) ;

  for ( int i = 0 ; i < nv ; i++ )
  {
    fgets ( buffer, 1024, stdin ) ;
    line_count++;

    if ( sscanf ( buffer, "%f %f %f",
                          &vtab[3*i+0], &vtab[3*i+1], &vtab[3*i+2] ) != 3 )
    {
      fprintf ( stderr, "%d: ac_to_gl: Illegal vertex record.\n", line_count ) ;
      fprintf ( stderr, "%s", buffer );
      exit ( 1 ) ;
    }

    printf ( "    %ff, %ff, %ff,  /* V%d */\n",
                  vtab[3*i+0],vtab[3*i+1],vtab[3*i+2], i ) ;
  }

  printf ( "  } ;\n" ) ;
  printf ( "\n" ) ;

  return PARSE_CONT ;
}

int do_numsurf  ( char *s )
{
  int ns = strtol ( s, NULL, 0 ) ;

  for ( int i = 0 ; i < ns ; i++ )
  {
    char buffer [ 1024 ] ;

    fgets ( buffer, 1024, stdin ) ;
    line_count++;
    search ( surf_tag, buffer ) ;
  }

  return PARSE_CONT ;
}

int do_surf     ( char *s )
{
  current_flags = strtol ( s, NULL, 0 ) ;

  char buffer [ 1024 ] ;

  while ( fgets ( buffer, 1024, stdin ) != NULL ) {
    line_count++;
    if ( search ( surface_tags, buffer ) == PARSE_POP )
      break ;
  }
  return PARSE_CONT ;
}

int do_mat      ( char *s )
{
  static int last_mat = -1 ;

  int mat = strtol ( s, NULL, 0 ) ;

  if ( mat != last_mat )
  {
    not_inside_begin_end () ;
    printf ( "  glCallList ( %d + materials_base ) ; /* Material #%d */\n", mat, mat ) ;
    last_mat = mat ;
  }

  return PARSE_CONT ;
}

int do_refs     ( char *s )
{
  int nrefs = strtol ( s, NULL, 0 ) ;
  char buffer [ 1024 ] ;
  int i;

  if ( nrefs == 0 )
    return PARSE_POP ;

  if ( need_texture && ! texture_enabled )
  {
    not_inside_begin_end () ;
    printf ( "  glEnable ( GL_TEXTURE_2D ) ;\n" ) ;
    texture_enabled = TRUE ;
  }
  else
  if ( ! need_texture && texture_enabled )
  {
    not_inside_begin_end () ;
    printf ( "  glDisable ( GL_TEXTURE_2D ) ;\n" ) ;
    texture_enabled = FALSE ;
  }

  int   *vlist  = new int   [ nrefs ] ;
  float *tlistu = new float [ nrefs ] ;
  float *tlistv = new float [ nrefs ] ;
 
  doBegin ( current_flags ) ;

  for (i = 0 ; i < nrefs ; i++ )
  {
    fgets ( buffer, 1024, stdin ) ;
    line_count++;
    if ( sscanf ( buffer, "%d %f %f", &vlist[i], &tlistu[i], &tlistv[i] ) != 3 )
    {
      fprintf ( stderr, "%d: ac_to_gl: Illegal ref record.\n", line_count ) ;
      fprintf ( stderr, "%s", buffer );
      exit ( 1 ) ;
    }
  }

  if ( nrefs >= 3 )
  {
    float nrm [ 3 ] ;
    makeNormal ( nrm, &vtab[3*vlist[0]],&vtab[3*vlist[1]],&vtab[3*vlist[2]] ) ;
    printf ( "  glNormal3f ( %ff, %ff, %ff ) ;\n", nrm[0], nrm[1], nrm[2] ) ;
  }

  for (i = 0 ; i < nrefs ; i++ )
  {
    printf ( "  glTexCoord2f ( %ff, %ff ) ;\n", tlistu[i], tlistv[i] ) ;
    printf ( "  glVertex3fv ( & ( vtab_%d [ %d ] ) ) ; /* V%d */\n",
                                              num_vtabs, vlist[i] * 3, vlist[i] ) ;
  }

  return PARSE_POP ;
}

int do_kids     ( char *s )
{
  last_num_kids = strtol ( s, NULL, 0 ) ;

  return PARSE_POP ;
}

/* 2013/05/02 12:38:26 - add as 'crease 60.000'' */
int do_crease ( char *s )
{
    double d;
    skip_spaces ( & s ) ;
    d = atof ( s ) ;
    if (shown_crease == 0) {
        fprintf ( stderr, "%d: WARNING - 'crease %f' string encountered! TODO\n",
          line_count, d) ;
    }
    if ( trim_tail( s ) ) {
        printf( "  /* crease %s */\n", s );
    }

    shown_crease++;

    return PARSE_CONT ;
}

int do_texoff   ( char *s ) /* 2013/05/06 - add as 'texoff 0 -0.299' */
{
    float off [ 2 ];
    skip_spaces ( & s );
    trim_tail(s);
    if ( sscanf ( s, "%f %f", &off[0], &off[1] ) != 2 ) {
        if (shown_texoff == 0) {
            fprintf ( stderr, "%d: WARNING - 'texoff %s' string encountered! TODO\n", line_count, s );
        }
    } else {
        if (shown_texoff == 0) {
            fprintf ( stderr, "%d: WARNING - 'texoff %f %f' string encountered! TODO\n", line_count,
                off[0], off[1] );
        }
    }
    printf( "  /* texoff %s */\n", s );
    shown_texoff++;
    return PARSE_CONT ;
}

void preamble ()
{
  printf ( "/*\n" ) ;
  printf ( "  This file was automatically generated by ac_to_gl from\n" ) ;
  printf ( "  data produced by AC3D.\n" ) ;
  printf ( "  DO NOT MODIFY THIS FILE!\n" );
  printf ( "  Instead modify ac_to_gl.cxx which did the generation\n" ) ;
  printf ( "*/\n" ) ;
  printf ( "\n" ) ;
  printf ( "#ifdef WIN32\n" );
  printf ( "#include <windows.h>\n" ) ;
  printf ( "#endif\n" );
  printf ( "#include <stdio.h>\n" ) ;
  printf ( "#include <stdlib.h>\n" ) ;
  printf ( "#include <GL/gl.h>\n" ) ;
  printf ( "\n" ) ;
  printf ( "\n" ) ;
  printf ( "#ifndef TRUE\n" ) ;
  printf ( "#define TRUE  1\n" ) ;
  printf ( "#define FALSE 0\n" ) ;
  printf ( "#endif\n" ) ;
  printf ( "\n" ) ;
  printf ( "static GLuint materials_base ;  /* The base display list ident for materials */\n" ) ;
  printf ( "static GLuint *texture_name ;   /* The texture names */\n" ) ;
  printf ( "static void (*load_texture)(char *) ; /* A pointer to a function that can load texture maps */\n" ) ;
  printf ( "\n" ) ;
  printf ( "\n" ) ;
  printf ( "/* Initialise display lists for each material */\n" ) ;
  printf ( "\n" ) ;
  printf ( "static void create_materials ( int num )\n" ) ;
  printf ( "{\n" ) ;
  printf ( "  materials_base = glGenLists ( num ) ;\n" ) ;
}

void postamble ()
{
  not_inside_begin_end () ;

  printf ( "}\n" ) ;
  printf ( "\n" ) ;
  printf ( "\n" ) ;
  printf ( "/* Initialise texture bindings */\n" ) ;
  printf ( "\n" ) ;
  printf ( "static void create_textures ()\n" ) ;
  printf ( "{\n" ) ;
  printf ( "  texture_name = new GLuint [ %d ] ;\n", num_textures ) ;
  printf ( "  glGenTextures ( %d, texture_name ) ;\n", num_textures ) ;
  printf ( "\n" ) ;

  for ( int i = 0 ; i < num_textures ; i++ )
  {
    printf ( "  glBindTexture ( GL_TEXTURE_2D, texture_name [ %d ] ) ;\n", i ) ;
    printf ( "  glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 ) ;\n" ) ;
    printf ( "  if ( load_texture != NULL )\n" ) ;
    printf ( "    (*load_texture) ( (char *)\"%s\" ) ;\n", texture_fnames [ i ] ) ;
    printf ( "  glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) ;\n" ) ;
    printf ( "  glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR ) ;\n" ) ;
    printf ( "  glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT ) ;\n" ) ;
    printf ( "  glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT ) ;\n" ) ;
  }
 
  printf ( "  glBindTexture ( GL_TEXTURE_2D, 0 ) ;\n" ) ;
  printf ( "}\n" ) ;
  printf ( "\n" ) ;
  printf ( "\n" ) ;
  printf ( "void draw_ac3d_model ( void (*load_tex)(char *fname) )\n" ) ;
  printf ( "{\n" ) ;
  printf ( "  static int firsttime = TRUE ;\n" ) ;
  printf ( "  load_texture = load_tex ;\n" ) ;
  printf ( "\n" ) ;
  printf ( "  if ( firsttime )\n" ) ;
  printf ( "  {\n" ) ;
  printf ( "    firsttime = FALSE ;\n" ) ;
  printf ( "    create_materials ( %d ) ;\n", num_materials ) ;
  printf ( "    create_textures  () ;\n" ) ;
  printf ( "  }\n" ) ;
  printf ( "\n" ) ;
  printf ( "  draw_objects () ;\n" ) ;
  printf ( "}\n" ) ;
  printf ( "\n" ) ;
}


int main ( int argc, char **argv )
{
  static char buffer [ 1024 ] ;
  int firsttime = TRUE ;

  preamble () ;

  while ( fgets ( buffer, 1024, stdin ) != NULL )
  {
      line_count++;
    char *s = buffer ;

    /* Skip leading whitespace */

    skip_spaces ( & s ) ;

    /* Skip blank lines and comments */

    if ( *s < ' ' && *s != '\t' ) continue ;
    if ( *s == '#' || *s == ';' ) continue ;

    if ( firsttime )
    {
      firsttime = FALSE ;

      if ( strncasecmp ( s, "AC3D", 4 ) != 0 )
      {
        fprintf ( stderr, "ac_to_gl: Input is not an AC3D format file.\n" ) ;
        exit ( 1 ) ;
      }
    }
    else
      search ( top_tags, s ) ;
  }

  postamble () ;

  return 0 ;
}

/* eof */
